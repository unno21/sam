<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Order;
use App\Model\Product;
use App\Model\Cart_Detail;
use View, Crypt, Auth;

class Order_Detail extends Model
{
    protected $table = 'order_details';
    protected $with = ['product', 'size'];

    public function order()
    {
        return $this->belongsTo('App\Model\Order');
    }
    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
    public function size()
    {
        return $this->belongsTo('App\Model\Category');
    }
    public static function addOrderDetail($request, $id)
    {
        $cart_details = Cart_Detail::where('cart_id', Crypt::decrypt($request->cart_id))->get();
        foreach ($cart_details as $cart_detail) {
            $data = new self;
            $data->order_id = $id;
            $data->product_id = $cart_detail->product_id;
            $data->size_id = $cart_detail->size_id;
            if ($cart_detail->sub_category_id !== null) {
                $data->sub_category_id = $cart_detail->sub_category_id;
                Product::minusStockCategory($cart_detail->sub_category_id,$cart_detail->quantity);
            }
            $data->quantity = $cart_detail->quantity;
            $data->save();
            Cart_Detail::deleteItems($cart_detail->id);
            Product::minusStock($cart_detail->product_id, $cart_detail->quantity);
        }
        return true;
    }
}
