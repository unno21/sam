<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product_Filter extends Model
{
    protected $table = "product_by_subcategory";
    protected $with = ['category'];
    protected $guarded = [];
    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
    public function category()
    {
        return $this->belongsTo('App\Model\Category');
    }
    public function cart_details()
    {
        return $this->hasMany('App\Model\Product', 'sub_category_id');
    }
    public static function addItem($image, $category_id, $stock, $id, $upload_file)  
    {
        $data = new self;
        if ($image === null) {
            $data->image                = 'default.png'; 
        } else {
            $path                       = 'public/products/';
            $data->image                = Helper::filtNotRequest($image,'image', $path);
        }
        if ( $upload_file === null ) {
            $data->upload_file          = 'default.fbx';
        } else {
            $path                       = 'public/fvx/';
            $data->upload_file          = Helper::filtNotRequest($upload_file,'upload_file', $path);    
        }
        $data->product_id = $id;
        $data->category_id = $category_id;
        $data->stock = $stock;
        $data->save();
    }
}
