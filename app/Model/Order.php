<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Order_Detail;
use App\Model\Order_Notify;
use App\Model\Cart;
use Auth, Crypt, View;
class Order extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function order_details()
    {
        return $this->hasMany('App\Model\Order_Detail');
    }
    public function order_notify()
    {
        return $this->hasMany('App\Model\Order_Notify');
    }
    public static function addOrder($request, $order_number)
    {
        $data = new self;
        $data->order_number = $order_number;
        $data->user_id = Auth::user()->id;
        $data->amount = Crypt::encrypt($request->grandtotal);
        if ( $request->address !== null && $request->contact_number2 !== null ) {
            $data->phone_number = $request->contact_number1 . $request->contact_number2;
            $data->address = $request->address;
        }
        if ( $request->label !== null )
            $data->type = '0';
        else 
            $data->type = '1';
        $data->save();
        $id = $data->id;
        Order_Detail::addOrderDetail($request, $id);
        Cart::deleteCart(Crypt::decrypt($request->cart_id));
        return $data;
    }
    public static function updateOrder($id, $status)
    {
        $data = self::find($id);
        if ($status === 'processed') 
            $data->status = '0';
        else if ($status === 'shipped') 
            $data->status = '1';
        else
            $data->status = '2';
        Order_Notify::addNotif($status, $id);
        $data->save();
        return $data;
    }
}

