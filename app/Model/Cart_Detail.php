<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Cart;
use App\Model\Product;
use View,Crypt,Auth;

class Cart_Detail extends Model
{
    protected $table = 'cart_details';
    protected $with = ['product', 'product_filters'];
    protected $appends = ['sub_total'];

    public function cart()
    {
        return $this->belongsTo('App\Model\Cart');
    }
    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
    public function product_filters()
    {
        return $this->belongsTo('App\Model\Product_Filter', 'sub_category_id');
    }
    public function size()
    {
        return $this->belongsTo('App\Model\Category');
    }
    public static function addQuantity($request, $id)
    {
        $data = self::find($id);
        $item_quantity = $data->quantity;
        $data->quantity = $item_quantity + $request->quantity;
        $data->save();
        return true;
    }
    public static function addOneQuantity($id)
    {
        $data = self::find($id);
        $item_quantity = $data->quantity;
        $data->quantity = $item_quantity + 1;
        $data->save();
        return true;
    }
    public static function addQuantityByProduct($request)
    {
        $data = self::where('product_id', $request->id)
                    ->where('cart_id', $request->cart_id)
                    ->first();
        $item_quantity = $data->quantity;
        $data->quantity = $item_quantity + $request->quantity;
        $data->save();
        return true;
    }
    public static function addItem($request, $id)
    {
        $data = new self;
        $data->cart_id = $id;
        $data->product_id = $request->id;
        $data->sub_category_id = $request->sub_category_id;
        $data->size_id = 1;
        $data->quantity = $request->quantity;
        $data->save();
        return true;
    }
    public static function addOneItem($id, $cart_id)
    {
        $data = new self;
        $data->cart_id = $cart_id;
        $data->product_id = $id;
        $data->quantity = 1;
        $data->save();
        return true;
    }
    public static function deleteItems($id)
    {
        self::find($id)->delete();
        return true;
    }
    public function sub_total()
    {
        $total = 0;
        $total = $this->product->price * $this->quantity;
        return $total;
    }
    public static function update_quantity($id, $quantity)
    {
        $data = self::find($id);
        $data->quantity = $quantity;
        $data->save(); 
    }
    public static function addItemWishlist($product_id, $cart_id)
    {
        $data = new self;
        $data->cart_id = $cart_id;
        $data->product_id = $product_id;
        $data->quantity = '1';
        $data->save();
        return true;
    }
    public static function updateQuantityWishlist($id, $quantity)
    {
        $data = self::find($id);
        if ( $data->product->stock > $data->quantity + $quantity ){
            $data->quantity = $data->quantity + $quantity;
            $data->save(); 
            return true;
        } else 
            return false;
        
    }
    /*
        Property for API
    */
    public function getSubTotalAttribute()
    {
        return $this->sub_total();
    }
    public static function changeColor($product_id, $id)
    {
        $data = self::find($id);
        if ( $product_id === 'default')
            $data->sub_category_id = null;
        else
            $data->sub_category_id = $product_id;
        $data->save();
        return $data;
    }
    public static function changeSize($size_id,$id)
    {
        $data = self::find($id);
        $data->size_id = $size_id;
        $data->save();
        return $data;
    }
}
