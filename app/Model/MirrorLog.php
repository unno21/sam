<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MirrorLog extends Model
{
    protected $table = 'mirror_logs';
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public static function login($id)
    {
        $data = new self;
        $data->user_id = $id;
        $data->save();

        return $data->user;
    }
    public static function logout($id)
    {
        $data = self::where('user_id', $id)->orderBy('id', 'DESC')->first();
        $data->status = 0;
        $data->save();

        return $data->user;
    }
    public static function getLoggedInUser()
    {
        return self::where('status', 1)
                    ->orderBy('updated_at', 'DESC')
                    ->first();
    }
}
