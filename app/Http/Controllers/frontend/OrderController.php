<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App\Model\Order;
use App\Model\Product;
use App\Model\Order_Notify;
use App\Model\Category;
use App\User;

use View, Crypt, Auth, Validator;

class OrderController extends Controller
{
    public function getIndex()
    {
        $this->data['orders'] = Order::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        return view('front-end.order.index', $this->data);
    }
    public function getDetail($id)
    {
        $d_id = Crypt::decrypt($id);
        $this->data['order'] = Order::find($d_id);
        return view('front-end.order.detail-index', $this->data);
    }
    function checkoutValidation(Request $request)
    {
        $validation = [
            'name'    => 'required',
            'number'     => 'required',
            'expiry'         => 'required',
            'cvv'       => 'required',
        ];

        $validator = Validator::make($request->all(), $validation);

        if($validator->fails()){
            return response()->json(['result' => 'failed', "errors" => $validator->errors()->messages()]);
        }
        
        return response()->json(['result' => 'success']);
    }

    public function postInsert(Request $request)
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));

        $customer = Customer::create(array(            
            "email"     => Auth::user()->email,
            'source'  => $request->stripeToken
        ));

        $charge = Charge::create(array(
            'customer' => $customer->id,
            'amount'   => $request->grandtotal * 100, 
            'currency' => 'php',
            "description" => "SAM Apparel"
        ));


        if ($request->address === null && $request->contact_number2 === null) {
            $check = User::find(Auth::user()->id);
            if ( $check->address === null ){
                $this->data['cart'] = User::find(Auth::user()->id)->cart;
                $this->data['snackbar'] =  "Item Successfully Added in Cart";
                $this->data['sizes'] = Category::where('type', 'size')->get();
                $this->data['colors'] = Category::where('type', 'color')->get();
                return view('front-end.cart.index', $this->data);
            }
                
        }
        $order = Order::all();
        $order_total = count($order) + 1;
        if ( strlen($order_total) == 1 )
            $order_total = '00' . $order_total;
        else if ( strlen($order_total) == 2)
            $order_total = '0' . $order_total;
        $order_number = 'ord' . $order_total ;
        $order = Order::addOrder($request, $order_number);
        $order_id = $order->id;
        $status = 0;
        Order_Notify::addNotif($status, $order_id);
        return redirect('/');   
    }

    public function postInsertCod(Request $request)
    {
        if ($request->address === null && $request->contact_number2 === null) {
            $check = User::find(Auth::user()->id);
            if ( $check->address === null )
                return redirect('/cart');
        }
        $order = Order::all();
        $order_total = count($order) + 1;
        if ( strlen($order_total) == 1 )
            $order_total = '00' . $order_total;
        else if ( strlen($order_total) == 2)
            $order_total = '0' . $order_total;
        $order_number = 'ord' . $order_total ;
        $order = Order::addOrder($request, $order_number);
        $order_id = $order->id;
        $status = 0;
        Order_Notify::addNotif($status, $order_id);
        return redirect('/'); 
    }

    public function getProductReview($id)
    {
        $decrypted_id = Crypt::decrypt($id);
        $this->data['product'] = Product::find($decrypted_id);
        return view('front-end.order.review', $this->data);
    }
}
