<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\AuditTrail;
use View,Crypt,Auth;

class EmployeeManagementController extends Controller
{
    public function getIndex()
    {
        $this->data['employees'] = User::where('type', 'cashier')->get();
        return view('back-end.employees.index', $this->data);    
    }
    public function postInsert(Request $request)
    {
        User::manageEmployee($request);
        $user_id = Auth::user()->id;
        $comment = 'you hired a cashier';
        AuditTrail::insertComment($user_id, $comment);
        $this->data['employees'] = User::where('type', 'cashier')->get();
        $content = View::make('back-end.employees.includes.index-inner', $this->data)->render();
        return response()->json([
            'content'       => $content
        ]);
    }
}
