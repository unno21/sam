<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Product_Filter;
use App\Model\Category;
use App\Model\AuditTrail;
use View,Crypt,Hash,Auth;

class ProductManagementController extends Controller
{
    public function getIndex()
    {
        $this->data['products'] = Product::select('*')->orderBy('created_at', 'desc')->paginate(5);
        $this->data['colors'] = Category::where('type', 'color')->get();
        $this->data['categories'] = Category::where('type', 'gender')->get();
        $this->data['categ_products'] = Category::where('type', 'product')->get();
        // dd(get_class($this->data['products']));
        return view('back-end.products.index', $this->data);
    }
    public function postInsert(Request $request)
    {
        Product::manageData($request);
        $this->data['products'] = Product::select('*')->orderBy('created_at', 'desc')->paginate(5);
        $content = View::make('back-end.products.includes.index-inner', $this->data)->render();
        return response()->json([
            'content'    => $content,
            'word'      => 'Product has been added into database'
        ]);
    }
    public function getDelete($id)
    {
        Product::find($id)->delete();
        $user_id = Auth::user()->id;
        $comment = "Icp management has deleted the " . $id . " product";
        AuditTrail::insertComment($user_id, $comment);
        $this->data['products'] = Product::select('*')->orderBy('created_at', 'desc')->paginate(5);
        $content = View::make('back-end.products.includes.index-inner', $this->data)->render();
        return response()->json([
            'content'       => $content,
            'word'      => 'Product has been removed.'
        ]);
    }
    public function getFilter($word)
    {
        
        if ( $word === 'ALL') {
            $this->data['products'] = Product::select('*')->orderBy('created_at', 'desc')->paginate(5);
        } else {
            $category_id = Category::where('name', $word)->first();
            $this->data['products'] = Product::where('category_id', $category_id->id)->orderBy('created_at', 'desc')->paginate(5);
        }
        
        $content = View::make('back-end.products.includes.index-inner', $this->data)->render();
        return response()->json([
            'content'       => $content,
            'word'          => $word
        ]);
    }
    public function getSearch($word)
    {
        //$category_id = Category::where('name', $word)->first();
        // $this->data['label'] = "search";
        $this->data['products'] = Product::orWhere('name', 'like', '%' . $word . '%')
          //                          ->orWhere('category_id', $category_id->id)
                                    ->orderBy('created_at', 'desc')
                                    ->paginate(5);
        $content = View::make('back-end.products.includes.index-inner', $this->data)->render();
        return response()->json([
            'content'       => $content
        ]);                            
    }
    public function getEdit($id)
    {
        $this->data['product'] = Product::find($id);
        $this->data['product_filter'] = Product_Filter::where('product_id', $id)->get();
        $content = View::make('back-end.products.includes.index-inner-inner', $this->data)->render();
        return response()->json([
            'product'   => $this->data['product'],
            'content'   => $content
        ]);
    }
    public function postUpdate(Request $request)
    {
        Product::updateProduct($request);
        $this->data['products'] = Product::select('*')->orderBy('created_at', 'desc')->paginate(5);
        $content = View::make('back-end.products.includes.index-inner', $this->data)->render();
            return response()->json([
                'content'    => $content
            ]);
    }
}
