<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Order;
use App\Model\Order_Detail;
use App\User;
use View, Crypt, Auth;

class DashboardController extends Controller
{
    public function getIndex()
    {
        $this->data['best_seller'] = Order_Detail::selectRaw('product_id,count(product_id) as best_seller')->groupBy('product_id')->orderBy('best_seller', 'desc')->first();
        $this->data['critical_product'] = Product::selectRaw('*,(stock/(stock + used_stock)) as critical_level')->orderBy('critical_level', 'asc')->first();
        $this->data['recently_addeds'] = Product::orderBy('created_at','desc')->limit(4)->get();
        $this->data['latest_orders'] = Order::orderBy('created_at', 'desc')->limit(6)->get();
        $this->data['total_products'] = count(Product::all());
        $this->data['total_customer'] = User::where('type', 'user')->count();
        $this->data['total_order'] = count(Order::all());
        $this->data['total_sales'] = 0;
        $orders_total = Order::all();
        foreach ($orders_total as $order) {
            $this->data['total_sales'] += Crypt::decrypt($order->amount);
        }
        return view('back-end.dashboard.manager.index', $this->data);
    }
}
