<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Product;
use App\Model\Category;

class ProductAPIController extends Controller
{
    public function getIndex()
    {
        $products = Product::all();
        return response()->json($products);
    }
    public function getSearch($keyword)
    {
        $products = Product::whereHas('category', function($q) use($keyword){
                            $q->where('name', 'LIKE', "%$keyword%");
                        })->orWhere('name', 'LIKE', "%$keyword%")
                        ->get();
        return response()->json($products);
    }
    public function getFilterByCategory($id)
    {
        $products = Product::where('category_id', $id)->get();
        return response()->json($products);
    }
    public function getFilterByCategoryAndGender($category_id, $gender_id)
    {
        $products = Product::where('category_id', $category_id)
                            ->where(function($query) use($gender_id){
                                $query->where('gender_id', $gender_id)
                                        ->orWhere('gender_id', 19);
                            })
                            ->get();
        return response()->json($products);
    }
    public function getScan($code)
    {
        $product = Product::where('barcode', $code)->first();
        return response()->json($product);
    }
    public function getSizes()
    {
        $sizes = Category::where('type', 'size')->get();
        return response()->json($sizes);
    }
}
