<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\MirrorLog;

class MirrorLogsController extends Controller
{
    public function login($id)
    {
        $user = MirrorLog::login($id);
        return response()->json($user);
    }
    public function logout($id)
    {
        $user = MirrorLog::logout($id);
        return response()->json($user);
    }
    public function getLoggedInUser()
    {
        $user = MirrorLog::getLoggedInUser()->user;
        return response()->json($user);
    }
}
