-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2018 at 05:14 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sam`
--

-- --------------------------------------------------------

--
-- Table structure for table `audit_trails`
--

CREATE TABLE `audit_trails` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audit_trails`
--

INSERT INTO `audit_trails` (`id`, `user_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 3, 'the order number ord006has been shipped by you.', '2018-10-05 03:17:24', '2018-10-05 03:17:24'),
(2, 3, 'HR has hired a cashier', '2018-10-05 04:01:58', '2018-10-05 04:01:58'),
(3, 4, 'the order ord008 has been delivered by you.', '2018-10-12 19:36:47', '2018-10-12 19:36:47'),
(4, 4, 'the order ord008 has been delivered by you.', '2018-10-12 19:36:50', '2018-10-12 19:36:50'),
(5, 4, 'the order ord008 has been closed by you.', '2018-10-12 19:36:58', '2018-10-12 19:36:58'),
(6, 4, 'the order ord008 has been closed by you.', '2018-10-12 19:36:59', '2018-10-12 19:36:59'),
(7, 4, 'the order ord008 has been closed by you.', '2018-10-12 19:36:59', '2018-10-12 19:36:59'),
(8, 4, 'the order ord008 has been closed by you.', '2018-10-12 19:37:00', '2018-10-12 19:37:00'),
(9, 4, 'the order ord008 has been closed by you.', '2018-10-12 19:37:01', '2018-10-12 19:37:01'),
(10, 4, 'the order ord008 has been closed by you.', '2018-10-12 19:37:01', '2018-10-12 19:37:01'),
(11, 4, 'the order ord022 has been shipped by you.', '2018-10-13 05:38:45', '2018-10-13 05:38:45'),
(12, 4, 'the order ord022 has been shipped by you.', '2018-10-13 05:38:46', '2018-10-13 05:38:46'),
(13, 4, 'the order ord022 has been shipped by you.', '2018-10-13 05:38:47', '2018-10-13 05:38:47'),
(14, 4, 'the order ord022 has been processed by you.', '2018-10-13 05:40:59', '2018-10-13 05:40:59'),
(15, 4, 'the order ord024 has been shipped by you.', '2018-10-13 21:36:13', '2018-10-13 21:36:13'),
(16, 4, 'Icp management added a product, 1800.00 with a price of 123', '2018-10-15 03:30:24', '2018-10-15 03:30:24'),
(17, 4, 'Icp management added a product, dasdasda with a price of 2311', '2018-10-16 22:00:12', '2018-10-16 22:00:12'),
(18, 4, 'Icp management added a product, dqwdqwdq with a price of 2121', '2018-10-16 22:04:33', '2018-10-16 22:04:33'),
(19, 4, 'Icp management added a product, First blood with a price of 32131', '2018-10-16 22:05:53', '2018-10-16 22:05:53'),
(20, 4, 'the order ord026 has been shipped by you.', '2018-10-21 02:19:13', '2018-10-21 02:19:13'),
(21, 4, 'the order ord026 has been delivered by you.', '2018-10-23 01:57:11', '2018-10-23 01:57:11'),
(22, 4, 'Icp management has deleted the 10 product', '2018-11-23 07:19:02', '2018-11-23 07:19:02'),
(23, 4, 'Icp management has deleted the 13 product', '2018-11-23 07:22:57', '2018-11-23 07:22:57'),
(24, 4, 'Icp management has deleted the 15 product', '2018-11-23 07:23:12', '2018-11-23 07:23:12'),
(25, 4, 'Icp management has deleted the 11 product', '2018-11-23 07:24:52', '2018-11-23 07:24:52'),
(26, 4, 'Icp management has deleted the 9 product', '2018-11-23 07:26:13', '2018-11-23 07:26:13'),
(27, 4, 'Icp management has deleted the 14 product', '2018-11-23 07:26:32', '2018-11-23 07:26:32'),
(28, 4, 'Icp management has deleted the 8 product', '2018-11-23 07:27:09', '2018-11-23 07:27:09'),
(29, 4, 'Icp management has deleted the 12 product', '2018-11-23 07:27:13', '2018-11-23 07:27:13'),
(30, 4, 'Icp management added a product, Hanging Blouse with a price of 230', '2018-11-23 07:32:31', '2018-11-23 07:32:31'),
(31, 4, 'Icp management added a product, Wide Armed Blouse with a price of 230', '2018-11-23 07:33:38', '2018-11-23 07:33:38'),
(32, 4, 'Icp management added a product, Common Blouse with a price of 320', '2018-11-23 07:34:21', '2018-11-23 07:34:21'),
(33, 4, 'Icp management added a product, Floral Blouse with a price of 500', '2018-11-23 07:35:51', '2018-11-23 07:35:51'),
(34, 4, 'Icp management added a product, See Through Blouse with a price of 1000', '2018-11-23 07:36:30', '2018-11-23 07:36:30'),
(35, 4, 'Icp management added a product, Maong Jacket with a price of 1200', '2018-11-23 07:37:23', '2018-11-23 07:37:23'),
(36, 4, 'Icp management added a product, Fitted Jacket with a price of 800', '2018-11-23 07:38:06', '2018-11-23 07:38:06'),
(37, 4, 'Icp management added a product, Hoodie Jacket with a price of 600', '2018-11-23 07:40:30', '2018-11-23 07:40:30'),
(38, 4, 'Icp management added a product, Fur Jacket with a price of 1500', '2018-11-23 07:40:59', '2018-11-23 07:40:59'),
(39, 4, 'Icp management added a product, Plain Long Sleeve with a price of 300', '2018-11-23 07:41:59', '2018-11-23 07:41:59'),
(40, 4, 'Icp management added a product, Korean Type Long Sleeve with a price of 400', '2018-11-23 07:42:33', '2018-11-23 07:42:33'),
(41, 4, 'Icp management added a product, Beep Long Sleeve with a price of 250', '2018-11-23 07:43:17', '2018-11-23 07:43:17'),
(42, 4, 'Icp management added a product, Long Sleeve with Button with a price of 250', '2018-11-23 07:44:07', '2018-11-23 07:44:07'),
(43, 4, 'Icp management added a product, Sun Ray Long Sleeve with a price of 320', '2018-11-23 07:50:22', '2018-11-23 07:50:22'),
(44, 4, 'Icp management added a product, Plain Polo Shirt with a price of 230', '2018-11-23 07:53:57', '2018-11-23 07:53:57'),
(45, 4, 'Icp management added a product, Horse Men Polo Shirt with a price of 300', '2018-11-23 07:55:26', '2018-11-23 07:55:26'),
(46, 4, 'Icp management added a product, Racing Polo Shirt with a price of 300', '2018-11-23 07:56:02', '2018-11-23 07:56:02'),
(47, 4, 'Icp management added a product, Puly Polo Shirt with a price of 250', '2018-11-23 07:56:38', '2018-11-23 07:56:38'),
(48, 4, 'Icp management added a product, Outer Coloured Polo Shirt with a price of 400', '2018-11-23 07:57:39', '2018-11-23 07:57:39'),
(49, 4, 'Icp management added a product, Pittsburgh Polo Shirt with a price of 300', '2018-11-23 07:58:22', '2018-11-23 07:58:22'),
(50, 4, 'Icp management added a product, Snake Polo Shirt with a price of 450', '2018-11-23 07:58:52', '2018-11-23 07:58:52'),
(51, 4, 'Icp management added a product, Liberty Or Death with a price of 320', '2018-11-23 08:00:29', '2018-11-23 08:00:29'),
(52, 4, 'Icp management added a product, Dub Magician with a price of 600', '2018-11-23 08:01:03', '2018-11-23 08:01:03'),
(53, 4, 'Icp management added a product, Drawed Tshirt with a price of 200', '2018-11-23 08:01:58', '2018-11-23 08:01:58'),
(54, 4, 'Icp management added a product, Hot Dama Lama with a price of 300', '2018-11-23 08:03:06', '2018-11-23 08:03:06'),
(55, 4, 'Icp management added a product, Yellow Days with a price of 400', '2018-11-23 08:03:48', '2018-11-23 08:03:48'),
(56, 4, 'Icp management added a product, Green Life Tshirt with a price of 400', '2018-11-23 08:04:10', '2018-11-23 08:04:10');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(26, 3, '2018-10-08 07:07:53', '2018-10-08 07:07:53', NULL),
(36, 6, '2018-10-21 01:39:35', '2018-10-21 01:39:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart_details`
--

CREATE TABLE `cart_details` (
  `id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `size_id` int(7) NOT NULL DEFAULT '1',
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_details`
--

INSERT INTO `cart_details` (`id`, `cart_id`, `product_id`, `sub_category_id`, `size_id`, `quantity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(14, 36, 15, NULL, 3, 1, '2018-10-21 01:39:35', '2018-10-21 02:13:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'small', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(2, 'medium\r\n', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(3, 'large', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(4, 'extra large', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(5, 'jacket', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(6, 'polo shirt', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(7, 'three fourth\'s', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(8, 't-shirt', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(9, 'blouse', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(10, 'long sleeve', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(11, 'others', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(12, 'yellow', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(13, 'blue', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(14, 'pink', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(15, 'red', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(16, 'orange', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(17, 'male', 'gender', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(18, 'female', 'gender', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(19, 'unisex', 'gender', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(20, 'purple', 'color', '2018-10-13 18:37:58', '2018-10-13 18:37:58'),
(21, 'brown', 'color', '2018-11-23 07:38:22', '2018-11-23 07:38:22'),
(22, 'green', 'color', '2018-11-23 07:51:02', '2018-11-23 07:51:02');

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `logo` varchar(191) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(191) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`id`, `logo`, `name`, `email`, `contact`, `created_at`, `updated_at`) VALUES
(1, 'asd', 'Shopping Assistant Mirror', 'internsprog@gmail.com', '123', '2018-07-26 01:51:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `log_trails`
--

CREATE TABLE `log_trails` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `log_date` date NOT NULL,
  `log_in` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_out` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_trails`
--

INSERT INTO `log_trails` (`id`, `user_id`, `log_date`, `log_in`, `log_out`, `created_at`, `updated_at`) VALUES
(7, 4, '2018-10-03', '2018-10-03 04:03:34', '2018-10-03 04:03:34', '2018-10-03 04:03:34', '2018-10-02 20:03:34'),
(8, 4, '2018-10-03', '2018-10-03 04:47:57', '2018-10-03 04:47:57', '2018-10-03 04:47:57', '2018-10-02 20:47:57'),
(9, 3, '2018-10-03', '2018-10-03 04:34:39', NULL, '2018-10-02 20:34:39', '2018-10-02 20:34:39'),
(10, 3, '2018-10-03', '2018-10-03 05:11:15', '2018-10-03 05:11:15', '2018-10-03 05:11:15', '2018-10-02 21:11:15'),
(11, 3, '2018-10-03', '2018-10-03 05:27:04', '2018-10-03 05:27:04', '2018-10-03 05:27:04', '2018-10-02 21:27:04'),
(12, 3, '2018-10-03', '2018-10-03 05:28:15', NULL, '2018-10-02 21:28:15', '2018-10-02 21:28:15'),
(13, 3, '2018-10-05', '2018-10-05 11:42:48', '2018-10-05 11:42:48', '2018-10-05 11:42:48', '2018-10-05 03:42:48'),
(14, 3, '2018-10-05', '2018-10-05 12:02:52', '2018-10-05 12:02:52', '2018-10-05 12:02:52', '2018-10-05 04:02:52'),
(15, 4, '2018-10-05', '2018-10-05 13:03:06', '2018-10-05 13:03:06', '2018-10-05 13:03:06', '2018-10-05 05:03:06'),
(16, 3, '2018-10-05', '2018-10-05 14:23:40', '2018-10-05 14:23:40', '2018-10-05 14:23:40', '2018-10-05 06:23:40'),
(17, 4, '2018-10-07', '2018-10-07 02:06:50', '2018-10-07 02:06:50', '2018-10-07 02:06:50', '2018-10-06 18:06:50'),
(18, 4, '2018-10-13', '2018-10-13 03:48:54', '2018-10-13 03:48:54', '2018-10-13 03:48:54', '2018-10-12 19:48:54'),
(19, 4, '2018-10-13', '2018-10-13 04:06:04', NULL, '2018-10-12 20:06:04', '2018-10-12 20:06:04'),
(20, 4, '2018-10-13', '2018-10-13 08:07:52', '2018-10-13 08:07:52', '2018-10-13 08:07:52', '2018-10-13 00:07:52'),
(21, 4, '2018-10-13', '2018-10-13 11:09:57', '2018-10-13 11:09:57', '2018-10-13 11:09:57', '2018-10-13 03:09:57'),
(22, 4, '2018-10-13', '2018-10-13 13:31:50', '2018-10-13 13:31:50', '2018-10-13 13:31:50', '2018-10-13 05:31:50'),
(23, 4, '2018-10-13', '2018-10-13 13:53:14', '2018-10-13 13:53:14', '2018-10-13 13:53:14', '2018-10-13 05:53:14'),
(24, 3, '2018-10-14', '2018-10-14 00:24:41', '2018-10-14 00:24:41', '2018-10-14 00:24:41', '2018-10-13 16:24:41'),
(25, 3, '2018-10-14', '2018-10-14 00:25:00', '2018-10-14 00:29:00', '2018-10-14 00:29:00', '2018-10-13 16:29:00'),
(26, 4, '2018-10-14', '2018-10-14 02:01:12', '2018-10-14 02:42:56', '2018-10-14 02:42:56', '2018-10-13 18:42:56'),
(27, 4, '2018-10-14', '2018-10-14 05:18:45', '2018-10-14 05:36:15', '2018-10-14 05:36:15', '2018-10-13 21:36:15'),
(28, 4, '2018-10-15', '2018-10-15 11:28:49', '2018-10-15 12:49:53', '2018-10-15 12:49:53', '2018-10-15 04:49:53'),
(29, 4, '2018-10-17', '2018-10-17 01:13:28', NULL, '2018-10-16 17:13:28', '2018-10-16 17:13:28'),
(30, 4, '2018-10-17', '2018-10-17 05:45:50', '2018-10-17 05:45:57', '2018-10-17 05:45:57', '2018-10-16 21:45:57'),
(31, 4, '2018-10-17', '2018-10-17 05:59:33', '2018-10-17 06:23:26', '2018-10-17 06:23:26', '2018-10-16 22:23:26'),
(32, 4, '2018-10-18', '2018-10-18 07:51:11', NULL, '2018-10-17 23:51:11', '2018-10-17 23:51:11'),
(33, 4, '2018-10-21', '2018-10-21 10:16:51', '2018-10-21 10:44:17', '2018-10-21 10:44:17', '2018-10-21 02:44:17'),
(34, 4, '2018-10-21', '2018-10-21 13:15:21', NULL, '2018-10-21 05:15:21', '2018-10-21 05:15:21'),
(35, 5, '2018-10-23', '2018-10-23 09:42:15', NULL, '2018-10-23 01:42:15', '2018-10-23 01:42:15'),
(36, 3, '2018-10-23', '2018-10-23 09:45:18', '2018-10-23 09:45:56', '2018-10-23 09:45:56', '2018-10-23 01:45:56'),
(37, 3, '2018-10-23', '2018-10-23 09:47:31', '2018-10-23 09:55:22', '2018-10-23 09:55:22', '2018-10-23 01:55:22'),
(38, 4, '2018-10-23', '2018-10-23 09:56:22', NULL, '2018-10-23 01:56:22', '2018-10-23 01:56:22'),
(39, 4, '2018-11-23', '2018-11-23 15:16:24', NULL, '2018-11-23 07:16:24', '2018-11-23 07:16:24');

-- --------------------------------------------------------

--
-- Table structure for table `notify_order`
--

CREATE TABLE `notify_order` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notify_order`
--

INSERT INTO `notify_order` (`id`, `order_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 2, 'Your order has been delivered by our courier.', '2018-09-24 22:05:10', '2018-09-24 22:05:10'),
(2, 1, 'Your order has been shipped by our courier.', '2018-09-24 22:05:21', '2018-09-24 22:05:21'),
(3, 4, 'Your order has been shipped by our courier.', '2018-09-27 18:10:29', '2018-09-27 18:10:29'),
(4, 4, 'Your order has been delivered by our courier.', '2018-09-27 18:11:45', '2018-09-27 18:11:45'),
(5, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:16:49', '2018-10-05 03:16:49'),
(6, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:17:03', '2018-10-05 03:17:03'),
(7, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:17:07', '2018-10-05 03:17:07'),
(8, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:17:24', '2018-10-05 03:17:24'),
(9, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:47', '2018-10-12 19:36:47'),
(10, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:50', '2018-10-12 19:36:50'),
(11, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:58', '2018-10-12 19:36:58'),
(12, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:59', '2018-10-12 19:36:59'),
(13, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:59', '2018-10-12 19:36:59'),
(14, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:37:00', '2018-10-12 19:37:00'),
(15, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:37:01', '2018-10-12 19:37:01'),
(16, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:37:01', '2018-10-12 19:37:01'),
(17, 22, 'Your order is being processed at the moment.', '2018-10-13 04:09:04', '2018-10-13 04:09:04'),
(18, 22, 'Your order has been shipped by our courier.', '2018-10-13 05:38:45', '2018-10-13 05:38:45'),
(19, 22, 'Your order has been shipped by our courier.', '2018-10-13 05:38:46', '2018-10-13 05:38:46'),
(20, 22, 'Your order has been shipped by our courier.', '2018-10-13 05:38:47', '2018-10-13 05:38:47'),
(21, 22, 'Your order is being processed at the moment.', '2018-10-13 05:40:59', '2018-10-13 05:40:59'),
(22, 23, 'Your order is being processed at the moment.', '2018-10-13 05:55:28', '2018-10-13 05:55:28'),
(23, 24, 'Your order is being processed at the moment.', '2018-10-13 05:56:06', '2018-10-13 05:56:06'),
(24, 24, 'Your order has been shipped by our courier.', '2018-10-13 21:36:13', '2018-10-13 21:36:13'),
(25, 25, 'Your order is being processed at the moment.', '2018-10-17 16:46:29', '2018-10-17 16:46:29'),
(26, 26, 'Your order is being processed at the moment.', '2018-10-21 01:26:10', '2018-10-21 01:26:10'),
(27, 26, 'Your order has been shipped by our courier.', '2018-10-21 02:19:13', '2018-10-21 02:19:13'),
(28, 26, 'Your order has been delivered by our courier.', '2018-10-23 01:57:10', '2018-10-23 01:57:10');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_number` varchar(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` text NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `address` text,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0 = process, 1 = shipped, 2 = delivered',
  `type` int(11) NOT NULL COMMENT '0 = COD, 1 = Credit Card',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_number`, `user_id`, `amount`, `phone_number`, `address`, `status`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ord001', 6, 'eyJpdiI6IkRvZG5FRjRQUXJ3V2xSOHdjSnJIdVE9PSIsInZhbHVlIjoiSVhLSDloOU9nd0d3bzFVVXMwWkhuZz09IiwibWFjIjoiMDhkMDRjOWIyNTA5YzdhYmUwMWM4ZTZmZDQ4MmU2MDE1ZjY0OGI1ZTMyNWQ1N2I5OTZmNDFmNDZkNmRkYmIzNyJ9', '', '', 1, 0, '2018-09-25 06:05:21', '2018-09-24 22:05:21', NULL),
(2, 'ord002', 6, 'eyJpdiI6IlhMWUVITXJUWEtzNFdRUXBDZk9cL0dBPT0iLCJ2YWx1ZSI6IjBJZENOYW82U3dXVkY3NDJmV294YWc9PSIsIm1hYyI6IjRkMWIzZTk2ZjQyZTkxMWFhOThmZjJlYjZhZDdjMTI3MGUyODJmODdjODg0Y2Q3YzQzOWQ5MDI4ZDhkZDY2NjcifQ==', '', '', 2, 0, '2018-09-25 06:05:11', '2018-09-24 22:05:11', NULL),
(3, 'ord003', 6, 'eyJpdiI6IjNYOEpyZ1p2a3JpWElZNGhXY3lIUkE9PSIsInZhbHVlIjoia1JsRVptXC9Pb1VNQkM5VEdGbW5BTUE9PSIsIm1hYyI6ImRkM2Q0MWRlMjQ0YzRmNzEyMzUxMWY5MTYzZWE2MDk2ODFkODI0MzVhZjcwMmE0YTViNWRkODVhOTU2MzBiODEifQ==', '', '', 0, 0, '2018-08-27 04:49:42', '2018-08-21 22:17:43', NULL),
(4, 'ord004', 6, 'eyJpdiI6ImlEQ0FQcGhkdzZCXC9tSUhWR1NYVHZRPT0iLCJ2YWx1ZSI6Ikx0TlkwWk1yWlR2UzFzV3gwbStnU1E9PSIsIm1hYyI6ImVhNmYyYTc2ZWIxNGU3N2Q4ZTFiYWU2ZWZiNzE4NmIyYmYyMGM4ZDkyMWM4ZDU4NmNkNmM0MWZjMzA3ZDJiNGYifQ==', '', '', 2, 0, '2018-09-28 02:11:45', '2018-09-27 18:11:45', NULL),
(5, 'ord005', 12, 'eyJpdiI6ImhwZFFZVGJHWjBaRHBPTmRZVDdNbVE9PSIsInZhbHVlIjoiMldpSXg2b0hcL2dBeGx3QU1MakgyaXc9PSIsIm1hYyI6IjgyMGY4ODg3Mzk5NGY5OTliMTY5Zjc0ZmZlYjE0MmIzOTQ5MDkyOWFlMjBiNjY5OWYyYzkyMWY1ZTEwMTI2M2IifQ==', '', '', 1, 0, '2018-09-25 04:33:05', '2018-09-24 20:33:05', NULL),
(6, 'ord006', 6, 'eyJpdiI6IkF1S0JCdjZpRDEwa0JlTGlnK29UQXc9PSIsInZhbHVlIjoiZ0lqNndocTdXd083RVFFVFwvMXg1b0E9PSIsIm1hYyI6Ijg0ZGFkMGZjYmYxNTVlNDk2YWRlOTE5MjNjZTM4MzFjODI5NDRhZDQwYTgzZDg3YTI2OGVhMWRlNTdhMjlkYzcifQ==', '', '', 1, 0, '2018-10-05 11:16:49', '2018-10-05 03:16:49', NULL),
(7, 'ord007', 6, 'eyJpdiI6InVtUGQ0aXRubDVMdE1WNG54TGtqd1E9PSIsInZhbHVlIjoidWhtcTFhWU5tbmI5NHREVXQ0SFYxZz09IiwibWFjIjoiNTFjZDNiZmY3MmY1NWQ0NDUyNGRkZmRhODVjNmU2NTI0MTZkN2Y1MjBmMDUxZmQ3ZDhhM2IyNmQwM2RlOWY2MCJ9', '', '', 0, 0, '2018-10-06 06:10:08', '2018-10-06 06:10:08', NULL),
(8, 'ord008', 6, 'eyJpdiI6InlyVXVGUHd6RWVFbkdlNnpsekVIQWc9PSIsInZhbHVlIjoiRFJnbHo2SVFQY0dvd2oyTFRaSW9DQT09IiwibWFjIjoiMTA0ODk0ZjM0NWFhYzY3ZjIxYzkyYjZmMGFmYjQzNGUxYjNhMWVjNzE3NTk4MjNlOTY3Y2JiMjE5ZDg2ZDcwZSJ9', '', '', 2, 0, '2018-10-13 03:36:47', '2018-10-12 19:36:47', NULL),
(9, 'ord009', 4, 'eyJpdiI6IlA4enFkbEwrNDRZRXNoT1owa0xJbEE9PSIsInZhbHVlIjoicFBTQWVLNysxQ0VQeGo1STRJNUFkUT09IiwibWFjIjoiYzA4NDIzOTIzNDAyYWYxYTgzOTM2NTcxZTMwYjM5YjNjMDZhZGJkNmVjNjcwZmFmM2E5ODEyOGE4YmY5Y2E1MyJ9', '', '', 0, 0, '2018-10-08 03:57:40', '2018-10-08 03:57:40', NULL),
(10, 'ord010', 6, 'eyJpdiI6InVTVE8yXC9iUmxaVHdzckJDUnJlVVFnPT0iLCJ2YWx1ZSI6IlNuWTk0MzNHRlYyaVh0N3p3enNQSHc9PSIsIm1hYyI6IjRhYTg3ZjdlOTExMDY5NzcyMGE5NTQ2YzY4ODAwMzc2OGY1YWU0ZTEzYjQxZTI2NmNiMmMwYWEyMTU1MDllY2UifQ==', '', '', 2, 0, '2018-10-08 04:52:19', '2018-10-08 04:52:19', NULL),
(11, 'ord011', 6, 'eyJpdiI6InE2VWNOU2src3F2NzlBdkQ3OWthTlE9PSIsInZhbHVlIjoiWlwvYjFiSVwvanhxTHBwdVBhb3Y2M0pRPT0iLCJtYWMiOiIzMmQ4ODEzY2JmYTQ3MTRhNWNiMjdlMTA1YmU2YjMxMGU5NmQ3YTIyZDdjOTJjZTJmYmNkNGU3YTQyZTgyNTNmIn0=', '', '', 2, 0, '2018-10-08 04:58:39', '2018-10-08 04:58:39', NULL),
(12, 'ord012', 6, 'eyJpdiI6IlMzSWdDOVF6UG83c0ZMZHFBaWFvUFE9PSIsInZhbHVlIjoiNFVSOEw4UUFGbXdzRkFQQ3l4QzJsQT09IiwibWFjIjoiNzFiNmNlMjAwNTgxOGMxOWZiMjJiY2IwOWZlZGZlMjZhOTdjOWRkZmQzYTA1OGE2NWRkNThiZDA0NzFlNjBiYiJ9', '', '', 2, 0, '2018-10-08 05:06:29', '2018-10-08 05:06:29', NULL),
(13, 'ord013', 6, 'eyJpdiI6Inl3Qjk1N1JISDdnclJkREQrTlNTQkE9PSIsInZhbHVlIjoieGdRczBNQlwvTGVUN3orajBtMEtlV1E9PSIsIm1hYyI6ImU3ODBmY2I2YTg5ODY2YzUyMTFmY2ZlZThlZDQ2MWNjOWRlMzZiZjU2YTFmM2FhZjM2NDRiZmM1YWM5MjI2NTIifQ==', '', '', 2, 0, '2018-10-08 05:11:00', '2018-10-08 05:11:00', NULL),
(14, 'ord014', 6, 'eyJpdiI6IjRQVGV2bDhEZHcyRDVTY0p0UVN0eWc9PSIsInZhbHVlIjoiS01BdmxlNDlHVUt5alZHRGpqTmVVZz09IiwibWFjIjoiMjllMGFiZWFkNzJjMTdlMTVjZTcxNTk2YzFjNmI1OTUzMTdlNTQyN2VhYTlmNWVkYTZjOTE0NmNiNDk4YmEzYiJ9', '', '', 0, 0, '2018-10-08 06:22:23', '2018-10-08 06:22:23', NULL),
(15, 'ord015', 6, 'eyJpdiI6ImduRzA4VDlHMkh5cUZyUWhvRENQSUE9PSIsInZhbHVlIjoidGFpR2hlUEVoTWNjWU16UGhVSVkxUT09IiwibWFjIjoiNDQzMDE1MjU5ZWUyZDJmNzdkY2IwOWQzN2UzODlhNmFjYjU1ZTBjNmE2OTg1YzFkZWU3Yjc4NzA5ZDcwODhkOCJ9', '', '', 2, 0, '2018-10-08 06:38:15', '2018-10-08 06:38:15', NULL),
(16, 'ord016', 6, 'eyJpdiI6InljdytwT3hHS1g3ekNPU2srXC9CVGhBPT0iLCJ2YWx1ZSI6IjRDc3d6aHpHVzhhdCtSTHRcL25iWXZnPT0iLCJtYWMiOiIyNDBiZWUwZDA4ZDNhNzMxZjc5NmViMjBiMGE1MTdhMDYyNzRlZGM3MjRiNmQ3NTgzY2NlMmYxMzM5OGZjZTYwIn0=', '', '', 2, 0, '2018-10-08 07:00:00', '2018-10-08 07:00:00', NULL),
(17, 'ord017', 6, 'eyJpdiI6ImxPUk1rZWZLcW9BVFJVYlwvbjlRVkxRPT0iLCJ2YWx1ZSI6IkRwUU14TWNaYUs1N2x5OG10T2oxYVE9PSIsIm1hYyI6IjJkZDg4MWJmMjM5Nzc5ZDg1ZGU3ZDE5NTM1MmE3YjkyMjFlMWQ3YjhiNjEzNzQwNDZhYTFiMzFkNGVjODEwYjkifQ==', '', '', 2, 0, '2018-10-08 07:44:45', '2018-10-08 07:44:45', NULL),
(18, 'ord018', 6, 'eyJpdiI6IkNtZ1NFVEcrVDFTUVVodTUydlpwUmc9PSIsInZhbHVlIjoiYVJvN242VmlWN01XYktWZkF2YkdiUT09IiwibWFjIjoiYjUwYTU3MWQwYzA0MTRhOWZiZGRhMWYyYjQ5MWZmNWMzYWU5ZTcwOWVlNTk2Y2QxOTYyYmUxMzJkNzFjMDlhMyJ9', '', '', 2, 0, '2018-10-08 07:52:38', '2018-10-08 07:52:38', NULL),
(19, 'ord019', 6, 'eyJpdiI6InBZODQxalo5eDVoQ2NFSnc5YXRJMWc9PSIsInZhbHVlIjoiaVVzQzhiSjBGakROdTdCTU56bTM0UT09IiwibWFjIjoiOGMxMjI2YzJkMzdiZDNjZTk0OGIxYzU3NmU0ZDg2NmQ5NjE5ZWVhNzRiNmJmMDkwMmMzNDgyMzU3YjczMWFlZiJ9', '', '', 0, 0, '2018-10-08 09:16:58', '2018-10-08 09:16:58', NULL),
(20, 'ord020', 6, 'eyJpdiI6ImxlUmV6NGx3U09iXC9iSUw3UDJoQ3d3PT0iLCJ2YWx1ZSI6Ims3Tkd4TjhYS2E3Z242ZGQ1VE5wbnc9PSIsIm1hYyI6IjQwODY4NTE2MzcwYjlhNjkxMTNmY2UwYTg1NTM2YWYyZDcyNzczODdlN2NlNThlMzUyOTEwYzNmYzM5YzgwNzQifQ==', '', '', 2, 0, '2018-10-08 11:18:29', '2018-10-08 11:18:29', NULL),
(21, 'ord021', 6, 'eyJpdiI6Im11VThhUGw3dlJ6Y3o2emdWUzgzZ3c9PSIsInZhbHVlIjoiRXlBK3M3TVdEckVhTXFnUmtNUUJaQT09IiwibWFjIjoiNjIzOGMzOWNkYzVlODI2NDI4MGUxM2JkY2E0YjZkNDQ5MTI2MWU3MmE0NzgxODk2NTE3NzhkNjM1NGI1M2MzMyJ9', '', '', 2, 0, '2018-10-08 11:33:06', '2018-10-08 11:33:06', NULL),
(22, 'ord022', 6, 'eyJpdiI6IkhGZFlSdmYwdHJpWlBhd3NGQmNNUVE9PSIsInZhbHVlIjoiWTNoSDVzVWhsYlorTzRHK1BGK3ZzZz09IiwibWFjIjoiMjg4NTgwOWI5ZGM4OGY3YWE2NTU2ZDQzNjdhOTE4NzE1MGZjZGE4NjE2MGIwOTA2ZjQ5MGNkOTdkYjFlZjNlNSJ9', NULL, NULL, 0, 0, '2018-10-13 04:09:04', '2018-10-13 05:40:59', NULL),
(23, 'ord023', 6, 'eyJpdiI6IjZDakFNSXR4NVAydW9NbkhCb0diMnc9PSIsInZhbHVlIjoidDZvTG9BSUtxOUVpSjNSb1NJMWhWUT09IiwibWFjIjoiYzBhODA5N2EzNWM5ODMwZTQ2NWZjMWU4NTdiY2Q3M2FjYThjZTBiZDc1MDdhY2FmMGU5OTQ0MzUwMDRmZDQyNSJ9', NULL, NULL, 0, 0, '2018-10-13 05:55:27', '2018-10-13 05:55:27', NULL),
(24, 'ord024', 6, 'eyJpdiI6IitXZ2hadEs5RDBIeHJLdzhUQUdFY0E9PSIsInZhbHVlIjoiOUFzaDVCXC9UeE8xT3prYU5cL0pIRFdBPT0iLCJtYWMiOiI3NmRhM2Q5MDVlMWVhZjdkYTI0ZGQzZTQ1NmFlYjk1NzY1ZWI0YzE1MzJiZTMzNmQxNTBiMjUwOGVmNDcyNWI5In0=', NULL, NULL, 1, 0, '2018-10-13 05:56:06', '2018-10-13 21:36:13', NULL),
(25, 'ord025', 6, 'eyJpdiI6ImF3Qm9kZzFacEg4XC9RYVYxa3I1NUNnPT0iLCJ2YWx1ZSI6IjVPcW1zbTI1TWw5d2hqU0Zld3dzYlE9PSIsIm1hYyI6ImMxMDcwYTVhYzEwZGM1ZjBkODJjMmNiMmI5MjY4NDUwMWE2ZGMxOTdhOTYwNjViZjI0NjBlMmY5MTZmNjQxOTQifQ==', '639+329392939', 'Catarman Northern Samar', 0, 0, '2018-10-17 16:46:28', '2018-10-17 16:46:28', NULL),
(26, 'ord026', 6, 'eyJpdiI6InRYa0ZSQ05TOU1aWVVrRkROZE5EVXc9PSIsInZhbHVlIjoia2crbzhZb3dlQjhYRnZRXC9BSGhVVnc9PSIsIm1hYyI6ImEyNWU4OGJkNTZkYWU0ZWY5ZTlmMWQ3ZmM2ZmNkZTc1ZmY3NDY5ZjJlZGY2YzMzYjM0ZDg1N2NjNGJjMTdjMjcifQ==', NULL, NULL, 2, 0, '2018-10-21 01:26:10', '2018-10-23 01:57:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `size_id` int(10) NOT NULL DEFAULT '1',
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `product_id`, `sub_category_id`, `size_id`, `quantity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 15, 0, 0, 4, '2018-08-19 16:43:17', '2018-08-19 16:43:17', NULL),
(2, 1, 7, 0, 0, 1, '2018-08-19 16:43:17', '2018-08-19 16:43:17', NULL),
(3, 2, 8, 0, 0, 3, '2018-09-23 11:24:42', '2018-08-19 16:43:18', NULL),
(4, 5, 15, 0, 0, 3, '2018-09-25 04:25:54', '2018-08-20 21:05:03', NULL),
(5, 3, 15, 0, 0, 5, '2018-08-21 22:17:43', '2018-08-21 22:17:43', NULL),
(6, 4, 15, 0, 0, 3, '2018-08-21 22:23:18', '2018-08-21 22:23:18', NULL),
(7, 2, 9, 0, 0, 2, '2018-09-23 11:28:43', '2018-09-01 22:53:45', NULL),
(8, 6, 9, 0, 0, 1, '2018-10-05 02:49:24', '2018-10-05 02:49:24', NULL),
(9, 6, 14, 0, 0, 2, '2018-10-05 02:49:24', '2018-10-05 02:49:24', NULL),
(10, 6, 10, 0, 0, 2, '2018-10-05 02:49:25', '2018-10-05 02:49:25', NULL),
(11, 6, 15, 0, 0, 1, '2018-10-05 02:49:25', '2018-10-05 02:49:25', NULL),
(12, 6, 8, 0, 0, 1, '2018-10-05 02:49:26', '2018-10-05 02:49:26', NULL),
(13, 6, 11, 0, 0, 1, '2018-10-05 02:49:26', '2018-10-05 02:49:26', NULL),
(14, 7, 14, 0, 0, 1, '2018-10-06 06:10:08', '2018-10-06 06:10:08', NULL),
(15, 8, 15, 0, 0, 2, '2018-10-08 03:56:36', '2018-10-08 03:56:36', NULL),
(16, 8, 8, 0, 0, 1, '2018-10-08 03:56:36', '2018-10-08 03:56:36', NULL),
(17, 9, 10, 0, 0, 3, '2018-10-08 03:57:40', '2018-10-08 03:57:40', NULL),
(18, 9, 14, 0, 0, 2, '2018-10-08 03:57:40', '2018-10-08 03:57:40', NULL),
(19, 9, 7, 0, 0, 6, '2018-10-08 03:57:41', '2018-10-08 03:57:41', NULL),
(20, 9, 13, 0, 0, 5, '2018-10-08 03:57:41', '2018-10-08 03:57:41', NULL),
(21, 9, 15, 0, 0, 3, '2018-10-08 03:57:41', '2018-10-08 03:57:41', NULL),
(22, 10, 15, 0, 0, 2, '2018-10-08 04:52:19', '2018-10-08 04:52:19', NULL),
(23, 10, 7, 0, 0, 1, '2018-10-08 04:52:19', '2018-10-08 04:52:19', NULL),
(24, 10, 14, 0, 0, 2, '2018-10-08 04:52:19', '2018-10-08 04:52:19', NULL),
(25, 11, 15, 0, 0, 1, '2018-10-08 04:58:39', '2018-10-08 04:58:39', NULL),
(26, 12, 15, 0, 0, 3, '2018-10-08 05:06:29', '2018-10-08 05:06:29', NULL),
(27, 13, 15, 0, 0, 1, '2018-10-08 05:11:00', '2018-10-08 05:11:00', NULL),
(28, 14, 15, 0, 0, 1, '2018-10-08 06:22:24', '2018-10-08 06:22:24', NULL),
(29, 14, 7, 0, 0, 1, '2018-10-08 06:22:24', '2018-10-08 06:22:24', NULL),
(30, 15, 12, 0, 0, 1, '2018-10-08 06:38:15', '2018-10-08 06:38:15', NULL),
(31, 15, 7, 0, 0, 1, '2018-10-08 06:38:15', '2018-10-08 06:38:15', NULL),
(32, 16, 10, 0, 0, 1, '2018-10-08 07:00:00', '2018-10-08 07:00:00', NULL),
(33, 16, 13, 0, 0, 1, '2018-10-08 07:00:00', '2018-10-08 07:00:00', NULL),
(34, 16, 7, 0, 0, 1, '2018-10-08 07:00:00', '2018-10-08 07:00:00', NULL),
(35, 17, 8, 0, 0, 1, '2018-10-08 07:44:45', '2018-10-08 07:44:45', NULL),
(36, 17, 11, 0, 0, 1, '2018-10-08 07:44:45', '2018-10-08 07:44:45', NULL),
(37, 17, 12, 0, 0, 1, '2018-10-08 07:44:45', '2018-10-08 07:44:45', NULL),
(38, 18, 9, 0, 0, 1, '2018-10-08 07:52:38', '2018-10-08 07:52:38', NULL),
(39, 18, 8, 0, 0, 1, '2018-10-08 07:52:38', '2018-10-08 07:52:38', NULL),
(40, 19, 9, 0, 0, 1, '2018-10-08 09:16:58', '2018-10-08 09:16:58', NULL),
(41, 19, 14, 0, 0, 2, '2018-10-08 09:16:58', '2018-10-08 09:16:58', NULL),
(42, 20, 11, 0, 0, 1, '2018-10-08 11:18:29', '2018-10-08 11:18:29', NULL),
(43, 21, 7, 0, 0, 1, '2018-10-08 11:33:06', '2018-10-08 11:33:06', NULL),
(44, 21, 10, 0, 0, 1, '2018-10-08 11:33:06', '2018-10-08 11:33:06', NULL),
(45, 21, 13, 0, 0, 1, '2018-10-08 11:33:06', '2018-10-08 11:33:06', NULL),
(46, 21, 15, 0, 0, 1, '2018-10-08 11:33:06', '2018-10-08 11:33:06', NULL),
(47, 22, 15, 0, 0, 5, '2018-10-13 04:09:04', '2018-10-13 04:09:04', NULL),
(48, 23, 10, 0, 0, 2, '2018-10-13 05:55:27', '2018-10-13 05:55:27', NULL),
(49, 24, 7, 0, 0, 1, '2018-10-13 05:56:06', '2018-10-13 05:56:06', NULL),
(50, 25, 15, 0, 0, 1, '2018-10-17 16:46:28', '2018-10-17 16:46:28', NULL),
(51, 26, 15, 1, 3, 2, '2018-10-21 01:26:10', '2018-10-21 01:26:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `image` text,
  `image_serialize` text,
  `barcode` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `gender_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `stock` int(10) NOT NULL,
  `used_stock` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `image`, `image_serialize`, `barcode`, `name`, `description`, `category_id`, `gender_id`, `price`, `stock`, `used_stock`, `created_at`, `updated_at`, `deleted_at`) VALUES
(20, '1 blue_1542987150.png', NULL, 'pro1', 'Hanging Blouse', '', 9, 18, '230.00', 20, 0, '2018-11-23 07:32:30', '2018-11-23 07:32:30', NULL),
(21, '2 white_1542987217.png', NULL, 'pro21', 'Wide Armed Blouse', '', 9, 18, '230.00', 20, 0, '2018-11-23 07:33:37', '2018-11-23 07:33:37', NULL),
(22, '3 qwe_1542987261.png', NULL, 'pro22', 'Common Blouse', '', 9, 18, '320.00', 20, 0, '2018-11-23 07:34:21', '2018-11-23 07:34:21', NULL),
(23, '4_1542987350.png', NULL, 'pro23', 'Floral Blouse', '', 9, 18, '500.00', 20, 0, '2018-11-23 07:35:50', '2018-11-23 07:35:50', NULL),
(24, '5_1542987390.png', NULL, 'pro24', 'See Through Blouse', '', 9, 18, '1000.00', 17, 0, '2018-11-23 07:36:30', '2018-11-23 07:36:30', NULL),
(25, '1_1542987443.png', NULL, 'pro25', 'Maong Jacket', '', 5, 19, '1200.00', 10, 0, '2018-11-23 07:37:23', '2018-11-23 07:37:23', NULL),
(26, '2 g_1542987486.png', NULL, 'pro26', 'Fitted Jacket', '', 5, 18, '800.00', 13, 0, '2018-11-23 07:38:06', '2018-11-23 07:38:06', NULL),
(27, '3 black_1542987567.png', NULL, 'pro27', 'Korean Jacket', '', 5, 19, '745.00', 15, 0, '2018-11-23 07:39:27', '2018-11-23 07:39:27', NULL),
(28, '4 white_1542987629.png', NULL, 'pro28', 'Hoodie Jacket', '', 5, 19, '600.00', 24, 0, '2018-11-23 07:40:29', '2018-11-23 07:40:29', NULL),
(29, '5 fur_1542987659.png', NULL, 'pro29', 'Fur Jacket', '', 5, 19, '1500.00', 10, 0, '2018-11-23 07:40:59', '2018-11-23 07:40:59', NULL),
(30, '1 black_1542987719.png', NULL, 'pro30', 'Plain Long Sleeve', '', 10, 19, '300.00', 13, 0, '2018-11-23 07:41:59', '2018-11-23 07:41:59', NULL),
(31, '2 white_1542987752.png', NULL, 'pro31', 'Korean Type Long Sleeve', '', 10, 19, '400.00', 12, 0, '2018-11-23 07:42:32', '2018-11-23 07:42:32', NULL),
(32, '4 sky blue_1542987797.png', NULL, 'pro32', 'Beep Long Sleeve', '', 10, 19, '250.00', 29, 0, '2018-11-23 07:43:17', '2018-11-23 07:43:17', NULL),
(33, '5_1542987847.png', NULL, 'pro33', 'Long Sleeve with Button', '', 10, 19, '250.00', 29, 0, '2018-11-23 07:44:07', '2018-11-23 07:44:07', NULL),
(34, '3_1542988222.png', NULL, 'pro34', 'Sun Ray Long Sleeve', '', 10, 17, '320.00', 17, 0, '2018-11-23 07:50:22', '2018-11-23 07:50:22', NULL),
(35, '2 grey_1542988437.png', NULL, 'pro35', 'Plain Polo Shirt', '', 6, 19, '230.00', 22, 0, '2018-11-23 07:53:57', '2018-11-23 07:53:57', NULL),
(36, '3 brown_1542988526.png', NULL, 'pro36', 'Horse Men Polo Shirt', '', 6, 17, '300.00', 15, 0, '2018-11-23 07:55:26', '2018-11-23 07:55:26', NULL),
(37, '4_1542988562.png', NULL, 'pro37', 'Racing Polo Shirt', '', 6, 17, '300.00', 22, 0, '2018-11-23 07:56:02', '2018-11-23 07:56:02', NULL),
(38, '5_1542988598.png', NULL, 'pro38', 'Puly Polo Shirt', '', 6, 18, '250.00', 18, 0, '2018-11-23 07:56:38', '2018-11-23 07:56:38', NULL),
(39, '1_1542988659.png', NULL, 'pro39', 'Outer Coloured Polo Shirt', '', 7, 18, '400.00', 18, 0, '2018-11-23 07:57:39', '2018-11-23 07:57:39', NULL),
(40, '2_1542988702.png', NULL, 'pro40', 'Pittsburgh Polo Shirt', '', 7, 19, '300.00', 17, 0, '2018-11-23 07:58:22', '2018-11-23 07:58:22', NULL),
(41, '3 _1542988732.png', NULL, 'pro41', 'Snake Polo Shirt', '', 7, 19, '450.00', 17, 0, '2018-11-23 07:58:52', '2018-11-23 07:58:52', NULL),
(42, '4_1542988829.png', NULL, 'pro42', 'Liberty Or Death', '', 7, 17, '320.00', 17, 0, '2018-11-23 08:00:29', '2018-11-23 08:00:29', NULL),
(43, '5_1542988863.png', NULL, 'pro43', 'Dub Magician', '', 7, 19, '600.00', 7, 0, '2018-11-23 08:01:03', '2018-11-23 08:01:03', NULL),
(44, '1 yellow_1542988917.png', NULL, 'pro44', 'Drawed Tshirt', '', 8, 17, '200.00', 10, 0, '2018-11-23 08:01:57', '2018-11-23 08:01:57', NULL),
(45, '2 blue_1542988958.png', NULL, 'pro45', 'Take it easy Tshirt', '', 8, 19, '300.00', 14, 0, '2018-11-23 08:02:38', '2018-11-23 08:02:38', NULL),
(46, '3 blue_1542988986.png', NULL, 'pro46', 'Hot Dama Lama', '', 8, 19, '300.00', 15, 0, '2018-11-23 08:03:06', '2018-11-23 08:03:06', NULL),
(47, '4 pink_1542989027.png', NULL, 'pro47', 'Yellow Days', '', 8, 19, '400.00', 13, 0, '2018-11-23 08:03:47', '2018-11-23 08:03:47', NULL),
(48, '5 skyblue_1542989050.png', NULL, 'pro48', 'Green Life Tshirt', '', 8, 19, '400.00', 13, 0, '2018-11-23 08:04:10', '2018-11-23 08:04:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_by_subcategory`
--

CREATE TABLE `product_by_subcategory` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `barcode` varchar(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `stock` int(10) NOT NULL,
  `used_stock` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_by_subcategory`
--

INSERT INTO `product_by_subcategory` (`id`, `image`, `barcode`, `product_id`, `category_id`, `stock`, `used_stock`, `created_at`, `updated_at`, `deleted_at`) VALUES
(9, '1 white_1542987151.png', '', 20, 11, 8, 0, '2018-11-23 07:32:31', '2018-11-23 07:32:31', '0000-00-00 00:00:00'),
(10, '2 blue_1542987218.png', '', 21, 13, 5, 0, '2018-11-23 07:33:38', '2018-11-23 07:33:38', '0000-00-00 00:00:00'),
(11, '3_1542987261.png', '', 22, 11, 10, 0, '2018-11-23 07:34:21', '2018-11-23 07:34:21', '0000-00-00 00:00:00'),
(12, '4 red_1542987350.png', '', 23, 15, 8, 0, '2018-11-23 07:35:51', '2018-11-23 07:35:51', '0000-00-00 00:00:00'),
(13, '5 black_1542987390.png', '', 24, 11, 5, 0, '2018-11-23 07:36:30', '2018-11-23 07:36:30', '0000-00-00 00:00:00'),
(14, '1 black_1542987443.png', '', 25, 11, 5, 0, '2018-11-23 07:37:23', '2018-11-23 07:37:23', '0000-00-00 00:00:00'),
(15, '1 black_1542987486.png', '', 26, 11, 5, 0, '2018-11-23 07:38:06', '2018-11-23 07:38:06', '0000-00-00 00:00:00'),
(16, '4 blue_1542987630.png', '', 28, 13, 4, 0, '2018-11-23 07:40:30', '2018-11-23 07:40:30', '0000-00-00 00:00:00'),
(17, '5 fur g_1542987659.png', '', 29, 21, 5, 0, '2018-11-23 07:40:59', '2018-11-23 07:40:59', '0000-00-00 00:00:00'),
(18, '1 red_1542987719.png', '', 30, 15, 8, 0, '2018-11-23 07:41:59', '2018-11-23 07:41:59', '0000-00-00 00:00:00'),
(19, '2 blue_1542987752.png', '', 31, 13, 10, 0, '2018-11-23 07:42:32', '2018-11-23 07:42:32', '0000-00-00 00:00:00'),
(20, '4 black_1542987797.png', '', 32, 11, 19, 0, '2018-11-23 07:43:17', '2018-11-23 07:43:17', '0000-00-00 00:00:00'),
(21, '5 grey_1542987847.png', '', 33, 11, 19, 0, '2018-11-23 07:44:07', '2018-11-23 07:44:07', '0000-00-00 00:00:00'),
(22, '3 pink_1542988222.png', '', 34, 14, 5, 0, '2018-11-23 07:50:22', '2018-11-23 07:50:22', '0000-00-00 00:00:00'),
(23, '1 green_1542988437.png', '', 35, 22, 12, 0, '2018-11-23 07:53:57', '2018-11-23 07:53:57', '0000-00-00 00:00:00'),
(24, '3 grey_1542988526.png', '', 36, 11, 5, 0, '2018-11-23 07:55:26', '2018-11-23 07:55:26', '0000-00-00 00:00:00'),
(25, '4 orange_1542988562.png', '', 37, 16, 12, 0, '2018-11-23 07:56:02', '2018-11-23 07:56:02', '0000-00-00 00:00:00'),
(26, '5 pink_1542988598.png', '', 38, 14, 6, 0, '2018-11-23 07:56:38', '2018-11-23 07:56:38', '0000-00-00 00:00:00'),
(27, '1 green_1542988659.png', '', 39, 22, 6, 0, '2018-11-23 07:57:39', '2018-11-23 07:57:39', '0000-00-00 00:00:00'),
(28, '2 skyblu_1542988702.png', '', 40, 11, 5, 0, '2018-11-23 07:58:22', '2018-11-23 07:58:22', '0000-00-00 00:00:00'),
(29, '3 black_1542988732.png', '', 41, 11, 5, 0, '2018-11-23 07:58:52', '2018-11-23 07:58:52', '0000-00-00 00:00:00'),
(30, '4 blue_1542988829.png', '', 42, 13, 5, 0, '2018-11-23 08:00:29', '2018-11-23 08:00:29', '0000-00-00 00:00:00'),
(31, '5 white_1542988863.png', '', 43, 11, 5, 0, '2018-11-23 08:01:03', '2018-11-23 08:01:03', '0000-00-00 00:00:00'),
(32, '1 pink_1542988918.png', '', 44, 14, 5, 0, '2018-11-23 08:01:58', '2018-11-23 08:01:58', '0000-00-00 00:00:00'),
(33, '3 purple_1542988986.png', '', 46, 20, 7, 0, '2018-11-23 08:03:06', '2018-11-23 08:03:06', '0000-00-00 00:00:00'),
(34, '4 green_1542989027.png', '', 47, 22, 5, 0, '2018-11-23 08:03:47', '2018-11-23 08:03:47', '0000-00-00 00:00:00'),
(35, '5 green_1542989050.png', '', 48, 22, 5, 0, '2018-11-23 08:04:10', '2018-11-23 08:04:10', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_review`
--

CREATE TABLE `product_review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `rate` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_review`
--

INSERT INTO `product_review` (`id`, `user_id`, `product_id`, `comment`, `rate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 15, '5 kasi sobrang ganda nya. Nkaka inlove <3', 5, '2018-09-16 03:03:03', '2018-09-16 03:03:03', NULL),
(2, 6, 15, 'Its nice but theres a crack in the bottom part.', 4, '2018-09-18 19:06:58', '2018-09-18 19:06:58', NULL),
(3, 6, 8, 'BUUUUUUUU', 4, '2018-09-20 07:31:16', '2018-09-20 07:31:16', NULL),
(5, 6, 8, 'wqeqweqweq', 5, '2018-10-13 03:16:20', '2018-10-13 03:16:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `returned_products`
--

CREATE TABLE `returned_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `buyer_note` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `extension_name` varchar(3) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(191) DEFAULT NULL,
  `phone_number` varchar(12) DEFAULT NULL,
  `random_code` varchar(6) DEFAULT NULL,
  `address` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `extension_name`, `username`, `email`, `password`, `remember_token`, `phone_number`, `random_code`, `address`, `status`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'S', 'A', 'M', 'SAM', 'Admin', 'web_admin@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', '0bq5Y0RLBX9CuBST9M0ncTJdC5Dfysbdj3XT88FhLqg7w1X6evWww8SduUrg', NULL, '', NULL, 1, 'admin', '2018-07-25 17:10:33', '2018-07-25 17:10:33', NULL),
(4, 'U', 'C', 'C', NULL, 'ICP', 'icp@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', 'X0F3Rbqe1ShmOZFIt6TPQk1KOg9HeWfYl98SvK4ZOejidVelx9VdVQPdROpG', NULL, '', NULL, 1, 'icp', '2018-07-25 17:27:24', '2018-07-25 17:27:24', NULL),
(5, 'Store', NULL, 'Manager', NULL, 'Manager', 'store_manager@gmail.com', '$2y$10$BvyQam/KHznW3SRNCr9T9uvp0miAi5gd9d2h.f5pTImmVsrbpSsIS', 'ToCVC1JThdrZhThHbSW4XeC3JjF9gY3j82jKyH9Dve1C31OhIDtKE0isVS5K', NULL, '', NULL, 1, 'manager', '2018-07-25 17:43:53', '2018-07-25 17:43:53', NULL),
(6, 'John', NULL, 'Roncales', NULL, 'jrroncales', 'jrroncales25@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', 'ejQrtbe8nARDspSDCGIMEuYvEXxMqXkXcEgTnvWGxeae8SyLqrkcasnmKGJr', '639452538775', 'JSVKTT', 'blk 11 lot 8 north triangle cielito camarin caloocan city', 1, 'user', '2018-07-25 17:59:44', '2018-10-13 17:41:45', NULL),
(7, 'qwe', NULL, 'qwe', NULL, 'qwe', 'qwe@yahoo.com', '$2y$10$1OBfXJ3sMo7OZRjyGSOM5OAv5u9DD1gRCeXRyT8HtyC78Jhscn/xW', NULL, '01923192311', '', NULL, 0, 'user', '2018-09-01 21:23:56', '2018-09-01 21:23:56', NULL),
(8, 'e2e2', NULL, 'e2e', NULL, '2e2', 'e2e2@yahoo.com', '$2y$10$0GoeVsYSuJjlASB8qDoSmeVY3S/GNVX/y1OPvVvMX3RjObwCW4C3a', NULL, '2', 'JSVKTT', NULL, 0, 'user', '2018-09-01 21:33:46', '2018-09-01 21:33:46', NULL),
(12, 'Richard', NULL, 'Roncales', NULL, 'unno21', 'chadxx1993@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', 'XoUNh0GSBNDAlIpmeeyLwOP5rzdENnSSvlXBPukZCAiZtzwXwJxgjOpAWWeK', '639507353920', 'JSSRG7', NULL, 0, 'user', '2018-09-01 22:40:37', '2018-09-01 22:40:37', NULL),
(18, 'asndansd', NULL, 'nasdna', NULL, 'iwqjweq', 'qwje@yahoo.com', '$2y$10$esfu/Ipxe8BikC8s8kLr0erLiYGjW3ncQJC.bLdgmNUbPxb3A5isS', NULL, '639507353920', 'HDFQLO', NULL, 0, 'user', '2018-09-01 22:51:42', '2018-09-01 22:51:42', NULL),
(19, 'qwehqweiq', NULL, 'qhweuqwe', NULL, 'hwuqeuwe', 'qweqwe@yahoo.com', '$2y$10$88DOAimDVKbkZazfwAvqUub4EFCHHB4k8TcEfQ2Pv4lOIctyohMja', NULL, '639507353920', 'VG8IO0', NULL, 0, 'user', '2018-09-01 22:52:09', '2018-09-01 22:52:09', NULL),
(20, 'dvasdvaos', NULL, 'viavoiaos', NULL, 'vaois', 'vsaio@gmail.com', '$2y$10$mcRv2w5kW0PVMWMXCO6eFuhyHLJmFeT5xfCRtBQGEn.vVz.sPy6y6', NULL, '639507353920', 'W1Y7KU', NULL, 0, 'user', '2018-09-20 07:35:37', '2018-09-20 07:35:37', NULL),
(21, 'Jade', NULL, 'Coral', NULL, 'jade_coral', 'jadecoral@gmail.com', '$2y$10$nZ55/YqUoTMTsCnBb3ftEeKp3IxVXoZJQR3qtNPFTJZnYfKuMAOGu', 'eLwmcny7jdENBQStJkicZ6JJL4ubNvCXlx6pkY204y4E4njryEsgSNdHtOij', '639507353920', '41J5YC', NULL, 0, 'user', '2018-10-02 09:09:03', '2018-10-02 09:09:03', NULL),
(22, 'Tin', NULL, 'Carpio', NULL, 'tincarpio', 'tincarpio@gmail.com', '$2y$10$usn8W/xkBBTsT56IA0LhbejwlbJaUuZnxXAwXvXCd.0/nxERRjudq', 'RUHgLDaBawnyNJQWZMDhHgTv2mBhmh6wCSDwLBR0DTHKaZTEb5W0AOuVcRvJ', '639507353920', '859Z2C', NULL, 0, 'user', '2018-10-02 09:10:26', '2018-10-02 09:10:26', NULL),
(23, 'Joseph', NULL, 'Malaga', NULL, 'joseph', 'malaga@gmail.com', '$2y$10$3YLBHu5gyLrV0g1IO7K.TuESaBYX5rhBqR.DfWK/GIj0.9m1vaKIu', 'JWjkRL7ZFFulwjfqZljmxIwR93iQRahV5MK2ohVS15FDKbA71MpooHfxR9N0', '639507353920', 'M8EIWC', NULL, 1, 'user', '2018-10-02 09:15:44', '2018-10-02 09:15:53', NULL),
(24, 'Mac', 'A', 'Acebes', NULL, 'allennegro', 'negronegro@gmail.com', '$2y$10$09IsmYTkmmJ5nwAVUC2.dOD8o5OP9i7iozCb3hjn8TOVMWHHr6b4C', NULL, NULL, NULL, NULL, 1, 'cashier', '2018-10-02 10:11:44', '2018-10-02 10:11:44', NULL),
(26, 'eohan', 'eohan', 'eohan', NULL, 'eohan', 'eohan@gmail.com', '$2y$10$2A2/bYH7TaB.3lYmKonnsuuVaWJSKQTNoggQ1Hj7/5Q79OHh.ZuIW', NULL, NULL, NULL, NULL, 1, 'cashier', '2018-10-05 04:01:58', '2018-10-05 04:01:58', NULL),
(27, 'regerge', NULL, 'rgergege', NULL, 'gergege', 'uih1ui2h31@yahoo.com', '$2y$10$UDJdv0hSErnR.iFtlzQhqu6siJiI2gy4mSuUPrRc1IDLxv1ICaX8q', NULL, '639507353920', 'RXRR4U', NULL, 1, 'user', '2018-10-21 00:44:32', '2018-10-21 00:48:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `post_code` varchar(255) NOT NULL,
  `phone_number` varchar(13) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`id`, `user_id`, `full_name`, `address`, `post_code`, `phone_number`, `created_at`, `updated_at`) VALUES
(1, 6, 'jomari germino', 'Blk 11 Lot 19921', '1391jdewq', '639423123121', '2018-09-05 06:59:10', '0000-00-00 00:00:00'),
(2, 6, 'Mac Allen Acebes', 'aosdmcmo2mfon n jw9ejejjvj  2123 fef', 'qwdiq29  8weq8e jqwjeq21', '639423123121', '2018-09-05 07:07:38', '0000-00-00 00:00:00'),
(723, 6, 'sf sfsdf s', '23 adeada', '2323', '13131313', '2018-09-16 00:01:05', '2018-09-16 00:01:05'),
(724, 6, 'fs s s f', '32 sdasdsfs', '223', '13131346', '2018-09-16 00:02:21', '2018-09-16 00:02:21'),
(725, 6, 'sfsf sfsf', '23213 asdasda', '2313', '13478765', '2018-09-16 00:03:13', '2018-09-16 00:03:13'),
(726, 6, 'dasdasd', 'sdasdasd', 'ada', '2131', '2018-09-16 00:20:49', '2018-09-16 00:20:49'),
(727, 6, 'huuuuuuuuuuuuuuuu', 'sdasdasd', 'ada', '2131', '2018-09-16 00:39:21', '2018-09-16 00:39:21'),
(728, 6, 'huuuuuuu', 'sdasdasd', 'ada', '2131', '2018-09-16 00:41:02', '2018-09-16 00:41:02'),
(729, 6, 'sf sfsdf s', '23 adeada', '2323', '42141241', '2018-09-16 00:44:05', '2018-09-16 00:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `user_id`, `product_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(53, 6, 9, '2018-10-21 07:53:08', '2018-10-20 23:53:08', '2018-10-20 23:53:08'),
(54, 6, 9, '2018-10-21 07:53:48', '2018-10-20 23:53:48', '2018-10-20 23:53:48'),
(55, 6, 13, '2018-10-21 08:53:03', '2018-10-21 00:53:03', '2018-10-21 00:53:03'),
(56, 6, 9, '2018-10-21 08:54:51', '2018-10-21 00:54:51', '2018-10-21 00:54:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audit_trails`
--
ALTER TABLE `audit_trails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_details`
--
ALTER TABLE `cart_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_trails`
--
ALTER TABLE `log_trails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notify_order`
--
ALTER TABLE `notify_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_by_subcategory`
--
ALTER TABLE `product_by_subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_review`
--
ALTER TABLE `product_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `returned_products`
--
ALTER TABLE `returned_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audit_trails`
--
ALTER TABLE `audit_trails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `cart_details`
--
ALTER TABLE `cart_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `log_trails`
--
ALTER TABLE `log_trails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `notify_order`
--
ALTER TABLE `notify_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `product_by_subcategory`
--
ALTER TABLE `product_by_subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `product_review`
--
ALTER TABLE `product_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `returned_products`
--
ALTER TABLE `returned_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=730;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
