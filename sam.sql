-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2019 at 06:20 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sam`
--

-- --------------------------------------------------------

--
-- Table structure for table `audit_trails`
--

CREATE TABLE `audit_trails` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audit_trails`
--

INSERT INTO `audit_trails` (`id`, `user_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 4, 'Icp management added a product, Dress 2 with a price of 1499', '2018-12-30 18:21:29', '2018-12-30 18:21:29'),
(2, 4, 'Icp management added a product, Dress 3 with a price of 999', '2018-12-30 18:23:24', '2018-12-30 18:23:24'),
(3, 4, 'Icp management added a product, Dress 4 with a price of 1299', '2018-12-30 18:25:17', '2018-12-30 18:25:17'),
(4, 4, 'Icp management added a product, Pants 1 with a price of 1399', '2018-12-30 18:28:38', '2018-12-30 18:28:38'),
(5, 4, 'Icp management added a product, Pants 2 with a price of 599', '2018-12-30 18:30:59', '2018-12-30 18:30:59'),
(6, 4, 'Icp management added a product, Pants 3 with a price of 399', '2018-12-30 18:33:17', '2018-12-30 18:33:17'),
(7, 4, 'Icp management added a product, Pants 4 with a price of 1599', '2018-12-30 18:35:51', '2018-12-30 18:35:51'),
(8, 4, 'Icp management added a product, Pants 5 with a price of 999', '2018-12-30 18:37:53', '2018-12-30 18:37:53'),
(9, 4, 'Icp management added a product, Short 1 with a price of 449', '2018-12-30 18:41:11', '2018-12-30 18:41:11'),
(10, 4, 'Icp management added a product, Short 2 with a price of 449', '2018-12-30 18:43:17', '2018-12-30 18:43:17'),
(11, 4, 'Icp management added a product, Short 3 with a price of 699', '2018-12-30 18:45:57', '2018-12-30 18:45:57'),
(12, 4, 'Icp management added a product, Short 4 with a price of 699', '2018-12-30 18:51:43', '2018-12-30 18:51:43'),
(13, 4, 'Icp management added a product, Short 5 with a price of 899', '2018-12-30 18:53:31', '2018-12-30 18:53:31'),
(14, 4, 'Icp management added a product, Skirt 1 with a price of 899', '2018-12-30 18:55:48', '2018-12-30 18:55:48'),
(15, 4, 'Icp management added a product, Skirt 2 with a price of 599', '2018-12-30 18:57:45', '2018-12-30 18:57:45'),
(16, 4, 'Icp management added a product, Skirt 3 with a price of 999', '2018-12-30 18:59:20', '2018-12-30 18:59:20'),
(17, 4, 'Icp management added a product, Skirt 4 with a price of 1299', '2018-12-30 19:01:17', '2018-12-30 19:01:17'),
(18, 4, 'Icp management added a product, Skirt 5 with a price of 749', '2018-12-30 19:03:07', '2018-12-30 19:03:07'),
(19, 4, 'Icp management added a product, Tshirt 1 with a price of 649', '2018-12-30 19:06:11', '2018-12-30 19:06:11');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 54, '2018-12-31 00:30:49', '2018-12-31 00:30:49', NULL),
(2, 55, '2018-12-31 00:30:51', '2018-12-31 00:30:51', NULL),
(3, 56, '2018-12-31 00:41:30', '2018-12-31 00:41:30', NULL),
(4, 57, '2018-12-31 00:41:31', '2018-12-31 00:41:31', NULL),
(5, 58, '2018-12-31 00:48:54', '2018-12-31 00:48:54', NULL),
(6, 59, '2018-12-31 00:48:56', '2018-12-31 00:48:56', NULL),
(7, 60, '2018-12-31 01:07:24', '2018-12-31 01:07:24', NULL),
(8, 61, '2018-12-31 01:07:25', '2018-12-31 01:07:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart_details`
--

CREATE TABLE `cart_details` (
  `id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `size_id` int(7) NOT NULL DEFAULT '1',
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'small', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(2, 'medium\r\n', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(3, 'large', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(4, 'extra large', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(5, 'jacket', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(6, 'polo shirt', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(7, 'three fourth\'s', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(8, 't-shirt', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(9, 'blouse', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(10, 'long sleeve', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(11, 'others', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(12, 'yellow', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(13, 'blue', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(14, 'pink', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(15, 'red', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(16, 'orange', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(17, 'male', 'gender', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(18, 'female', 'gender', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(19, 'unisex', 'gender', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(20, 'purple', 'color', '2018-10-13 18:37:58', '2018-10-13 18:37:58'),
(21, 'brown', 'color', '2018-11-23 07:38:22', '2018-11-23 07:38:22'),
(22, 'green', 'color', '2018-11-23 07:51:02', '2018-11-23 07:51:02'),
(23, 'skirt', 'product', '2018-11-26 21:29:27', '2018-11-26 21:29:27'),
(24, 'pants', 'product', '2018-11-26 21:30:14', '2018-11-26 21:30:14'),
(25, 'short', 'product', '2018-11-26 21:29:27', '2018-11-26 21:29:27'),
(26, 'dress', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48');

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `logo` varchar(191) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(191) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`id`, `logo`, `name`, `email`, `contact`, `created_at`, `updated_at`) VALUES
(1, 'asd', 'Shopping Assistant Mirror', 'internsprog@gmail.com', '123', '2018-07-26 01:51:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `log_trails`
--

CREATE TABLE `log_trails` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `log_date` date NOT NULL,
  `log_in` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_out` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_trails`
--

INSERT INTO `log_trails` (`id`, `user_id`, `log_date`, `log_in`, `log_out`, `created_at`, `updated_at`) VALUES
(1, 4, '2018-12-31', '2018-12-31 02:15:03', '2018-12-31 03:06:57', '2018-12-31 03:06:57', '2018-12-30 19:06:57');

-- --------------------------------------------------------

--
-- Table structure for table `mirror_logs`
--

CREATE TABLE `mirror_logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notify_order`
--

CREATE TABLE `notify_order` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notify_order`
--

INSERT INTO `notify_order` (`id`, `order_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 2, 'Your order has been delivered by our courier.', '2018-09-24 22:05:10', '2018-09-24 22:05:10'),
(2, 1, 'Your order has been shipped by our courier.', '2018-09-24 22:05:21', '2018-09-24 22:05:21'),
(3, 4, 'Your order has been shipped by our courier.', '2018-09-27 18:10:29', '2018-09-27 18:10:29'),
(4, 4, 'Your order has been delivered by our courier.', '2018-09-27 18:11:45', '2018-09-27 18:11:45'),
(5, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:16:49', '2018-10-05 03:16:49'),
(6, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:17:03', '2018-10-05 03:17:03'),
(7, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:17:07', '2018-10-05 03:17:07'),
(8, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:17:24', '2018-10-05 03:17:24'),
(9, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:47', '2018-10-12 19:36:47'),
(10, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:50', '2018-10-12 19:36:50'),
(11, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:58', '2018-10-12 19:36:58'),
(12, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:59', '2018-10-12 19:36:59'),
(13, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:59', '2018-10-12 19:36:59'),
(14, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:37:00', '2018-10-12 19:37:00'),
(15, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:37:01', '2018-10-12 19:37:01'),
(16, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:37:01', '2018-10-12 19:37:01'),
(17, 22, 'Your order is being processed at the moment.', '2018-10-13 04:09:04', '2018-10-13 04:09:04'),
(18, 22, 'Your order has been shipped by our courier.', '2018-10-13 05:38:45', '2018-10-13 05:38:45'),
(19, 22, 'Your order has been shipped by our courier.', '2018-10-13 05:38:46', '2018-10-13 05:38:46'),
(20, 22, 'Your order has been shipped by our courier.', '2018-10-13 05:38:47', '2018-10-13 05:38:47'),
(21, 22, 'Your order is being processed at the moment.', '2018-10-13 05:40:59', '2018-10-13 05:40:59'),
(22, 23, 'Your order is being processed at the moment.', '2018-10-13 05:55:28', '2018-10-13 05:55:28'),
(23, 24, 'Your order is being processed at the moment.', '2018-10-13 05:56:06', '2018-10-13 05:56:06'),
(24, 24, 'Your order has been shipped by our courier.', '2018-10-13 21:36:13', '2018-10-13 21:36:13'),
(25, 25, 'Your order is being processed at the moment.', '2018-10-17 16:46:29', '2018-10-17 16:46:29'),
(26, 26, 'Your order is being processed at the moment.', '2018-10-21 01:26:10', '2018-10-21 01:26:10'),
(27, 26, 'Your order has been shipped by our courier.', '2018-10-21 02:19:13', '2018-10-21 02:19:13'),
(28, 26, 'Your order has been delivered by our courier.', '2018-10-23 01:57:10', '2018-10-23 01:57:10'),
(29, 27, 'Your order is being processed at the moment.', '2018-11-26 21:12:34', '2018-11-26 21:12:34'),
(30, 28, 'Your order is being processed at the moment.', '2018-11-26 21:16:24', '2018-11-26 21:16:24'),
(31, 29, 'Your order is being processed at the moment.', '2018-11-26 21:17:36', '2018-11-26 21:17:36'),
(32, 59, 'Your order is being processed at the moment.', '2018-12-27 01:03:53', '2018-12-27 01:03:53'),
(33, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:38', '2018-12-27 01:05:38'),
(34, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:39', '2018-12-27 01:05:39'),
(35, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:40', '2018-12-27 01:05:40'),
(36, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:40', '2018-12-27 01:05:40'),
(37, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:41', '2018-12-27 01:05:41'),
(38, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:42', '2018-12-27 01:05:42'),
(39, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:43', '2018-12-27 01:05:43'),
(40, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:43', '2018-12-27 01:05:43'),
(41, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:44', '2018-12-27 01:05:44'),
(42, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:44', '2018-12-27 01:05:44'),
(43, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:45', '2018-12-27 01:05:45'),
(44, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:45', '2018-12-27 01:05:45'),
(45, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:45', '2018-12-27 01:05:45'),
(46, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:46', '2018-12-27 01:05:46'),
(47, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:46', '2018-12-27 01:05:46'),
(48, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:46', '2018-12-27 01:05:46'),
(49, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:46', '2018-12-27 01:05:46'),
(50, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:47', '2018-12-27 01:05:47'),
(51, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:47', '2018-12-27 01:05:47'),
(52, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:47', '2018-12-27 01:05:47'),
(53, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:48', '2018-12-27 01:05:48'),
(54, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:48', '2018-12-27 01:05:48'),
(55, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:48', '2018-12-27 01:05:48'),
(56, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:49', '2018-12-27 01:05:49'),
(57, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:49', '2018-12-27 01:05:49'),
(58, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:49', '2018-12-27 01:05:49'),
(59, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:50', '2018-12-27 01:05:50'),
(60, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:50', '2018-12-27 01:05:50'),
(61, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:50', '2018-12-27 01:05:50'),
(62, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:51', '2018-12-27 01:05:51'),
(63, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:51', '2018-12-27 01:05:51'),
(64, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:51', '2018-12-27 01:05:51'),
(65, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:52', '2018-12-27 01:05:52'),
(66, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:06:43', '2018-12-27 01:06:43'),
(67, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:06:46', '2018-12-27 01:06:46'),
(68, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:08:56', '2018-12-27 01:08:56'),
(69, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:08:56', '2018-12-27 01:08:56'),
(70, 59, 'Your order is being processed at the moment.', '2018-12-27 01:09:05', '2018-12-27 01:09:05'),
(71, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:09:45', '2018-12-27 01:09:45'),
(72, 59, 'Your order has been delivered by our courier.', '2018-12-27 01:10:10', '2018-12-27 01:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_number` varchar(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` text NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `address` text,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0 = process, 1 = shipped, 2 = delivered',
  `type` int(11) NOT NULL COMMENT '0 = COD, 1 = Credit Card',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `size_id` int(10) NOT NULL DEFAULT '1',
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `image` text,
  `image_serialize` text,
  `barcode` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `gender_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `stock` int(10) NOT NULL,
  `used_stock` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `image`, `image_serialize`, `barcode`, `name`, `description`, `category_id`, `gender_id`, `price`, `stock`, `used_stock`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'yellow_1546222708.png', NULL, 'pro1', 'Dress 1', '', 26, 18, '749.00', 26, 0, '2018-12-30 18:18:28', '2018-12-30 18:18:28', NULL),
(2, 'gray_1546222888.png', NULL, 'pro2', 'Dress 2', '', 26, 18, '1499.00', 45, 0, '2018-12-30 18:21:28', '2018-12-30 18:21:28', NULL),
(3, 'white_1546223003.png', NULL, 'pro3', 'Dress 3', '', 26, 18, '999.00', 62, 0, '2018-12-30 18:23:23', '2018-12-30 18:23:23', NULL),
(4, 'black_1546223116.png', NULL, 'pro4', 'Dress 4', '', 26, 18, '1299.00', 92, 0, '2018-12-30 18:25:16', '2018-12-30 18:25:16', NULL),
(5, 'white_1546223317.png', NULL, 'pro5', 'Pants 1', '', 24, 17, '1399.00', 61, 0, '2018-12-30 18:28:37', '2018-12-30 18:28:37', NULL),
(6, 'gray_1546223459.png', NULL, 'pro6', 'Pants 2', '', 24, 19, '599.00', 93, 0, '2018-12-30 18:30:59', '2018-12-30 18:30:59', NULL),
(7, 'gray_1546223596.png', NULL, 'pro7', 'Pants 3', '', 24, 17, '399.00', 66, 0, '2018-12-30 18:33:16', '2018-12-30 18:33:16', NULL),
(8, 'white_1546223750.png', NULL, 'pro8', 'Pants 4', '', 24, 17, '1599.00', 76, 0, '2018-12-30 18:35:50', '2018-12-30 18:35:50', NULL),
(9, 'black_1546223873.png', NULL, 'pro9', 'Pants 5', '', 24, 18, '999.00', 55, 0, '2018-12-30 18:37:53', '2018-12-30 18:37:53', NULL),
(10, 'black_1546224071.png', NULL, 'pro10', 'Short 1', '', 25, 17, '449.00', 158, 0, '2018-12-30 18:41:11', '2018-12-30 18:41:11', NULL),
(11, 'white_1546224197.png', NULL, 'pro11', 'Short 2', '', 25, 18, '449.00', 118, 0, '2018-12-30 18:43:17', '2018-12-30 18:43:17', NULL),
(12, 'white_1546224356.png', NULL, 'pro12', 'Short 3', '', 25, 19, '699.00', 172, 0, '2018-12-30 18:45:56', '2018-12-30 18:45:56', NULL),
(13, 'white_1546224703.png', NULL, 'pro13', 'Short 4', '', 25, 17, '699.00', 137, 0, '2018-12-30 18:51:43', '2018-12-30 18:51:43', NULL),
(14, 'white_1546224811.png', NULL, 'pro14', 'Short 5', '', 25, 17, '899.00', 67, 0, '2018-12-30 18:53:31', '2018-12-30 18:53:31', NULL),
(15, 'gray_1546224948.png', NULL, 'pro15', 'Skirt 1', '', 23, 18, '899.00', 90, 0, '2018-12-30 18:55:48', '2018-12-30 18:55:48', NULL),
(16, 'black_1546225065.png', NULL, 'pro16', 'Skirt 2', '', 23, 18, '599.00', 128, 0, '2018-12-30 18:57:45', '2018-12-30 18:57:45', NULL),
(17, 'white_1546225160.png', NULL, 'pro17', 'Skirt 3', '', 23, 18, '999.00', 128, 0, '2018-12-30 18:59:20', '2018-12-30 18:59:20', NULL),
(18, 'black_1546225277.png', NULL, 'pro18', 'Skirt 4', '', 23, 18, '1299.00', 54, 0, '2018-12-30 19:01:17', '2018-12-30 19:01:17', NULL),
(19, 'blue_1546225387.png', NULL, 'pro19', 'Skirt 5', '', 23, 18, '749.00', 241, 0, '2018-12-30 19:03:07', '2018-12-30 19:03:07', NULL),
(20, 'white_1546225571.png', NULL, 'pro20', 'Tshirt 1', '', 8, 19, '649.00', 237, 0, '2018-12-30 19:06:11', '2018-12-30 19:06:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_by_subcategory`
--

CREATE TABLE `product_by_subcategory` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `upload_file` varchar(255) NOT NULL,
  `barcode` varchar(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `stock` int(10) NOT NULL,
  `used_stock` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_by_subcategory`
--

INSERT INTO `product_by_subcategory` (`id`, `image`, `upload_file`, `barcode`, `product_id`, `category_id`, `stock`, `used_stock`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'yellow_1546222708.png', 'FemaleJacket_1546222708.fbx', '', 1, 12, 1, 0, '2018-12-30 18:18:28', '2018-12-30 18:18:28', '0000-00-00 00:00:00'),
(2, 'gray_1546222888.png', 'FemaleJacket_1546222888.fbx', '', 2, 11, 8, 0, '2018-12-30 18:21:28', '2018-12-30 18:21:28', '0000-00-00 00:00:00'),
(3, 'yellow_1546222889.png', 'FemaleJacket2_1546222889.fbx', '', 2, 12, 6, 0, '2018-12-30 18:21:29', '2018-12-30 18:21:29', '0000-00-00 00:00:00'),
(4, 'blue_1546222889.png', 'Femalelongsleeve_1546222889.fbx', '', 2, 13, 4, 0, '2018-12-30 18:21:29', '2018-12-30 18:21:29', '0000-00-00 00:00:00'),
(5, 'red_1546222889.png', 'Femalelongsleeve3_1546222889.fbx', '', 2, 15, 4, 0, '2018-12-30 18:21:29', '2018-12-30 18:21:29', '0000-00-00 00:00:00'),
(6, 'orange_1546222889.png', 'VneckFemale_1546222889.fbx', '', 2, 16, 3, 0, '2018-12-30 18:21:29', '2018-12-30 18:21:29', '0000-00-00 00:00:00'),
(7, 'white_1546223003.png', 'FemaleJacket_1546223003.fbx', '', 3, 11, 9, 0, '2018-12-30 18:23:24', '2018-12-30 18:23:24', '0000-00-00 00:00:00'),
(8, 'yellow_1546223004.png', 'FemaleJacket2_1546223004.fbx', '', 3, 12, 6, 0, '2018-12-30 18:23:24', '2018-12-30 18:23:24', '0000-00-00 00:00:00'),
(9, 'blue_1546223004.png', 'Femalelongsleeve_1546223004.fbx', '', 3, 13, 5, 0, '2018-12-30 18:23:24', '2018-12-30 18:23:24', '0000-00-00 00:00:00'),
(10, 'red_1546223004.png', 'Femalelongsleeve3_1546223004.fbx', '', 3, 15, 5, 0, '2018-12-30 18:23:24', '2018-12-30 18:23:24', '0000-00-00 00:00:00'),
(11, 'purple_1546223004.png', 'jacket_1546223004.fbx', '', 3, 20, 7, 0, '2018-12-30 18:23:24', '2018-12-30 18:23:24', '0000-00-00 00:00:00'),
(12, 'black_1546223117.png', 'FemaleJacket_1546223117.fbx', '', 4, 11, 5, 0, '2018-12-30 18:25:17', '2018-12-30 18:25:17', '0000-00-00 00:00:00'),
(13, 'yellow_1546223117.png', 'FemaleJacket2_1546223117.fbx', '', 4, 12, 7, 0, '2018-12-30 18:25:17', '2018-12-30 18:25:17', '0000-00-00 00:00:00'),
(14, 'blue_1546223117.png', 'Femalelongsleeve_1546223117.fbx', '', 4, 13, 13, 0, '2018-12-30 18:25:17', '2018-12-30 18:25:17', '0000-00-00 00:00:00'),
(15, 'red_1546223117.png', 'Femalelongsleeve3_1546223117.fbx', '', 4, 15, 9, 0, '2018-12-30 18:25:17', '2018-12-30 18:25:17', '0000-00-00 00:00:00'),
(16, 'purple_1546223117.png', 'poloshirtlong_1546223117.fbx', '', 4, 20, 8, 0, '2018-12-30 18:25:17', '2018-12-30 18:25:17', '0000-00-00 00:00:00'),
(17, 'white_1546223318.png', 'poloshirt_1546223318.fbx', '', 5, 11, 5, 0, '2018-12-30 18:28:38', '2018-12-30 18:28:38', '0000-00-00 00:00:00'),
(18, 'yellow_1546223318.png', 'poloshirtlong_1546223318.fbx', '', 5, 12, 5, 0, '2018-12-30 18:28:38', '2018-12-30 18:28:38', '0000-00-00 00:00:00'),
(19, 'pink_1546223318.png', 'jacket_1546223318.fbx', '', 5, 14, 1, 0, '2018-12-30 18:28:38', '2018-12-30 18:28:38', '0000-00-00 00:00:00'),
(20, 'purple_1546223318.png', 'jacket2_1546223318.fbx', '', 5, 20, 5, 0, '2018-12-30 18:28:38', '2018-12-30 18:28:38', '0000-00-00 00:00:00'),
(21, 'gray_1546223318.png', 'Tshirt_1546223318.fbx', '', 5, 21, 5, 0, '2018-12-30 18:28:38', '2018-12-30 18:28:38', '0000-00-00 00:00:00'),
(22, 'gray_1546223459.png', 'poloshirtFemale_1546223459.fbx', '', 6, 11, 8, 0, '2018-12-30 18:30:59', '2018-12-30 18:30:59', '0000-00-00 00:00:00'),
(23, 'yellow_1546223459.png', 'poloshirtnopocket_1546223459.fbx', '', 6, 12, 7, 0, '2018-12-30 18:30:59', '2018-12-30 18:30:59', '0000-00-00 00:00:00'),
(24, 'blue_1546223459.png', 'sweater_1546223459.fbx', '', 6, 13, 3, 0, '2018-12-30 18:30:59', '2018-12-30 18:30:59', '0000-00-00 00:00:00'),
(25, 'red_1546223459.png', 'Vneck_1546223459.fbx', '', 6, 15, 7, 0, '2018-12-30 18:30:59', '2018-12-30 18:30:59', '0000-00-00 00:00:00'),
(26, 'orange_1546223459.png', 'VneckFemale_1546223459.fbx', '', 6, 16, 8, 0, '2018-12-30 18:30:59', '2018-12-30 18:30:59', '0000-00-00 00:00:00'),
(27, 'gray_1546223596.png', 'jacket_1546223596.fbx', '', 7, 11, 5, 0, '2018-12-30 18:33:16', '2018-12-30 18:33:16', '0000-00-00 00:00:00'),
(28, 'yellow_1546223596.png', 'jacket2_1546223596.fbx', '', 7, 12, 4, 0, '2018-12-30 18:33:16', '2018-12-30 18:33:16', '0000-00-00 00:00:00'),
(29, 'blue_1546223596.png', 'poloshirt_1546223596.fbx', '', 7, 13, 1, 0, '2018-12-30 18:33:16', '2018-12-30 18:33:16', '0000-00-00 00:00:00'),
(30, 'orange_1546223596.png', 'sweater_1546223596.fbx', '', 7, 16, 4, 0, '2018-12-30 18:33:16', '2018-12-30 18:33:16', '0000-00-00 00:00:00'),
(31, 'purple_1546223597.png', 'Tshirt_1546223597.fbx', '', 7, 20, 2, 0, '2018-12-30 18:33:17', '2018-12-30 18:33:17', '0000-00-00 00:00:00'),
(32, 'white_1546223750.png', 'poloshirtnopocket_1546223750.fbx', '', 8, 11, 4, 0, '2018-12-30 18:35:51', '2018-12-30 18:35:51', '0000-00-00 00:00:00'),
(33, 'blue_1546223751.png', 'jacket2_1546223751.fbx', '', 8, 13, 9, 0, '2018-12-30 18:35:51', '2018-12-30 18:35:51', '0000-00-00 00:00:00'),
(34, 'gray_1546223751.png', 'poloshirtlong_1546223751.fbx', '', 8, 16, 5, 0, '2018-12-30 18:35:51', '2018-12-30 18:35:51', '0000-00-00 00:00:00'),
(35, 'purple_1546223751.png', 'poloshirtnopocket_1546223751.fbx', '', 8, 20, 2, 0, '2018-12-30 18:35:51', '2018-12-30 18:35:51', '0000-00-00 00:00:00'),
(36, 'brown_1546223751.png', 'Vneck_1546223751.fbx', '', 8, 21, 6, 0, '2018-12-30 18:35:51', '2018-12-30 18:35:51', '0000-00-00 00:00:00'),
(37, 'black_1546223873.png', 'FemaleJacket_1546223873.fbx', '', 9, 11, 4, 0, '2018-12-30 18:37:53', '2018-12-30 18:37:53', '0000-00-00 00:00:00'),
(38, 'yellow_1546223873.png', 'FemaleJacket2_1546223873.fbx', '', 9, 12, 4, 0, '2018-12-30 18:37:53', '2018-12-30 18:37:53', '0000-00-00 00:00:00'),
(39, 'blue_1546223873.png', 'Femalelongsleeve_1546223873.fbx', '', 9, 13, 6, 0, '2018-12-30 18:37:53', '2018-12-30 18:37:53', '0000-00-00 00:00:00'),
(40, 'pink_1546223873.png', 'Femalelongsleeve3_1546223873.fbx', '', 9, 14, 4, 0, '2018-12-30 18:37:53', '2018-12-30 18:37:53', '0000-00-00 00:00:00'),
(41, 'gray_1546223873.png', 'jacket_1546223873.fbx', '', 9, 21, 7, 0, '2018-12-30 18:37:53', '2018-12-30 18:37:53', '0000-00-00 00:00:00'),
(42, 'black_1546224071.png', 'poloshirtnopocket_1546224071.fbx', '', 10, 11, 12, 0, '2018-12-30 18:41:11', '2018-12-30 18:41:11', '0000-00-00 00:00:00'),
(43, 'yellow_1546224071.png', 'poloshirtlong_1546224071.fbx', '', 10, 12, 13, 0, '2018-12-30 18:41:11', '2018-12-30 18:41:11', '0000-00-00 00:00:00'),
(44, 'blue_1546224071.png', 'Tshirt_1546224071.fbx', '', 10, 13, 12, 0, '2018-12-30 18:41:11', '2018-12-30 18:41:11', '0000-00-00 00:00:00'),
(45, 'orange_1546224071.png', 'Vneck_1546224071.fbx', '', 10, 16, 11, 0, '2018-12-30 18:41:11', '2018-12-30 18:41:11', '0000-00-00 00:00:00'),
(46, 'gray_1546224071.png', 'FemaleJacket2_1546224071.fbx', '', 10, 21, 10, 0, '2018-12-30 18:41:11', '2018-12-30 18:41:11', '0000-00-00 00:00:00'),
(47, 'white_1546224197.png', 'poloshirtFemale_1546224197.fbx', '', 11, 11, 4, 0, '2018-12-30 18:43:17', '2018-12-30 18:43:17', '0000-00-00 00:00:00'),
(48, 'blue_1546224197.png', 'jacket_1546224197.fbx', '', 11, 13, 4, 0, '2018-12-30 18:43:17', '2018-12-30 18:43:17', '0000-00-00 00:00:00'),
(49, 'red_1546224197.png', 'poloshirtnopocket_1546224197.fbx', '', 11, 15, 4, 0, '2018-12-30 18:43:17', '2018-12-30 18:43:17', '0000-00-00 00:00:00'),
(50, 'gray_1546224197.png', 'Vneck_1546224197.fbx', '', 11, 16, 3, 0, '2018-12-30 18:43:17', '2018-12-30 18:43:17', '0000-00-00 00:00:00'),
(51, 'black_1546224197.png', 'poloshirt_1546224197.fbx', '', 11, 21, 3, 0, '2018-12-30 18:43:17', '2018-12-30 18:43:17', '0000-00-00 00:00:00'),
(52, 'white_1546224356.png', 'Femalelongsleeve3_1546224356.fbx', '', 12, 11, 4, 0, '2018-12-30 18:45:56', '2018-12-30 18:45:56', '0000-00-00 00:00:00'),
(53, 'blue_1546224356.png', 'poloshirtFemale_1546224356.fbx', '', 12, 13, 2, 0, '2018-12-30 18:45:56', '2018-12-30 18:45:56', '0000-00-00 00:00:00'),
(54, 'red_1546224357.png', 'jacket_1546224357.fbx', '', 12, 15, 9, 0, '2018-12-30 18:45:57', '2018-12-30 18:45:57', '0000-00-00 00:00:00'),
(55, 'gray_1546224357.png', 'Vneck_1546224357.fbx', '', 12, 21, 4, 0, '2018-12-30 18:45:57', '2018-12-30 18:45:57', '0000-00-00 00:00:00'),
(56, 'black_1546224357.png', 'VneckFemale_1546224357.fbx', '', 12, 22, 3, 0, '2018-12-30 18:45:57', '2018-12-30 18:45:57', '0000-00-00 00:00:00'),
(57, 'white_1546224703.png', 'Femalelongsleeve3_1546224703.fbx', '', 13, 11, 7, 0, '2018-12-30 18:51:43', '2018-12-30 18:51:43', '0000-00-00 00:00:00'),
(58, 'blue_1546224703.png', 'poloshirtFemale_1546224703.fbx', '', 13, 13, 8, 0, '2018-12-30 18:51:43', '2018-12-30 18:51:43', '0000-00-00 00:00:00'),
(59, 'gray_1546224703.png', 'sweater_1546224703.fbx', '', 13, 15, 8, 0, '2018-12-30 18:51:43', '2018-12-30 18:51:43', '0000-00-00 00:00:00'),
(60, 'black_1546224703.png', 'jacket_1546224703.fbx', '', 13, 16, 7, 0, '2018-12-30 18:51:43', '2018-12-30 18:51:43', '0000-00-00 00:00:00'),
(61, 'brown_1546224703.png', 'VneckFemale_1546224703.fbx', '', 13, 21, 7, 0, '2018-12-30 18:51:43', '2018-12-30 18:51:43', '0000-00-00 00:00:00'),
(62, 'white_1546224811.png', 'jacket_1546224811.fbx', '', 14, 11, 3, 0, '2018-12-30 18:53:31', '2018-12-30 18:53:31', '0000-00-00 00:00:00'),
(63, 'black_1546224811.png', 'poloshirt_1546224811.fbx', '', 14, 15, 3, 0, '2018-12-30 18:53:31', '2018-12-30 18:53:31', '0000-00-00 00:00:00'),
(64, 'orange_1546224811.png', 'TshirtFemale_1546224811.fbx', '', 14, 16, 4, 0, '2018-12-30 18:53:31', '2018-12-30 18:53:31', '0000-00-00 00:00:00'),
(65, 'purple_1546224811.png', 'Model_1546224811.fbx', '', 14, 20, 3, 0, '2018-12-30 18:53:31', '2018-12-30 18:53:31', '0000-00-00 00:00:00'),
(66, 'gray_1546224811.png', 'Vneck_1546224811.fbx', '', 14, 21, 4, 0, '2018-12-30 18:53:31', '2018-12-30 18:53:31', '0000-00-00 00:00:00'),
(67, 'gray_1546224948.png', 'jacket_1546224948.fbx', '', 15, 11, 5, 0, '2018-12-30 18:55:48', '2018-12-30 18:55:48', '0000-00-00 00:00:00'),
(68, 'blue_1546224948.png', 'Model_1546224948.fbx', '', 15, 13, 5, 0, '2018-12-30 18:55:48', '2018-12-30 18:55:48', '0000-00-00 00:00:00'),
(69, 'pink_1546224948.png', 'VneckFemale_1546224948.fbx', '', 15, 14, 5, 0, '2018-12-30 18:55:48', '2018-12-30 18:55:48', '0000-00-00 00:00:00'),
(70, 'orange_1546224948.png', 'sweater_1546224948.fbx', '', 15, 16, 14, 0, '2018-12-30 18:55:48', '2018-12-30 18:55:48', '0000-00-00 00:00:00'),
(71, 'black_1546224948.png', 'poloshirtnopocket_1546224948.fbx', '', 15, 21, 11, 0, '2018-12-30 18:55:48', '2018-12-30 18:55:48', '0000-00-00 00:00:00'),
(72, 'black_1546225065.png', 'Femalelongsleeve_1546225065.fbx', '', 16, 11, 4, 0, '2018-12-30 18:57:45', '2018-12-30 18:57:45', '0000-00-00 00:00:00'),
(73, 'red_1546225065.png', 'poloshirtFemale_1546225065.fbx', '', 16, 15, 7, 0, '2018-12-30 18:57:45', '2018-12-30 18:57:45', '0000-00-00 00:00:00'),
(74, 'purple_1546225065.png', 'Femalelongsleeve3_1546225065.fbx', '', 16, 20, 6, 0, '2018-12-30 18:57:45', '2018-12-30 18:57:45', '0000-00-00 00:00:00'),
(75, 'gray_1546225065.png', 'sweater_1546225065.fbx', '', 16, 21, 6, 0, '2018-12-30 18:57:45', '2018-12-30 18:57:45', '0000-00-00 00:00:00'),
(76, 'green_1546225065.png', 'VneckFemale_1546225065.fbx', '', 16, 22, 5, 0, '2018-12-30 18:57:45', '2018-12-30 18:57:45', '0000-00-00 00:00:00'),
(77, 'white_1546225160.png', 'Femalelongsleeve3_1546225160.fbx', '', 17, 11, 6, 0, '2018-12-30 18:59:20', '2018-12-30 18:59:20', '0000-00-00 00:00:00'),
(78, 'blue_1546225160.png', 'Femalelongsleeve_1546225160.fbx', '', 17, 13, 6, 0, '2018-12-30 18:59:20', '2018-12-30 18:59:20', '0000-00-00 00:00:00'),
(79, 'pink_1546225160.png', 'poloshirt_1546225160.fbx', '', 17, 14, 6, 0, '2018-12-30 18:59:20', '2018-12-30 18:59:20', '0000-00-00 00:00:00'),
(80, 'purple_1546225160.png', 'sweater_1546225160.fbx', '', 17, 20, 5, 0, '2018-12-30 18:59:20', '2018-12-30 18:59:20', '0000-00-00 00:00:00'),
(81, 'green_1546225160.png', 'VneckFemale_1546225160.fbx', '', 17, 22, 5, 0, '2018-12-30 18:59:20', '2018-12-30 18:59:20', '0000-00-00 00:00:00'),
(82, 'black_1546225277.png', 'FemaleJacket_1546225277.fbx', '', 18, 11, 7, 0, '2018-12-30 19:01:17', '2018-12-30 19:01:17', '0000-00-00 00:00:00'),
(83, 'blue_1546225277.png', 'jacket2_1546225277.fbx', '', 18, 13, 5, 0, '2018-12-30 19:01:17', '2018-12-30 19:01:17', '0000-00-00 00:00:00'),
(84, 'red_1546225277.png', 'poloshirtFemale_1546225277.fbx', '', 18, 15, 3, 0, '2018-12-30 19:01:17', '2018-12-30 19:01:17', '0000-00-00 00:00:00'),
(85, 'purple_1546225277.png', 'Vneck_1546225277.fbx', '', 18, 20, 4, 0, '2018-12-30 19:01:17', '2018-12-30 19:01:17', '0000-00-00 00:00:00'),
(86, 'brown_1546225277.png', 'sweaterfemale_1546225277.fbx', '', 18, 21, 5, 0, '2018-12-30 19:01:17', '2018-12-30 19:01:17', '0000-00-00 00:00:00'),
(87, 'blue_1546225387.png', 'FemaleJacket_1546225387.fbx', '', 19, 13, 9, 0, '2018-12-30 19:03:07', '2018-12-30 19:03:07', '0000-00-00 00:00:00'),
(88, 'pink_1546225387.png', 'FemaleJacket2_1546225387.fbx', '', 19, 14, 9, 0, '2018-12-30 19:03:07', '2018-12-30 19:03:07', '0000-00-00 00:00:00'),
(89, 'orange_1546225387.png', 'Femalelongsleeve_1546225387.fbx', '', 19, 16, 9, 0, '2018-12-30 19:03:07', '2018-12-30 19:03:07', '0000-00-00 00:00:00'),
(90, 'purple_1546225387.png', 'Femalelongsleeve3_1546225387.fbx', '', 19, 20, 7, 0, '2018-12-30 19:03:07', '2018-12-30 19:03:07', '0000-00-00 00:00:00'),
(91, 'green_1546225387.png', 'poloshirtFemale_1546225387.fbx', '', 19, 22, 7, 0, '2018-12-30 19:03:07', '2018-12-30 19:03:07', '0000-00-00 00:00:00'),
(92, 'white_1546225571.png', 'Femalelongsleeve_1546225571.fbx', '', 20, 11, 7, 0, '2018-12-30 19:06:11', '2018-12-30 19:06:11', '0000-00-00 00:00:00'),
(93, 'pink_1546225571.png', 'poloshirt_1546225571.fbx', '', 20, 14, 7, 0, '2018-12-30 19:06:11', '2018-12-30 19:06:11', '0000-00-00 00:00:00'),
(94, 'purple_1546225571.png', 'poloshirtnopocket_1546225571.fbx', '', 20, 20, 7, 0, '2018-12-30 19:06:11', '2018-12-30 19:06:11', '0000-00-00 00:00:00'),
(95, 'gray_1546225571.png', 'sweater_1546225571.fbx', '', 20, 21, 9, 0, '2018-12-30 19:06:11', '2018-12-30 19:06:11', '0000-00-00 00:00:00'),
(96, 'green_1546225571.png', 'VneckFemale_1546225571.fbx', '', 20, 22, 7, 0, '2018-12-30 19:06:11', '2018-12-30 19:06:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_review`
--

CREATE TABLE `product_review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `rate` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `returned_products`
--

CREATE TABLE `returned_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `buyer_note` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `extension_name` varchar(3) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(191) DEFAULT NULL,
  `phone_number` varchar(12) DEFAULT NULL,
  `random_code` varchar(6) DEFAULT NULL,
  `address` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `extension_name`, `username`, `email`, `password`, `remember_token`, `phone_number`, `random_code`, `address`, `status`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'S', 'A', 'M', 'SAM', 'Admin', 'web_admin@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', 'LSmyCkW0wZZZtDXSFx2g9HQR9iR4UsR9MUdaMHcBo5rj465rqulQLB2Gn6wu', NULL, '', NULL, 1, 'admin', '2018-07-25 17:10:33', '2018-07-25 17:10:33', NULL),
(4, 'U', 'C', 'C', NULL, 'ICP', 'icp@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', 'aZvmH9c2zkNddGH21CWqaHJhYoFakG8o5XpaOd6cO0JJxbxAVHax2i1B0gJn', NULL, '', NULL, 1, 'icp', '2018-07-25 17:27:24', '2018-07-25 17:27:24', NULL),
(5, 'Store', NULL, 'Manager', NULL, 'Manager', 'store_manager@gmail.com', '$2y$10$BvyQam/KHznW3SRNCr9T9uvp0miAi5gd9d2h.f5pTImmVsrbpSsIS', NULL, NULL, '', NULL, 1, 'manager', '2018-07-25 17:43:53', '2018-07-25 17:43:53', NULL),
(6, 'John', NULL, 'Roncales', NULL, 'jrroncales', 'jrroncales25@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', '5qNl5LQJAHi1LnFjgaxZdwOHpBPgME9wz2Gid7S8wp3RGX0MJ4p15egZpaV9', '639452538775', 'JSVKTT', 'blk 11 lot 8 north triangle cielito camarin caloocan city', 1, 'user', '2018-07-25 17:59:44', '2018-10-13 17:41:45', NULL),
(12, 'Richard', NULL, 'Roncales', NULL, 'unno21', 'chadxx1993@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', 'XoUNh0GSBNDAlIpmeeyLwOP5rzdENnSSvlXBPukZCAiZtzwXwJxgjOpAWWeK', '639507353920', 'JSSRG7', NULL, 0, 'user', '2018-09-01 22:40:37', '2018-09-01 22:40:37', NULL),
(21, 'Jade', NULL, 'Coral', NULL, 'jade_coral', 'jadecoral@gmail.com', '$2y$10$nZ55/YqUoTMTsCnBb3ftEeKp3IxVXoZJQR3qtNPFTJZnYfKuMAOGu', 'eLwmcny7jdENBQStJkicZ6JJL4ubNvCXlx6pkY204y4E4njryEsgSNdHtOij', '639507353920', '41J5YC', NULL, 0, 'user', '2018-10-02 09:09:03', '2018-10-02 09:09:03', NULL),
(22, 'Tin', NULL, 'Carpio', NULL, 'tincarpio', 'tincarpio@gmail.com', '$2y$10$usn8W/xkBBTsT56IA0LhbejwlbJaUuZnxXAwXvXCd.0/nxERRjudq', 'RUHgLDaBawnyNJQWZMDhHgTv2mBhmh6wCSDwLBR0DTHKaZTEb5W0AOuVcRvJ', '639507353920', '859Z2C', NULL, 0, 'user', '2018-10-02 09:10:26', '2018-10-02 09:10:26', NULL),
(23, 'Joseph', NULL, 'Malaga', NULL, 'joseph', 'malaga@gmail.com', '$2y$10$3YLBHu5gyLrV0g1IO7K.TuESaBYX5rhBqR.DfWK/GIj0.9m1vaKIu', 'JWjkRL7ZFFulwjfqZljmxIwR93iQRahV5MK2ohVS15FDKbA71MpooHfxR9N0', '639507353920', 'M8EIWC', NULL, 1, 'user', '2018-10-02 09:15:44', '2018-10-02 09:15:53', NULL),
(24, 'Mac', 'A', 'Acebes', NULL, 'allennegro', 'negronegro@gmail.com', '$2y$10$09IsmYTkmmJ5nwAVUC2.dOD8o5OP9i7iozCb3hjn8TOVMWHHr6b4C', NULL, NULL, NULL, NULL, 1, 'cashier', '2018-10-02 10:11:44', '2018-10-02 10:11:44', NULL),
(26, 'eohan', 'eohan', 'eohan', NULL, 'eohan', 'eohan@gmail.com', '$2y$10$2A2/bYH7TaB.3lYmKonnsuuVaWJSKQTNoggQ1Hj7/5Q79OHh.ZuIW', NULL, NULL, NULL, NULL, 1, 'cashier', '2018-10-05 04:01:58', '2018-10-05 04:01:58', NULL),
(54, 'New User', NULL, 'Customer', NULL, NULL, 'sample_1546245048@sample.com', '$2y$10$26XhBvvjWKR6qHwS7W0TMOtyxzrme/lo4qKSNLR4VS6lfUkujtWia', NULL, NULL, '9JUF2F', NULL, 1, 'user', '2018-12-31 00:30:49', '2018-12-31 00:30:49', NULL),
(55, 'New User', NULL, 'Customer', NULL, NULL, 'sample_1546245051@sample.com', '$2y$10$f7lO7/u0mKZGxk9LoSTLP.aXdvNDOgeMwj/h1rdNzpvX/cfmfeIyy', NULL, NULL, 'O4I37F', NULL, 1, 'user', '2018-12-31 00:30:51', '2018-12-31 00:30:51', NULL),
(56, 'New User', NULL, 'Customer', NULL, NULL, 'sample_1546245689@sample.com', '$2y$10$hpiIM6enbV.Q0IaW6k1zKOvwISAuctTmOA8dlEUnaersyOiPLwe.W', NULL, NULL, 'LCCT5Q', NULL, 1, 'user', '2018-12-31 00:41:29', '2018-12-31 00:41:29', NULL),
(57, 'New User', NULL, 'Customer', NULL, NULL, 'sample_1546245691@sample.com', '$2y$10$h025dHzzrGVUQK/mHdnxh.xOA6J59o5lIgJHTuRCOHYUD4qWwKEDy', NULL, NULL, 'ATFPJ6', NULL, 1, 'user', '2018-12-31 00:41:31', '2018-12-31 00:41:31', NULL),
(58, 'New User', NULL, 'Customer', NULL, NULL, 'sample_1546246133@sample.com', '$2y$10$jjo2W6KSHUaiE/UMv611d.9MJGJOLCdHBKhIwhmGUkj5oWTA1i29q', NULL, NULL, '6XFI8D', NULL, 1, 'user', '2018-12-31 00:48:54', '2018-12-31 00:48:54', NULL),
(59, 'New User', NULL, 'Customer', NULL, NULL, 'sample_1546246135@sample.com', '$2y$10$NYjoz5jCVjMA7ITxNSQRieGikX2CiCeM1/jrm0REDlOPRcTtfnkTO', NULL, NULL, 'BM0633', NULL, 1, 'user', '2018-12-31 00:48:55', '2018-12-31 00:48:55', NULL),
(60, 'New User', NULL, 'Customer', NULL, NULL, 'sample_1546247243@sample.com', '$2y$10$JB/DRf1fByrgoc5eue3pl.ZSeB19NVJZ/TzkSEYk6D6wqdleK/0tC', NULL, NULL, '6UNNQW', NULL, 1, 'user', '2018-12-31 01:07:23', '2018-12-31 01:07:23', NULL),
(61, 'New User', NULL, 'Customer', NULL, NULL, 'sample_1546247245@sample.com', '$2y$10$EHBAo0wirlhHxkP1LGiQRO/A8y0PfXL1XJLtA5JMkHTo5Q3Vjbq7G', NULL, NULL, 'W7QS87', NULL, 1, 'user', '2018-12-31 01:07:25', '2018-12-31 01:07:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `post_code` varchar(255) NOT NULL,
  `phone_number` varchar(13) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`id`, `user_id`, `full_name`, `address`, `post_code`, `phone_number`, `created_at`, `updated_at`) VALUES
(1, 6, 'jomari germino', 'Blk 11 Lot 19921', '1391jdewq', '639423123121', '2018-09-05 06:59:10', '0000-00-00 00:00:00'),
(2, 6, 'Mac Allen Acebes', 'aosdmcmo2mfon n jw9ejejjvj  2123 fef', 'qwdiq29  8weq8e jqwjeq21', '639423123121', '2018-09-05 07:07:38', '0000-00-00 00:00:00'),
(723, 6, 'sf sfsdf s', '23 adeada', '2323', '13131313', '2018-09-16 00:01:05', '2018-09-16 00:01:05'),
(724, 6, 'fs s s f', '32 sdasdsfs', '223', '13131346', '2018-09-16 00:02:21', '2018-09-16 00:02:21'),
(725, 6, 'sfsf sfsf', '23213 asdasda', '2313', '13478765', '2018-09-16 00:03:13', '2018-09-16 00:03:13'),
(726, 6, 'dasdasd', 'sdasdasd', 'ada', '2131', '2018-09-16 00:20:49', '2018-09-16 00:20:49'),
(727, 6, 'huuuuuuuuuuuuuuuu', 'sdasdasd', 'ada', '2131', '2018-09-16 00:39:21', '2018-09-16 00:39:21'),
(728, 6, 'huuuuuuu', 'sdasdasd', 'ada', '2131', '2018-09-16 00:41:02', '2018-09-16 00:41:02'),
(729, 6, 'sf sfsdf s', '23 adeada', '2323', '42141241', '2018-09-16 00:44:05', '2018-09-16 00:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `user_id`, `product_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 12, 21, '2018-12-04 07:34:13', '2018-12-04 07:34:13', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audit_trails`
--
ALTER TABLE `audit_trails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_details`
--
ALTER TABLE `cart_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_trails`
--
ALTER TABLE `log_trails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mirror_logs`
--
ALTER TABLE `mirror_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notify_order`
--
ALTER TABLE `notify_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_by_subcategory`
--
ALTER TABLE `product_by_subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_review`
--
ALTER TABLE `product_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `returned_products`
--
ALTER TABLE `returned_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audit_trails`
--
ALTER TABLE `audit_trails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cart_details`
--
ALTER TABLE `cart_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `log_trails`
--
ALTER TABLE `log_trails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mirror_logs`
--
ALTER TABLE `mirror_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notify_order`
--
ALTER TABLE `notify_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `product_by_subcategory`
--
ALTER TABLE `product_by_subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `product_review`
--
ALTER TABLE `product_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `returned_products`
--
ALTER TABLE `returned_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=730;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
