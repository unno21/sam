<?php $__env->startSection('title'); ?>
    Reports | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="main-container w-100">

        <div class="main-wrapper">
            <div class="container">
                
                <ul class="nav nav-tabs d-none" id="reportsTab" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link" id="iReport-tab" data-toggle="tab" href="#iReport" aria-controls="iReport" aria-selected="true">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="sReport-tab" data-toggle="tab" href="#sReport" aria-controls="sReport" aria-selected="false">Profile</a>
                    </li>
                </ul> 
                <nav class="cust_tabs1 mt-4 p-0 d-flex w-100 bg-white">
                    <div class="cust_selector1" id="cust_selector1"></div>
                    <a href="#" class="reportsTab_active mdl-js-button mdl-js-ripple-effect w-50 text-center position-relative py-3" id="iReport1">INVENTORY REPORTS</a>
                    <a href="#" class="mdl-js-button mdl-js-ripple-effect w-50 text-center position-relative py-3" id="sReport1">SALES REPORT</a>
                </nav>                                  
                <div class="tab-content" id="myReportTab">
                    <div class="tab-pane fade show active h-100 py-5" id="iReport" role="tabpanel" aria-labelledby="iReport-tab">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered bg-white mdl-shadow--4dp" id="iReportsTbl">
                                <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Category</th>
                                        <th>Price</th>
                                        <th>Available Stock</th>
                                        <th>Sold (%)</th>
                                        <th>Stock (%)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                    $total_stock = $product->used_stock + $product->stock;
                                    $percent_stock = ($product->used_stock / $total_stock) * 100;
                                ?>
                                    <tr>
                                        <td><?php echo e($product->name); ?></td>
                                        <td class="text-capitalize"><?php echo e($product->category->name); ?></td>
                                        <td><?php echo e($product->price); ?></td>
                                        <td><?php echo e($product->stock); ?></td>
                                        <td>Sold (%)</td>
                                        <td><?php echo e($percent_stock); ?>%</td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="8">
                                            <button class="mdl-button mdl-js-ripple-effect mdl-js-button mdl-button--raised mdl-button--colored myButton1 px-4 py-2 d-flex align-items-center">
                                                <i class="material-icons-new outline-print icon-white mr-2"></i>Print Report
                                            </button>
                                        </th>
                                    </tr>
                                </tfoot>
                                <tfoot>
                                    <tr>
                                        <th colspan="8">
                                            <div class="pagination-navigation">
                                                <div class="pagination-current cust_gradient"></div>
                                                <div class="pagination-dots" id="iReportsPag">
                                                    <button class="pagination-dot paginate_active">
                                                        <span class="pagination-number">1</span>
                                                    </button>
                                                    <button class="pagination-dot">
                                                        <span class="pagination-number">2</span>
                                                    </button>
                                                    <button class="pagination-dot">
                                                        <span class="pagination-number">3</span>
                                                    </button>
                                                    <button class="pagination-dot">
                                                        <span class="pagination-number">4</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                </tfoot>
                                
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade active py-5" id="sReport" role="tabpanel" aria-labelledby="sReport-tab">
                        <div class="container-fluid">
                            <div class="row w-100">
                                <div class="col-sm-3">
                                    <div class="dateFrom">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txtFields">
                                            <input class="mdl-textfield__input" type="text" id="date1" name="prodName">
                                            <label class="mdl-textfield__label" for="date1">From</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="dateTo">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txtFields">
                                            <input class="mdl-textfield__input" type="text" id="date2" name="prodName">
                                            <label class="mdl-textfield__label" for="date2">To</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered bg-white mdl-shadow--4dp">
                                <thead>
                                    <tr>
                                        <th>Order Name</th>
                                        <th>Payment</th>
                                        <th>Status</th>
                                        <th>Date of Ordered</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $sales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td class="text-capitalize"><?php echo e($sale->order_number); ?></td>
                                        <td><?php echo e(Crypt::decrypt($sale->amount)); ?></td>
                                        <td class="text-<?php echo e($sale->status == '2' ? 'success' : 'danger'); ?>"><?php echo e($sale->status == '2' ? 'Delivered!' : 'In Process'); ?></td>
                                        <td><?php echo e(date('F m, Y', strtotime($sale->created_at))); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="4">
                                            <button class="mdl-button mdl-js-ripple-effect mdl-js-button mdl-button--raised mdl-button--colored myButton1 px-4 py-2 d-flex align-items-center">
                                                <i class="material-icons-new outline-print icon-white mr-2"></i>Print Report
                                            </button>
                                        </th>
                                    </tr>
                                </tfoot>
                                <tfoot>
                                    <tr>
                                        <th colspan="4">
                                            <div class="pagination-navigation1">
                                                <div class="pagination-current1 cust_gradient"></div>
                                                <div class="pagination-dots1" id="iSalesPag">
                                                    <button class="pagination-dot1 paginate_active">
                                                        <span class="pagination-number1">1</span>
                                                    </button>
                                                    <button class="pagination-dot1">
                                                        <span class="pagination-number1">2</span>
                                                    </button>
                                                    <button class="pagination-dot1">
                                                        <span class="pagination-number1">3</span>
                                                    </button>
                                                    <button class="pagination-dot1">
                                                        <span class="pagination-number1">4</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                </tfoot>
                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
                                    <defs>
                                        <filter id="goo">
                                            <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                                            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                                            <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
                                        </filter>
                                    </defs>
                                </svg>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript" src="<?php echo e(asset('assets/custom/js/admin.js')); ?>"></script>
    <script>
        $(".repSNL").addClass("SNLactive")
        $(".repSNL a").css("color","white") 

        $('#sReport1').click(function(){
            $('.nav-tabs > .active').next('li').find('a').trigger('click')
        })
        $('#iReport1').click(function(){
            $('.nav-tabs > .active > a').trigger('click')
        })

        $(".cust_tabs1").on("click","a",function(){
            $('.cust_tabs1 a').removeClass("reportsTab_active")
            $(this).addClass('reportsTab_active')
            var itemPos = $(this).position()
            $(".cust_selector1").css({
                "left":itemPos.left+"px"
            })
        })  
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('back-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>