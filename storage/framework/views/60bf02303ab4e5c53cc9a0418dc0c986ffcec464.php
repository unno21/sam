<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

	<title><?php echo $__env->yieldContent('title'); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" type="img/icon" href="<?php echo e(asset('assets/images/logo.png')); ?>">

	<!-- *** JQUERY *** -->

	<!-- Bootstrap jquery --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/code-jquery-3.2.1.slim.min.js')); ?>"></script>
	<!-- jquery --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/jquery-3.3.1.min.js')); ?>"></script>
	<!-- jquery --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/jquery-ui.js')); ?>"></script>
	
	<!-- custom css --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/custom/css/style.css')); ?>">
	<!-- custom css --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/Animocons/css/icons.css')); ?>">
	<?php echo $__env->make('includes.links-styles', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<script src="<?php echo e(asset('assets/custom/js/modernizr.custom.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/custom/js/snap.svg-min.js')); ?>"></script>
	<script>document.documentElement.className = 'js';</script>

</head>
<body class="demo-1">
	
	<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored mdl-button--fab cust_gradient" id="back2topBtn" data-tooltip="BACK TO TOP" data-inverted="">
		<!-- <div class="mdl-tooltip" for="back2topBtn">BACK TO TOP</div> -->
		<i class="material-icons">keyboard_arrow_up</i>
	</button>
	
	<div class="top_nav">
		<div class="menu cross menu--1 position-relative">
						
		    <label id="openSidenav" class="position-absolute">
		      	<input type="checkbox" class="d-none" id="menuCkhBx">
		      	<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
		        	<path class="line--1" d="M0 40h62c13 0 6 28-4 18L35 35" />
		        	<path class="line--2" d="M0 50h70" />
		        	<path class="line--3" d="M0 60h62c13 0 6-28-4-18L35 65" />
		      	</svg>
		    </label>

	  	</div>
		<ul class="mb-0 position-relative d-flex main-nav">
			<li class="bright">
				<a href="#" class="mdl-js-button mdl-js-ripple-effect position-relative">
					<div class="d-flex align-items-center justify-content-center h-100">
						<div><img src="<?php echo e(asset('assets/images/logo.png')); ?>" height="44px"></div>
					</div>
				</a>
			</li>
			<li class="bright">
				<a href="<?php echo e(url('/')); ?>" class="mdl-js-button mdl-js-ripple-effect position-relative Lspacing2 text-center text-uppercase">
					<div class="d-flex align-items-center justify-content-center h-100">
						<div>home</div>
					</div>
				</a>
			</li>
			<li class="bright">
				<a href="#" class="mdl-js-button mdl-js-ripple-effect position-relative Lspacing2 text-center text-uppercase">
					<div class="d-flex align-items-center justify-content-center h-100">
						<div>about</div>
					</div>
				</a>
			</li>
			<li class="bright">
				<a href="<?php echo e(url('/shop')); ?>" class="mdl-js-button mdl-js-ripple-effect position-relative Lspacing2 text-center text-uppercase">
					<div class="d-flex align-items-center justify-content-center h-100">
						<div>shop</div>
					</div>
				</a>
			</li>
			<li class="mdl-layout-spacer"></li>
			<li class="bleft">
				<a href="<?php echo e(url('/wishlist')); ?>" class="mdl-js-button mdl-js-ripple-effect position-relative Lspacing2 text-center text-uppercase px-4">
					<div class="d-flex align-items-center justify-content-center h-100">
						<div><i class="ion-ios-heart-outline"></i></div>
					</div>
				</a>
			</li>
			<li class="bleft">
				<a href="<?php echo e(url('/cart')); ?>" class="mdl-js-button mdl-js-ripple-effect position-relative Lspacing2 text-center text-uppercase px-4">
					<div class="d-flex align-items-center justify-content-center h-100">
						<div><i class="ion-ios-cart-outline"></i></div>
					</div>
				</a>
			</li>
			<li class="bleft">
				<a href="#" class="mdl-js-button mdl-js-ripple-effect position-relative Lspacing2 text-center text-uppercase px-4" id="btnAccount">
					<div class="d-flex align-items-center justify-content-center h-100">
						<div><i class="ion-ios-contact-outline"></i></div>
					</div>
				</a>
				<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="btnAccount">
					<a href="<?php echo e(url('/account')); ?>"><li class="mdl-menu__item">My Account</li></a>
					<?php if(Auth::check()): ?>
					<a href="<?php echo e(url('/logout')); ?>"><li class="mdl-menu__item">Logout</li></a>
					<?php endif; ?>
				</ul>
			</li>
		</ul>
	</div>
	<nav id="sidebar" class="position-fixed text-white">
        <div class="sidebar-header position-relative">
            <a href="#" class="">
            	<div>
            		<img src="<?php echo e(asset('assets/images/logo.png')); ?>" class="img-fluid">
            	</div>
            </a>
        </div>

        <ul class="list-unstyled components">
            <li class="active">
                <a href="<?php echo e(url('/')); ?>" aria-expanded="false" class="text-uppercase Lspacing2 activeSNL">Home</a>
            </li>
            <li>
                <a href="#" class="text-uppercase Lspacing2">About</a>
            </li>
            <li>
                <a href="<?php echo e(url('/shop')); ?>" class="text-uppercase Lspacing2">Shop</a>
            </li>
        </ul>
        <div class="menuTxt">
        	<p class="mb-0 text-uppercase cust_bold select_none lspacing2" id="menuTxt"></p>
        </div>
    </nav>
    <div class="overlay position-fixed w-100"></div>

    <?php echo $__env->yieldContent('content'); ?>

	<div class="footer_container">
	<div class="footer1 pt-5">
	<!-- TATANGGALIN TO! -->
		<div class="container py-5">
			
			<h1 class="text-center text-uppercase Lspacing2 pt-5 mb-5">Shopping Assistant Mirror</h1>
			<div class="container">
				<div class="row justify-content-center mb-5">
					<div class="col-sm-7">
						<p class="text-center lead">Tenetur dolorem alias ad veniam voluptatum cupiditate deleniti eos, quia accusamus, autem numquam.</p>
					</div>
				</div>
			</div>

		</div>
	</div><!-- END FOOTER 1 -->

	<div class="footer2 pb-5">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-3">
					<h2 class="text-white Lspacing2 mt-5">SAM</h2>
					<p class="text-white pt-2">SAM is a shopping assistant mirror with augmented reality to improve the customer shopping experience.</p>
				</div>
				<div class="col-sm-3">
					<h2 class="text-white Lspacing2 mt-5">SITE MAP</h2>
					<div class="sitemap">
						<ul class="m-0 p-0">
							<li><a href="<?php echo e(url('/')); ?>">Home</a></li>
							<li><a href="<?php echo e(url('/shop')); ?>">Shop</a></li>
							<li><a href="<?php echo e(url('/cart')); ?>">Cart</a></li>
							<li><a href="<?php echo e(url('/account')); ?>">Account</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6">
					<h2 class="text-white Lspacing2 mt-5">CONTACT US</h2>
					<div class="contactus">
						<form>
							<div class="row">
								<div class="col-sm-12 col-md-12 col-lg-9">
									<input type="text" name="" placeholder="NAME *" class="mb-3 p-3 radius3 mdl-shadow--4dp w-100" autocomplete="off">
								</div>
								<div class="col-sm-12 col-md-12 col-lg-9">
									<input type="text" name="" placeholder="EMAIL *" class="mb-3 p-3 radius3 mdl-shadow--4dp w-100" autocomplete="off">
								</div>
								<div class="col-sm-12 col-md-12 col-lg-9">
									<textarea  name="" placeholder="EMAIL *" class="mb-3 p-3 radius3 mdl-shadow--4dp w-100" rows="6"></textarea>
								</div>
								<div class="col-sm-12 col-md-12 col-lg-9"><button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored w-100 myButton3">submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>
	</div><!-- END FOOTER 2 -->

	<div class="footer3 py-3">
		<div class="container">
			
			<div class="d-flex justify-content-center">
				<div class="text-white mb-3">
					<a href="#" class="text-white mr-2">FAQs</a>|
					<a href="#" class="text-white mr-2">Privacy</a>|
					<a href="#" class="text-white">Terms of Service</a>
				</div>
			</div>
			<p class="text-white text-center">SAM © 2018</p>

		</div>
	</div><!-- END FOOTER 3 -->
	</div>
</body>
    <?php echo $__env->yieldContent('js'); ?>
    <script>
    	$(document).ready(function(){
	       	if ($('.top_nav').width() <= 767 ){
	       		$('.top_nav').addClass('sticky-top')
	       	}
		})
    	$(window).resize(function(){
	       	if ($('.top_nav').width() <= 767 ){
	       		$('.top_nav').addClass('sticky-top')
	       	}
		})
    </script>
</html>

