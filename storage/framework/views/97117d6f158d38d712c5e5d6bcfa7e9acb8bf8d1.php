

<?php $__env->startSection('title'); ?>
    Home | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="main-container">

	<div class="slideshow">
		<div class="slides">
			<div class="slide slide--current">
				<div class="slide__img" style="background-image: url(<?php echo e(asset('assets/images/1.jpg')); ?>)"></div>
				<h1 class="slide__title text-white Lspacing2 text-uppercase display-4">Sample title</h1>
				<p class="slide__desc text-white">A matter of delicate proportions and aesthetics.</p>
				<a class="slide__link mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 Lspacing2 py-2 px-5" href="#">Read More</a>
			</div>
			<div class="slide">
				<div class="slide__img" style="background-image: url(<?php echo e(asset('assets/images/2.jpg')); ?>)"></div>
				<h2 class="slide__title text-white Lspacing2 text-uppercase display-4">Massive</h2>
				<p class="slide__desc text-white">The thoughtful making of space is an art.</p>
				<a class="slide__link mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 Lspacing2 py-2 px-5" href="#">Go to Shop</a>
			</div>
			<div class="slide">
				<div class="slide__img" style="background-image: url(<?php echo e(asset('assets/images/3.jpg')); ?>)"></div>
				<h2 class="slide__title text-white Lspacing2 text-uppercase display-4">Towering</h2>
				<p class="slide__desc text-white">If a building becomes architecture, then it is art.</p>
				<a class="slide__link mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 Lspacing2 py-2 px-5" href="#">Find out more</a>
			</div>
			<div class="slide">
				<div class="slide__img" style="background-image: url(<?php echo e(asset('assets/images/4.jpg')); ?>)"></div>
				<h2 class="slide__title text-white Lspacing2 text-uppercase display-4">Immense</h2>
				<p class="slide__desc text-white">Architecture is a visual art, and the buildings speak for themselves.</p>
				<a class="slide__link mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 Lspacing2 py-2 px-5" href="#">Uncover beauty</a>
			</div>
		</div>
		<nav class="slidenav d-flex justify-content-between">
			<button class="slidenav__item slidenav__item--prev mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab"><i class="material-icons text-white">keyboard_arrow_left</i></button>
			<button class="slidenav__item slidenav__item--next mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab"><i class="material-icons text-white">keyboard_arrow_right</i></button>
		</nav>
	</div>

	<div class="container main-wrapper">
		
		<div class="main_area radius5 overflow-hidden mdl-shadow--16dp mb-5">

			<div class="layer1">
				<div class="container">

					<div class="row">
						<?php $__currentLoopData = $newest_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newest_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="col-sm-6 d-flex">

							<div class="row">
								<div class="col-md-6 py-5">
									<div class="container newItemWrapper">
										<div>
											<label class="text-uppercase text-white py-1 px-2 radius3 bg-black"><small>new</small></label>
										</div>
										<div class="container h-100 hiddenNewItem">
											<div class="item_img_container h-100 d-flex">
												<div class="item_img_holder d-flex align-content-center">
													<img src="<?php echo e(asset('assets/images/yellow.png')); ?>" class="img-fluid d-flex align-self-center">
												</div>
											</div>
										</div>
										<div class="newItemName_button_wrapper">
											<p class="h1 text-uppercase Lspacing2 text-white my-5 newItemName"><?php echo e($newest_product->name); ?></p>
											<div class="moreInfoContainer d-flex">
											<a href="<?php echo e(URL('product/'. Crypt::encrypt($newest_product->id))); ?>" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton2 px-5 py-2">more&nbsp;info</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6 py-5 visibleNewItem">
									<div class="container h-100">
										<div class="item_img_container h-100 d-flex">
											<div class="item_img_holder d-flex align-content-center">
												<img src="<?php echo e(asset('storage/products/'.$newest_product->image)); ?>" class="img-fluid d-flex align-self-center">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div><!-- END ROW -->

				</div>
			</div><!-- END LAYER1 -->

			<div class="layer2 position-relative py-5">
				
				<div class="deco deco--title"></div>
				<div id="e-slideshow" class="e-slideshow">
					<?php $__currentLoopData = $popular_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $popular_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="e-slide">
							<h2 class="e-slide__title e-slide__title--preview"><?php echo e($popular_product->product->name); ?> <span class="slide__price">₱<?php echo e($popular_product->product->price); ?>.00</span></h2>
							<div class="e-slide__item">
								<div class="e-slide__inner">
									<img class="e-slide__img e-slide__img--small" src="<?php echo e(asset('storage/products/' . $popular_product->product->image)); ?>" alt="Some image" />
									<a href="<?php echo e(url('/product/'. Crypt::encrypt($popular_product->product->id))); ?>"><button class="action action--open" aria-label="View details"><i class="material-icons mt-3">add_shopping_cart</i></button></a>
								</div>
							</div>
						</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<button class="action action--close" aria-label="Close"><i class="fa fa-close"></i></button>
				</div>
			</div><!-- END LAYER2 -->

			<div class="layer3">
				
				<h1 class="text-center text-uppercase Lspacing2 my-4">sale</h1>

				<div class="container item_sale_container">
					<div class="row">

						<?php $__currentLoopData = $random_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $random_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<!-- TO BE LOOPED -->
						<div class="col-sm-6 col-md-4 col-lg-3 item_sale_wrapper">
							<div class="container h-100 py-3">
								<div class="item_img_container h-100 d-flex flex-column">
									<div class="item_img_holder d-flex align-content-center">
										<img src="<?php echo e(asset('storage/products/'. $random_product->image)); ?>" class="img-fluid d-flex align-self-center">
									</div>
									<div class="mt-auto">
										<p class="lead text-uppercase m-0"><a href="<?php echo e(URL('product/'.Crypt::encrypt($random_product->id))); ?>" class="item_name"><?php echo e($random_product->name); ?></a></p>
										<?php 
											$price = $random_product->price;
											$result = ($price * 20) / 100;
											$total = $price + $result;
										?>
										<div class="d-flex">
											<p class="strike-through mr-3"><small>₱<?php echo e($total); ?>.00</small></p>
											<p class="sale_price">₱ <?php echo e($random_product->price); ?>.00</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						
					</div><!-- END ROW -->
				</div>

			</div>

			<div class="layer4">
				
				<div class="container">
					<div class="text-center">
						<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 py-2 px-5 my-5">go to shop</button>
					</div>
				</div>

			</div>

		</div>

	</div>

</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript">
		$('.mdl-navigation1').find('a:nth-child(1)').addClass('activeLink')
	</script>
	<!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/script.js')); ?>"></script>
	<?php echo $__env->make('includes.links-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<!-- elastic_slideshow --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/elastic_slideshow.js')); ?>"></script>
	<!-- slideshow --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/demo1.js')); ?>"></script>
	<script>
		(function() {
			document.documentElement.className = 'js'
			var slideshow = new CircleSlideshow(document.getElementById('e-slideshow'))
		})()
	</script>

	<script>
		// $(document).ready(function(){
		// 	$.ajax({
		// 		url 	: "http://localhost/sam-web_based/public/api/wishlist/remove/" + 6,
		// 		type 	: "GET",
		// 		// data 	: {
		// 		// 	'id' 			: '11', //product_id
		// 		// 	'user_id' 		: '6'
		// 		// },
		// 		success : function(data) {
		// 			console.log(data)
		// 		},
		// 		error 	: function(data) {
		// 			console.log(data)
		// 		}
		// 	})
		// })
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>