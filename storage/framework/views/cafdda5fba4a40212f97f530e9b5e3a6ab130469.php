<?php if( $cart !== null ): ?>
    <form action="<?php echo e(url('cart/update')); ?>" method="post" id="updateForm">
    <?php echo e(csrf_field()); ?>

    <?php $__empty_1 = true; $__currentLoopData = $cart->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
    <!-- TO BE LOOPED -->
    <tr>
        <td></td>
        <td>
            <div class="td_wrapper">
                <img src="<?php echo e(asset('storage/products/'.$item->product->image)); ?>" width="70px" class="border">
            </div>
        </td>
        <td class="text-uppercase">
            <div class="td_wrapper">
                <?php echo e($item->product->name); ?></td>
            </div>
        <td class="text-uppercase">
            <div class="td_wrapper">
                ₱<?php echo e($item->product->price); ?>.00</td>
            </div>
        <td class="text-uppercase">
            <div class="td_wrapper">
                <input type="hidden" name="id[]" value="<?php echo e($item->id); ?>">
                <input type="number" name="quantity[]" min="1" class="quantity p-3 text-center" value="<?php echo e($item->quantity); ?>">
            </div>
        </td>

        <td class="text-uppercase text-primary lead">
            <div class="td_wrapper">
                ₱<?php echo e($item->sub_total()); ?>.00</td>
            </div>
        <td>
            <div class="td_wrapper">
                <button data-id="<?php echo e(Crypt::encrypt($item->id)); ?>" data-product="<?php echo e($item->product->name); ?>" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab mdl-button--mini-fab removeToCart btnRemove" type="button">
                    <i class="material-icons">clear</i>
                </button>
            </div>
        </td>
    </tr><!-- END BE LOOPED -->
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <tr>
            <td>NO ITEM</td>
        </tr>
    <?php endif; ?>
<?php else: ?> 
    <tr>
        <td>NO ITEM</td>
    </tr>
<?php endif; ?>


