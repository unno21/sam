
<div class="row">
    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <!-- TO BE LOOPED -->
    <div class="col-sm-4 col-md-6 col-lg-4 item_sale_wrapper">
        <div class="container h-100 py-3">
            <div class="item_img_container h-100 d-flex flex-column">
                <div class="item_img_holder d-flex align-content-center">
                    <img src="<?php echo e(asset('storage/products/'. $product->image)); ?>" class="img-fluid d-flex align-self-center">
                </div>
                <div class="mt-auto">
                    <p class="lead text-uppercase m-0"><a href="<?php echo e(URL('/product/'. Crypt::encrypt($product->id))); ?>" class="item_name"><?php echo e($product->name); ?></a></p>
                    <div class="d-flex">
                        <p class="strike-through mr-3"><small>₱233.00</small></p>
                        <p class="sale_price">₱<?php echo e($product->price); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END TO BE LOOPED -->
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    

    <div class="col-12 shop_pagination border-top p-0">
        <div class="d-flex">
            <div>
                <button class="mdl-button mdl-js-button mdl-js-ripple-effect shop_paginate_nav  h-100"><i class="material-icons">keyboard_arrow_left</i></button>
            </div>
            <div class="container h-100 py-3 borderX">
                <div class="pagination-navigation">
                    <div class="pagination-current"></div>
                    <div class="pagination-dots">
                        <button class="pagination-dot paginate_active">
                            <span class="pagination-number">1</span>
                        </button>
                    </div>
                </div>
            </div>
            <div>
                <button class="mdl-button mdl-js-ripple-effect mdl-js-button shop_paginate_nav h-100"><i class="material-icons">keyboard_arrow_right</i></button>
            </div>
        </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
            </filter>
        </defs>
    </svg>

</div><!-- END ROW -->