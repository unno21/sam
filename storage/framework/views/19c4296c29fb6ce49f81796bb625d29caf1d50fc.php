<?php $__env->startSection('title'); ?>
    Order Detail | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="main-container">

<div class="banner">

    <div class="d-flex justify-content-center align-content-center h-100">
        
        <div class="logo_container h-100 d-flex flex-column">
            <img src="<?php echo e(asset('assets/images/logo.png')); ?>" class="img-fluid h-50 d-flex align-self-center mt-5 pt-5"><br>
            <p class="h5 text-uppercase Lspacing2 text-white text-center m-0 align-self-center">shopping assistant mirror</p>
        </div>

    </div>

</div>

<div class="container main-wrapper">
    
    <div class="main_area radius5 overflow-hidden mdl-shadow--16dp mb-5">

        <div class="container">
            <div class="d-flex justify-content-between px-5">
                <div>
                    <p class="h5 text-uppercase Lspacing2 pt-5 m-0">order detail</p>
                    <p class="text-uppercase text_grayish mb-4"><b>order no:</b><span class="text-primary"> #<?php echo e($order->order_number); ?></span></p>
                </div>
                <div>
                    <p class="h5 text-uppercase Lspacing2 pt-5 m-0">total : <span><b>₱<?php echo e(Crypt::decrypt($order->amount)); ?>.00</b></span></p>
                    <p class="text-uppercase mb-4"><b><span class="text_grayish">placed on:</span> <?php echo e(date('F m, Y', strtotime($order->created_at))); ?></b></p>
                </div>
            </div>

            <!-- PROCESS -->

            <div class="process container mb-5">
                
                <div class="row justify-content-center">
                    <div class="col-md-8">

                        <div class="d-flex justify-content-between position-relative align-content-center">
                            <div class="processCircle"></div>
                            <div class="processCircle activeProcess mdl-shadow--4dp"></div>
                            <div class="processCircle "></div>
                            <div class="progressBar w-100">
                                <div class="progressFill"></div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between position-relative align-content-center mt-2">
                            <p class="text-uppercase text_grayish processText"><b>processing</b></p>
                            <p class="lead text-uppercase processText activeprocessText"><b>shipped</b></p>
                            <p class="text-uppercase processText text_grayish"><b>delivered</b></p>
                        </div>

                    </div>
                </div>

            </div>

            <!-- MESSAGES -->

            <div class="message_container container mb-5">
                
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        
                        <div class="messagesWrapper radius5 p-4">

                              <div class="collapse" id="collapseExample">
                                  <div class="container pb-3">
                                      <!-- TO BE LOOPED -->
                                      <div class="row mb-2">
                                          <div class="col-sm-4">
                                              <p class="text_grayish text-uppercase"><b>august 4, 2018</b></p>
                                          </div>
                                          <div class="col-sm-8">
                                              <p><b>Your order has been shipped by our courier.</b></p>
                                          </div>
                                      </div><!-- END TO BE LOOPED -->
                                      <div class="row mb-2">
                                          <div class="col-sm-4">
                                              <p class="text_grayish text-uppercase"><b>august 4, 2018</b></p>
                                          </div>
                                          <div class="col-sm-8">
                                              <p><b>Your order has been shipped by our courier.</b></p>
                                          </div>
                                      </div>
                                      <div class="row mb-2">
                                          <div class="col-sm-4">
                                              <p class="text_grayish text-uppercase"><b>august 4, 2018</b></p>
                                          </div>
                                          <div class="col-sm-8">
                                              <p><b>Your order has been shipped by our courier.</b></p>
                                          </div>
                                      </div>
                                      <div class="row mb-2">
                                          <div class="col-sm-4">
                                              <p class="text_grayish text-uppercase"><b>august 4, 2018</b></p>
                                          </div>
                                          <div class="col-sm-8">
                                              <p><b>Your order has been shipped by our courier.</b></p>
                                          </div>
                                      </div>


                                  </div>
                              </div>
                            <center>
                                <button class="mdl-button mdl-js-button mdl-js-ripple-effect" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    view more
                                  </button>
                              </center>
                        </div>

                    </div>
                </div>

            </div>


            <div class="container">
                <div class="table-responsive">
                    <table class="table cart_table text-center">
                            <tr>
                                <th><div class="td_wrapper"></div></th>
                                <th><div class="td_wrapper">PRODUCT</div></th>
                                <th><div class="td_wrapper">PRICE</div></th>
                                <th><div class="td_wrapper">QUANTITY</div></th>
                                <th><div class="td_wrapper">TOTAL</div></th>
                                <th></th>
                            </tr>
                        <tbody>
                            <?php $__currentLoopData = $order->order_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <!-- TO BE LOOPED -->
                            <tr>
                                <td>
                                    <div class="td_wrapper">
                                        <img src="<?php echo e(asset('storage/products/'. $order_detail->product->image)); ?>" width="70px" class="border">
                                    </div>
                                </td>
                                <td class="text-uppercase"><div class="td_wrapper"><?php echo e($order_detail->product->name); ?></div></td>
                                <td class="text-uppercase">₱<?php echo e($order_detail->product->price); ?>.00</td>
                                <td class="text-uppercase">
                                    <div class="td_wrapper">
                                        <p class="lead">×<?php echo e($order_detail->quantity); ?></p>
                                    </div>
                                </td>
                                <?php 
                                    $total = $order_detail->quantity * $order_detail->product->price;
                                ?>
                                <td class="text-uppercase text-primary lead"><div class="td_wrapper">₱<?php echo e($total); ?>.00</div></td>
                                <td>
                                    <div class="td_wrapper">
                                        <a href="<?php echo e(url('/order/review/' . Crypt::encrypt($order_detail->product->id))); ?>">write a review</a>
                                    </div>
                                </td>
                            </tr><!-- END BE LOOPED -->

                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6"><div class="td_wrapper"></div></td>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- END TABLE CONTAINER -->
                
                <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 px-5 py-2 mb-5">continue shopping</button>

            </div>
        </div>

    </div>

</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript">
		$('.mdl-navigation2').find('a:nth-child(3)').addClass('activeLink')
	</script>
	<!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/script.js')); ?>"></script>
	<?php echo $__env->make('includes.links-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>