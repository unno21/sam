<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

    <title>Login | SAM</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- *** JQUERY *** -->

    <!-- Bootstrap jquery --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/code-jquery-3.2.1.slim.min.js')); ?>"></script>
    <!-- jquery --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/jquery-3.3.1.min.js')); ?>"></script>
    <!-- dragabilly --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/dragabilly.pkgd.js')); ?>"></script>
    
    <!-- custom css --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/custom/css/productPage.css')); ?>">
    <?php echo $__env->make('includes/links-styles', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <script>document.documentElement.className = 'js';</script>

</head>
<body class="loading render">
<!-- VERIFICATION MODAL -->
<div class="ui mini modal" id="cust_modal">
    <div class="header d-flex justify-content-between">
        <div class="header_title">VERIFICATION</div>
        <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
            <a href="#" class="close-button" id="hideModal">
                <div class="in">
                    <div class="close-button-block"></div>
                    <div class="close-button-block"></div>
                </div>
                <div class="out">
                    <div class="close-button-block"></div>
                    <div class="close-button-block"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="content">
        <div class="container">
            <form class="modal_form" id="addProduct" method="post" action="<?php echo e(url('verify')); ?>">
            <?php echo e(csrf_field()); ?>

                <!-- PRODUCT SKU -->
                <input type="text" name="code" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="Verification Code">

                <div class="actions text-center border-0 bg-white p-3">
                    <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored button myButton1 px-5 py-2 mb-4" name="signin">submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- FORGOT VERIFICATION MODAL -->
<div class="ui mini modal" id="verify_modal">
    <div class="header d-flex justify-content-between">
        <div class="header_title">VERIFICATION</div>
        <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
            <a href="#" class="close-button" id="hideModal">
                <div class="in">
                    <div class="close-button-block"></div>
                    <div class="close-button-block"></div>
                </div>
                <div class="out">
                    <div class="close-button-block"></div>
                    <div class="close-button-block"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="content">
        <div class="container">
            <form class="modal_form" id="addProduct" method="post" action="<?php echo e(url('verify_forgot')); ?>">
            <?php echo e(csrf_field()); ?>

                <!-- PRODUCT SKU -->
                <input type="text" name="code" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="Verification Code">
                <input type="hidden" name="email" id="emailVerify">
                <div class="actions text-center border-0 bg-white p-3">
                    <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored button myButton1 px-5 py-2 mb-4" name="signin">submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- FORGOT PASSWORD MODAL -->
<div class="ui mini modal" id="forgotPassword_modal">
    <div class="header d-flex justify-content-between">
        <div class="header_title">FORGOT PASSWORD</div>
        <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
            <a href="#" class="close-button" id="hideModal">
                <div class="in">
                    <div class="close-button-block"></div>
                    <div class="close-button-block"></div>
                </div>
                <div class="out">
                    <div class="close-button-block"></div>
                    <div class="close-button-block"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="content">
        <div class="container">
            <form class="modal_form" id="addProduct">
            <?php echo e(csrf_field()); ?>

                <!-- PRODUCT SKU -->
                <input type="text" name="" class="radius5 py-3 px-4 w-100 login_field" autocomplete="off" placeholder="Your Email Address...">
                <div class="d-flex w-100 align-items-center">
                    <hr class="w-50"><span class="mx-3 text_grayish">Or</span><hr class="w-50">
                </div>
                <input type="text" name="" class="radius5 py-3 px-4 w-100 login_field" autocomplete="off" placeholder="Use Phone Number...">
            </form>
        </div>
    </div>
    <div class="actions text-center border-0 bg-white p-3">
        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored button myButton1 px-5 py-2 mb-4" name="signin">submit</button>
    </div>
</div>

<!-- VERIFY MODAL -->
<div class="ui mini modal" id="forgotVerify_modal">
    <div class="header d-flex justify-content-between">
        <div class="header_title">VERIFY YOUR ACCOUNT</div>
        <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
            <a href="#" class="close-button" id="hideModal">
                <div class="in">
                    <div class="close-button-block"></div>
                    <div class="close-button-block"></div>
                </div>
                <div class="out">
                    <div class="close-button-block"></div>
                    <div class="close-button-block"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="content">
        <div class="container">
            <form class="modal_form">
            <?php echo e(csrf_field()); ?>

                <!-- PRODUCT SKU -->
                <input type="text" name="code" id="emailAddress" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="Your Email Address">

                <div class="actions text-center border-0 bg-white p-3">
                    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored button myButton1 px-5 py-2 mb-4" name="signin" id="resendEmail">submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- main container -->
<div class="main-container">
    <div class="modalContainer row" id="modalContainer">
        <div class="modalBGblue col-5"></div>
        <div class="container modal-wrapper d-flex p-0 radius5">
            <div class="container modal-area row m-0 p-0 radius5 overflow-hidden">

                <div class="col-md-6 py-5 rightSide">
                    <div class="container logoArea mt-4 mb-5 text-center text-white">
                        <div class="align-self-center"><img src="<?php echo e(asset('assets/images/logo.png')); ?>" class="img-fluid w-75"></div>
                        <p class="mb-5 mt-2 text-center text-uppercase Lspacing2 lead">shopping assistant mirror</h2>
                        <p class="text-center lead mb-0 mt-5">Whoevery said that money can't buy happiness,</p>
                        <p class="text-center lead mb-5">simply didn't know where to go shopping.</p>
                        <!-- <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 px-5 py-2 mt-5">read more</button> -->
                    </div>
                </div>

                <div class="col-md-6 leftSide py-5">
                    <div class="loginArea h-100 w-100">
                        <center>
                            <nav class="cust_tabs mt-4 p-0">
                                <div class="cust_selector" id="cust_selector"></div>
                                <a href="#" class="active" id="mediaLibrary">SIGN IN</a>
                                <a href="#" class="" id="uploadFiles">SIGN UP</a>
                            </nav>
                        </center>
                        <ul class="nav nav-tabs d-none">
                            <li class="active"><a href="#tab1" data-toggle="tab">SIGN IN</a></li>
                            <li><a href="#tab2" data-toggle="tab">SIGN UP</a></li>
                        </ul>
                        <div class="tab-content mt-4">
                            <div class="tab-pane active" id="tab1">

                                <form method="post" action="<?php echo e(URL('/signin')); ?>" class="w-100 align-self-center">
                                <?php echo e(csrf_field()); ?>

                                    <h1 class="text-center mycolor">Sign In</h1>
                                    <center><label for="" style="color:red;"><?php echo e(session('errors') ? session('errors')->first('error') : ''); ?></label></center>
                                    <div class="mx-4 inputs">
                                        <center>
                                            <input type="email" name="email" class="radius5 py-3 px-4 w-75 login_field mb-4" autocomplete="off" placeholder="Email">
                                            <input type="password" name="password" class="radius5 py-3 px-4 w-75 login_field mb-3" autocomplete="off" placeholder="Password">
                                            
                                        </center>
                                        <div>
                                        
                                            <div class="d-flex justify-content-between w-75 mx-auto position-relative">
                                                <div class="d-flex">
                                                    <div class="toggle_wrapper d-flex">
                                                        <label for="bubble">
                                                            <div class="form">
                                                                <input type="checkbox" id="bubble" />
                                                                <label class="bubble" for="bubble"></label>
                                                            </div>
                                                        </label>
                                                        <label for="bubble" class="mt-3 rememberMe">Remember me</label>
                                                    </div>
                                                </div>
                                                <a href="#" id="open_forgotPassword_modal" class="mt-3">Forgot Password?</a>
                                                <a href="#" id="resendcode" class="mt-3">Forgot to verify account?</a>
                                            </div>
                                        </div>
                                    </div>
                                    <center><div class="container signin mt-5">
                                        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored button myButton1 px-5 py-2 mb-4" name="signin">sign in</button>
                                    </div></center>
                                    <div class="container socialSignin mt-5">
                                        <p>Or Sign in with</p>
                                        <div class="socialIcons">
                                            <button class="ui circular facebook icon button px-3" type="button">
                                              <i class="fa fa-facebook-f"></i>
                                            </button>
                                            <button class="ui circular twitter icon button">
                                              <i class="fa fa-twitter"></i>
                                            </button>
                                            <button class="ui circular google plus icon button">
                                              <i class="fa fa-google-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div class="tab-pane" id="tab2">
                                
                                <form method="post" action="<?php echo e(URL('/register')); ?>" class="w-100 align-self-center">
                                <?php echo e(csrf_field()); ?>

                                    <h1 class="text-center mycolor">Sign Up</h1>
                                    <div class="mx-4 inputs">
                                        <center>
                                            <input type="text" name="first_name" class="radius5 py-3 px-4 w-75 login_field mb-3" autocomplete="off" placeholder="First name" value="">
                                            <label for="" style="color:red;"><?php echo e($errors->has('first_name') ? $errors->first('first_name') : ''); ?></label>
                                            <input type="text" name="last_name" class="radius5 py-3 px-4 w-75 login_field mb-3" autocomplete="off" placeholder="Last name" value="">
                                            <label for="" style="color:red;"><?php echo e($errors->has('last_name') ? $errors->first('last_name') : ''); ?></label>
                                            <input type="text" name="username" class="radius5 py-3 px-4 w-75 login_field mb-3" autocomplete="off" placeholder="Username" value="">
                                            <label for="" style="color:red;"><?php echo e($errors->has('username') ? $errors->first('username') : ''); ?></label>
                                            <input type="email" name="email" class="radius5 py-3 px-4 w-75 login_field mb-3" autocomplete="off" placeholder="Email" value="">
                                            <label for="" style="color:red;"><?php echo e($errors->has('email') ? $errors->first('email') : ''); ?></label>
                                            <input type="number" name="phone_number" class="radius5 py-3 px-4 w-75 login_field mb-3" placeholder="Phone Number">
                                            <label for="" style="color:red;"><?php echo e($errors->has('phone_number') ? $errors->first('phone_number') : ''); ?></label>
                                            <input type="password" name="password" class="radius5 py-3 px-4 w-75 login_field mb-3" autocomplete="off" placeholder="Password" value="">
                                            <label for="" style="color:red;"><?php echo e($errors->has('password') ? $errors->first('password') : ''); ?></label>
                                            <input type="password" name="retype_password" class="radius5 py-3 px-4 w-75 login_field mb-3" autocomplete="off" placeholder="Confirm Password" value="">
                                            <label for="" style="color:red;"><?php echo e($errors->has('retype_password') ? $errors->first('retype_password') : ''); ?></label>
                                        </center>
                                    </div>
                                    <center><div class="container signin mt-5">
                                        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored button myButton1 px-5 py-2 mb-4" name="signin">sign up</button>
                                    </div></center>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>




</body>
<!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/script.js')); ?>"></script>
<?php echo $__env->make('includes/links-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script>
    $('#cust_modal').modal('attach events', '#openModal', 'show')
    $('#cust_modal').modal('attach events', '.close-button', 'hide')
    $("#forgotPassword_modal").modal('attach events', '#open_forgotPassword_modal', 'show')
    $("#forgotPassword_modal").modal('attach events', '.close-button', 'hide')
    $("#forgotVerify_modal").modal('attach events', '#resendcode', 'show')
    $("#forgotVerify_modal").modal('attach events', '.close-button', 'hide')

    $(document).ready(function(){
        

        if ( '<?php echo e($label); ?>' == "yes") {
            $('#cust_modal').modal('show')
        } else if ( '<?php echo e($label); ?>' == "status") {
            alert('Please verify your account.')
        }

        if ( '<?php echo e($status); ?>' != 'no') {
            alert('<?php echo e($status); ?>')
        }

        $('#resendEmail').on('click',function(e) {
            e.preventDefault()
            var email = $('#emailAddress').val()
            $.ajax({
                url     : "<?php echo e(url('/resend')); ?>/" + email,
                type    : "get",
                success : function(data) {
                    
                    if (data.error) {
                        alert(data.error)
                        $("#forgotVerify_modal").modal('hide')
                    } else {
                        $('#verify_modal').modal('show')
                        $('#emailVerify').val(email)
                    }
                },
                error   : function(data) {
                    console.log(data)
                }
            })
        })
    });
    
</script>
</html>
