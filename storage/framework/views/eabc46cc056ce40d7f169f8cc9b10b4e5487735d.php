<?php $__env->startSection('title'); ?>
    Customers | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="main-container w-100">

        <div class="main-wrapper">
            <div class="container">
                             
                <div class="py-5">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered bg-white mdl-shadow--4dp" id="iReportsTbl">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Username</th>
                                    <th>E-mail</th>
                                    <th>Billing Address</th>
                                    <th>Shipping Address</th>
                                    <th>Orders</th>
                                    <th>Items Cancelled</th>
                                    <th>Status</th>
                                    <th>Message</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($user->full_name()); ?></td>
                                    <td><?php echo e($user->username); ?></td>
                                    <td><?php echo e($user->email); ?></td>
                                    <td>address 1</td>
                                    <td>address 2</td>
                                    <td><?php echo e(count($user->orders)); ?></td>
                                    <td>2</td>
                                    <td>
                                        <span class="onlineStatus online"></span>Online
                                    </td>
                                    <td><a href="#">Send an E-mail</a></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="9">
                                        <button class="mdl-button mdl-js-ripple-effect mdl-js-button mdl-button--raised mdl-button--colored myButton1 px-4 py-2 d-flex align-items-center">
                                            <i class="material-icons-new outline-print icon-white mr-2"></i>Print Report
                                        </button>
                                    </th>
                                </tr>
                            </tfoot>
                            <tfoot>
                                <tr>
                                    <th colspan="9">
                                        <div class="pagination-navigation">
                                            <div class="pagination-current cust_gradient"></div>
                                            <div class="pagination-dots" id="iReportsPag">
                                                <button class="pagination-dot paginate_active">
                                                    <span class="pagination-number">1</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">2</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">3</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">4</span>
                                                </button>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </tfoot>
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
                                <defs>
                                    <filter id="goo">
                                        <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                                        <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                                        <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
                                    </filter>
                                </defs>
                            </svg>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript" src="<?php echo e(asset('assets/custom/js/admin.js')); ?>"></script>
    <script>
        $(".custSNL").addClass("SNLactive")
        $(".custSNL a").css("color","white")  
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('back-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>