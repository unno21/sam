

<form action="<?php echo e(URL('/reset-password')); ?>" method="POST">
    <?php echo e(csrf_field()); ?>

    <input type="hidden" name="id" value="<?php echo e($id); ?>" />
    <input type="password" name="password" />
    <input type="password" name="confirm_password" />
    <input type="submit" name="submit" value="Reset Password" />
</form>