<?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
        <td><?php echo e($employee->full_name()); ?></td>
        <td><?php echo e($employee->email); ?></td>
        <td><?php echo e($employee->username); ?></td>
        <td class="text-capitalize"><?php echo e($employee->type); ?></td>
        <td>
            <span class="onlineStatus online"></span>Online
        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>