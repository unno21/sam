<?php $__env->startSection('title'); ?>
    Cart | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="main-container">

	<div class="banner">

		<div class="d-flex justify-content-center align-content-center h-100">
			
			<div class="logo_container h-100 d-flex flex-column">
				<img src="<?php echo e(asset('assets/images/logo.png')); ?>" class="img-fluid h-50 d-flex align-self-center mt-5 pt-5"><br>
				<p class="h5 text-uppercase Lspacing2 text-white text-center m-0 align-self-center">shopping assistant mirror</p>
			</div>

		</div>

	</div>

	<div class="container main-wrapper">
		
		<div class="main_area radius5 overflow-hidden mdl-shadow--16dp mb-5">

			<div class="container">
				<p class="h5 text-uppercase Lspacing2 py-5 ml-5">cart</p>

				<div class="container">
					<div class="table-responsive">
						<table class="table cart_table text-center">
							<thead>
								<tr>
									<th></th>
									<th>PRODUCT</th>
									<th>PRICE</th>
									<th>QUANTITY</th>
									<th>TOTAL</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php
									$grandtotal = 0;
								?>
								<?php if( count($cart) > 0 ): ?>
									<?php $__empty_1 = true; $__currentLoopData = $cart->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
									<!-- TO BE LOOPED -->
									<tr>
										<td>
											<img src="<?php echo e(asset('storage/products/'.$item->product->image)); ?>" width="70px" class="border">
										</td>
										<td class="text-uppercase"><?php echo e($item->product->name); ?></td>
										<td class="text-uppercase">₱<?php echo e($item->product->price); ?>.00</td>
										<td class="text-uppercase">
											<input type="number" name="" min="1" class="quantity p-3 text-center" value="<?php echo e($item->quantity); ?>">
										</td>
										<?php 
											$total = $item->product->price * $item->quantity;
											$grandtotal += $total;
										?>
										<td class="text-uppercase text-primary lead">₱<?php echo e($total); ?>.00</td>
										<td>
											<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab mdl-button--mini-fab removeToCart">
												<i class="material-icons">clear</i>
											</button>
										</td>
									</tr><!-- END BE LOOPED -->
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
										<tr>
											<td>NO ITEM</td>
										</tr>
									<?php endif; ?>
								<?php else: ?> 
									<tr>
										<td>NO ITEM</td>
									</tr>
								<?php endif; ?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="6"></td>
								</tr>
							</tfoot>
						</table>
					</div><!-- END TABLE CONTAINER -->

					<div class="d-flex justify-content-end">
						<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 px-5 py-2 mb-5">update cart</button>
					</div>
					<form action="<?php echo e(url('order/insert')); ?>" method="post">
					<?php echo e(csrf_field()); ?>

						<input type="hidden" name="cart_id" id="" value="<?php echo e(count($cart) > 0 ? $cart->id : ''); ?>">
						<input type="hidden" name="grandtotal" value="<?php echo e($grandtotal); ?>">
						<div class="mb-5">
							<p class="h5 text-uppercase">cart total: <span class="text-primary">₱<?php echo e($grandtotal); ?>.00</span></p>
						</div>
						<button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 px-5 py-2 mb-5">proceed to checkout</button>
					</form>
					
				</div>
			</div>

		</div>

	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
	<script type="text/javascript">
		$('.mdl-navigation1').find('a:nth-child(1)').addClass('activeLink')
	</script>
	<!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/script.js')); ?>"></script>
	<?php echo $__env->make('includes.links-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<script>
	
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>