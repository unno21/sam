<?php $__env->startSection('title'); ?>
    Shop | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="main-container">

        <div class="banner">

            <div class="d-flex justify-content-center align-content-center h-100">
                
                <div class="logo_container h-100 d-flex flex-column">
                    <img src="assets/images/logo.png" class="img-fluid h-50 d-flex align-self-center mt-5 pt-5"><br>
                    <p class="h5 text-uppercase Lspacing2 text-white text-center m-0 align-self-center">shopping assistant mirror</p>
                </div>

            </div>

        </div>

        <div class="container main-wrapper">
            
            <div class="main_area radius5 overflow-hidden mdl-shadow--16dp mb-5">

                <div class="layer1">
                    <div class="container">

                        <div class="row">
                            <?php $__currentLoopData = $newest_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newest_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-6">

                                <div class="row">
                                    <div class="col-sm-6 py-5">
                                        <div class="container">
                                            <label class="text-uppercase text-white py-1 px-2 radius3 bg-black"><small>new</small></label>
                                            <p class="h1 text-uppercase Lspacing2 text-white my-5"><?php echo e($newest_product->name); ?></p>
                                            <a href="<?php echo e(url('product/'. Crypt::encrypt($newest_product->id))); ?>"><button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton2 px-5 py-2">more&nbsp;info</button></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 py-5">
                                        <div class="container h-100">
                                            <div class="item_img_container h-100 d-flex">
                                                <div class="item_img_holder d-flex align-content-center">
                                                    <img src="<?php echo e(asset('storage/products/'. $newest_product->image)); ?>" class="img-fluid d-flex align-self-center">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div><!-- END ROW -->
                        

                    </div>
                </div><!-- END LAYER1 -->

                <div class="container main_shop">
                    
                    <div class="row">
                        <div class="col-md-4 col-lg-3 p-0">

                            <div class="container py-4">
                                <p class="lead text-uppercase Lspacing2 px-3">filter</p>
                            </div>

                            <div class="accordion" id="accordionExample">
                                <div class="card border-top">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link text-uppercase d-flex" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <i class="material-icons mr-2">keyboard_arrow_down</i> <b>categories</b>
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="categ_list">
                                                <ul>
                                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li class="d-flex justify-content-between"><a href="" class="category_trigger" data-id="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></a><span class="mt-1"><?php echo e(count($category->products)); ?></span></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link text-uppercase d-flex" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <i class="material-icons mr-2">keyboard_arrow_down</i> <b>price</b>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="price_range">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <input type="number" name="" placeholder="From" class="p-3 w-100 price-range-field mb-4 range" min=0 oninput="validity.valid||(value='0');" id="min_price" >
                                                        </div>	
                                                        <div class="col-lg-6">
                                                            <input type="number" name="" placeholder="To" class="p-3 w-100 price-range-field mb-4 range" min=0 max="300" oninput="validity.valid||(value='300');" id="max_price">
                                                        </div>	
                                                    </div>
                                                    <div class="py-3">
                                                        <div id="slider-range" class="price-filter-range radius5" name="rangeInput" id="price_range"></div>
                                                        <input type="hidden" name="" value="<?php echo e($max_value->price); ?>" id="max_value">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-8 col-lg-9" id="content">
                            <?php echo $__env->make('front-end.shop.includes.index-inner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                        

                    </div>

                </div>

            </div>

        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript">
        /* test API */
        // $.ajax({
        //     url             : "<?php echo e(URL('api/product')); ?>",
        //     type            : "get",
        //     succcess        : function(data) {
        //         console.log(data)
        //     },
        //     error           : function(data) {
        //         console.log(data)
        //     }
        // })

		$('.mdl-navigation1').find('a:nth-child(3)').addClass('activeLink')
	</script>
	<!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/script.js')); ?>"></script>
	<?php echo $__env->make('includes.links-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script>
    $(document).ready(function(){
        
    });
        $('.category_trigger').on('click', function(e){
            e.preventDefault();
            var id = $(this).data('id')
            $.ajax({
                type        : "get",
                url         : "<?php echo e(url('shop/filter')); ?>/" + id,
                success     : function(data) {
                            $('#content').empty();
                            $('#content').append(data.content)
                },
                error       : function(data) {
                            console.log(data)
                },
            });
        });
       
        $('.range').on('change', function(e){
            var min = $('#min_price').val()
            var max = $('#max_price').val()
            price_range(min, max)
        });

        function price_range(min_price, max_price)
        {
        var min = min_price;
        var max = max_price;
        $.ajax({
            url     : "<?php echo e(url('shop/range_filter')); ?>/" + min + '/' + max ,
            type    : "get",
            success : function(data) {
                        $('#content').empty();
                        $('#content').append(data.content)
            },
            error   : function(data) {
                    console.log(data)
            }
        });
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>