<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

	<title><?php echo $__env->yieldContent('title'); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- <link rel="icon" type="img/icon" href="<?php echo e(asset('assets/images/logo.png')); ?>"> -->

	<!-- *** JQUERY *** -->

	<!-- Bootstrap jquery --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/code-jquery-3.2.1.slim.min.js')); ?>"></script>
	<!-- jquery --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/jquery-3.3.1.min.js')); ?>"></script>
	<!-- jquery --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/jquery-ui.js')); ?>"></script>
	
	<!-- custom css --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/custom/css/style.css')); ?>">
	<?php echo $__env->make('includes.links-styles', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<script>document.documentElement.className = 'js';</script>

</head>
<body class="demo-1">
	
	<div class="top_nav">
		<div class="container-fluid">
			<ul class="m-0 p-0">
				<li><a href="#"><img src=""><img src="<?php echo e(asset('assets/images/facebook.png')); ?>" height="15px"></a></li>
				<li><a href="#"><img src=""><img src="<?php echo e(asset('assets/images/twitter.png')); ?>" height="15px"></a></li>
				<li><a href="#"><img src=""><img src="<?php echo e(asset('assets/images/instagram.png')); ?>" height="15px"></a></li>
				<li><a href="#"><img src=""><img src="<?php echo e(asset('assets/images/google.png')); ?>" height="15px"></a></li>
			</ul>
		</div>
	</div>
	<div class="mdl-layout mdl-js-layout mdl-shadow--4dp" id="mdl-layout">
	  <header class="mdl-layout__header">
		    <div class="mdl-layout__header-row px-0">
		      	<!-- Title -->
		      	<span class="mdl-layout-title px-3 h-100">
		      		<a href="#"><img src="<?php echo e(asset('assets/images/logo.png')); ?>" class="mt-3"></a>
		      	</span>
		      	<nav class="mdl-navigation mdl-navigation1">
			        <a class="mdl-navigation__link text-uppercase Lspacing2" href="<?php echo e(URL('/')); ?>">Home</a>
			        <a class="mdl-navigation__link text-uppercase Lspacing2" href="">About</a>
			        <a class="mdl-navigation__link text-uppercase Lspacing2" href="<?php echo e(URL('/shop')); ?>">Shop</a>
		      	</nav>
		      	<!-- Add spacer, to align navigation to the right -->
		      	<div class="mdl-layout-spacer"></div>
		      	<!-- Navigation -->
		      	<nav class="mdl-navigation mdl-navigation2">
			        <a class="mdl-navigation__link" href="<?php echo e(URL('/wishlist')); ?>"><i class="ion-ios-heart-outline"></i></a>
			        <a class="mdl-navigation__link" href="<?php echo e(URL('/cart')); ?>"><i class="ion-ios-cart-outline"></i></a>
			        <a class="mdl-navigation__link" href="<?php echo e(URL('/account')); ?>"><i class="ion-ios-contact-outline"></i></a>
		      	</nav>
		    </div>
	  	</header>
	  	<div class="mdl-layout__drawer">
	    	<span class="mdl-layout-title px-3">
	      		<a href="#"><img src="<?php echo e(asset('assets/images/logo.png')); ?>" class="mt-3"></a>
	      	</span>
	    	<nav class="mdl-navigation">
	      		<a class="mdl-navigation__link text-uppercase Lspacing2" href="home.php">Home</a>
		        <a class="mdl-navigation__link text-uppercase Lspacing2" href="">About</a>
		        <a class="mdl-navigation__link text-uppercase Lspacing2" href="shop.php">Shop</a>
	    	</nav>
	  	</div>
	</div>

    <?php echo $__env->yieldContent('content'); ?>

<div class="footer_container">
	<div class="footer1 pt-5">
		<div class="container py-5">
			
			<h1 class="text-center text-uppercase Lspacing2 pt-5 mb-5">subscribe to our newsletter</h1>
			<div class="container">
				<div class="row justify-content-center mb-5">
					<div class="col-sm-7">
						<p class="text-center lead">Tenetur dolorem alias ad veniam voluptatum cupiditate deleniti eos, quia accusamus, autem numquam.</p>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-sm-7">
						<div class="subscribe_txtbox_wrapper row w-100 py-3 mdl-shadow--8dp overflow-hidden">
							<div class="col-10">
								<input type="text" name="" class="subscribe_txtbox w-100 px-5" placeholder="YOUR EMAIL ADRESS">
							</div>
							<div class="col-2">
								<p class="text-center m-0">
									<i class="ion-ios-email-outline mail"></i>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div><!-- END FOOTER 1 -->

	<div class="footer2 pb-5">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-3">
					<h2 class="text-white Lspacing2 mt-5">SAM</h2>
					<p class="text-white pt-2">SAM is a shopping assistant mirror with augmented reality to improve the customer shopping experience.</p>
				</div>
				<div class="col-sm-3">
					<h2 class="text-white Lspacing2 mt-5">SITE MAP</h2>
					<div class="sitemap">
						<ul class="m-0 p-0">
							<li><a href="home.php">Home</a></li>
							<li><a href="#">About</a></li>
							<li><a href="shop.php">Shop</a></li>
							<li><a href="cart.php">Cart</a></li>
							<li><a href="account.php">Account</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6">
					<h2 class="text-white Lspacing2 mt-5">CONTACT US</h2>
					<div class="contactus">
						<form>
							<div class="row">
								<div class="col-sm-12 col-md-12 col-lg-9">
									<input type="text" name="" placeholder="NAME *" class="mb-3 p-3 radius3 mdl-shadow--4dp w-100" autocomplete="off">
								</div>
								<div class="col-sm-12 col-md-12 col-lg-9">
									<input type="text" name="" placeholder="EMAIL *" class="mb-3 p-3 radius3 mdl-shadow--4dp w-100" autocomplete="off">
								</div>
								<div class="col-sm-12 col-md-12 col-lg-9">
									<textarea  name="" placeholder="EMAIL *" class="mb-3 p-3 radius3 mdl-shadow--4dp w-100" rows="6"></textarea>
								</div>
								<div class="col-sm-12 col-md-12 col-lg-9"><button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored w-100 myButton3">submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>
	</div><!-- END FOOTER 2 -->
</div>
</body>
    <?php echo $__env->yieldContent('js'); ?>
</html>

