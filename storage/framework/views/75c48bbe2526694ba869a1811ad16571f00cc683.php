<?php $__currentLoopData = $addresses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $address): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr>
    <td class="text-capitalize"><?php echo e($address->full_name); ?></td>
    <td><?php echo e($address->address); ?></td>
    <td><?php echo e($address->post_code); ?></td>
    <td><?php echo e($address->phone_number); ?></td>
    <td><button class="mdl-button mdl-js-button mdl-js-ripple-effect edit_address" id="edit_address" data-id="<?php echo e($address->id); ?>">edit</button></td>
</tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>