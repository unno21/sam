<script>

function postAddProduct(){
    $('#addProduct').submit(function(e){
    e.preventDefault();
    var formData = new FormData($(this)[0]);
        $.ajax({
            'url'			: "<?php echo URL('admin/products/insert'); ?>",
            'method'		: 'post',
            'dataType'      : 'json',
            'data'			: formData,
            success 		: function(data){
                            $('#content').empty();
                            $('#content').append(data.content);
                            console.log(data);
            },
            error           : function(data){
                            console.log(data);
            },
            contentType		: false,
            cache			: false,
            processData		: false
        });
});
}

</script>