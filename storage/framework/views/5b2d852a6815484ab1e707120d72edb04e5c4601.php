<!-- *** SCRIPTS *** -->

	<!-- Bootstrap popper --><script type="text/javascript" src="<?php echo e(asset('assets/bootstrap/popper.min.js')); ?>"></script>
	<!-- Bootstrap --><script type="text/javascript" src="<?php echo e(asset('assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
	<!-- Material Design Lite --><script type="text/javascript" src="<?php echo e(asset('assets/MDL/material.min.js')); ?>"></script>
	<!-- Material Design Lite --><script type="text/javascript" src="<?php echo e(asset('assets/getmdl-select-master/getmdl-select.min.js')); ?>"></script>
	<!-- Semantic UI --><script type="text/javascript" src="<?php echo e(asset('assets/semantic/semantic.min.js')); ?>"></script>
	<!-- GoogleNexusMenu --><script type="text/javascript" src="<?php echo e(asset('assets/GoogleNexusWebsiteMenu/js/gnmenu.js')); ?>"></script>
	<!-- GoogleNexusMenu --><script type="text/javascript" src="<?php echo e(asset('assets/GoogleNexusWebsiteMenu/js/classie.js')); ?>"></script>
	<!-- cust-button --><script type="text/javascript" src="<?php echo e(asset('assets/cust-button/js/index.js')); ?>"></script>
	<!-- chartist-js-master --><script type="text/javascript" src="<?php echo e(asset('assets/chartist-js-master/dist/chartist.min.js')); ?>"></script>
	<!-- CreativeGooeyEffects --><script type="text/javascript" src="<?php echo e(asset('assets/CreativeGooeyEffects/js/TweenMax.min.js')); ?>"></script>
	<!-- CreativeGooeyEffects --><script type="text/javascript" src="<?php echo e(asset('assets/CreativeGooeyEffects/js/pagination.js')); ?>"></script>
	<!-- CreativeGooeyEffects --><script type="text/javascript" src="<?php echo e(asset('assets/CreativeGooeyEffects/js/select.js')); ?>"></script>
	<!-- slideshow --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/demo.js')); ?>"></script>
	<!-- slideshow --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/anime.min.js')); ?>"></script>
	<!-- slideshow --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/demo1.js')); ?>"></script>
	<!-- elastic_slideshow --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/elastic_slideshow_dynamics.min.js')); ?>"></script>
	<!-- elastic_slideshow --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/elastic_slideshow.js')); ?>"></script>
	<!-- shape_overlay --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/easings.js')); ?>"></script>
	<!-- shape_overlay --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/shape_overlay.js')); ?>"></script>
	<!-- shape_overlay --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/jquery-ui.js')); ?>"></script>
	<script>
		(function() {
			document.documentElement.className = 'js'
			var slideshow = new CircleSlideshow(document.getElementById('e-slideshow'))
		})()
		$(document).ready( function() {
		  	var $draggable = $('.draggable').draggabilly()
	  		// Draggabilly instance
		  	var draggie = $draggable.data('draggabilly')
		  	$draggable.on( 'dragMove', function() {
		    	console.log( 'dragMove', draggie.position.x, draggie.position.y )
		  	})
		})
		new gnMenu( document.getElementById( 'gn-menu' ) )
	</script>