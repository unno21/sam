
<h1>You need to verify your account.</h1>

<?php if(Session::has('success')): ?>
    <div class="alert alert-success">
        <?php echo e(session('success')); ?>

    </div>
<?php endif; ?>
<?php if(Session::has('failed')): ?>
    <div class="alert alert-danger">
        <?php echo e(session('failed')); ?>

    </div>
<?php endif; ?>

<form action="<?php echo e(URL('/verify')); ?>" method="POST">
    <?php echo e(csrf_field()); ?>

    <input type="text" name="code" placeholder="Verification Code" />
    <input type="submit" name="submit" value="Verify">
    <a href="<?php echo e(URL('/verify/send-code')); ?>">Resend Code</a>
</form>