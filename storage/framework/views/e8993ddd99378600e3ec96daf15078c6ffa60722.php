
<?php $__currentLoopData = $order_details->order_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="orders_container my-3">
    <!-- to be looped -->
    <div class="container-fluid order_item py-3">
        <div class="row">
            <div class="col-sm-3">
                <div class="d-flex w-100 justify-content-center align-items-center h-100">
                    <div class="align-self-center">
                        <img src="<?php echo e(asset('storage/products/' . $order_detail->product->image)); ?>" height="100px">
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <h2 class="mb-0"><?php echo e($order_detail->product->name); ?></h2>
                <p class="text_grayish text-uppercase"><sup><?php echo e($order_detail->product->category->name); ?></sup></p>
                <div class="color_icon yellow mb-3"></div>
                <p>PRICE <?php echo e($order_detail->product->price); ?> X QUANTITY <?php echo e($order_detail->quantity); ?> = <?php echo e($total = $order_detail->product->price * $order_detail->quantity); ?></p>
            </div>
        </div>
    </div>
    <!-- end to be looped -->
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>