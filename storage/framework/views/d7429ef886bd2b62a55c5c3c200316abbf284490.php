<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Export Data</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <center><h2><?php echo e($title); ?></h2></center>
    <div class="table-responsive bg-white mdl-shadow--2dp" id="table-container">
        <table class="table table-hover mb-0">
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Username</th>
                    <th>E-mail</th>
                    <th>Shipping Address</th>
                    <th>Orders</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($user->full_name()); ?></td>
                        <td><?php echo e($user->username); ?></td>
                        <td><?php echo e($user->email); ?></td>
                        <td><?php echo e($user->address == null ? 'Does not set his/her address yet' : $user->address); ?></td>
                        <td><?php echo e(count($user->orders)); ?></td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</body>
</html>