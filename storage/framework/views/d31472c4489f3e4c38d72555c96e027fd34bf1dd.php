<div class="table_container table-responsive mdl-shadow--8dp mb-0 mt-3">
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th scope="col">
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                        <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                    </label>
                </th>
                <th scope="col">
                    <i class="material-icons">image</i>
                </th>
                <th scope="col">NAME</th>
                <th scope="col">STOCK</th>
                <th scope="col">PRICE</th>
                <th scope="col">CATEGORIES</th>
                <!-- <th scope="col">TAGS</th> -->
                <!-- <th scope="col">FEATURED</th> -->
                <th scope="col">DATE</th>
                <th scope="col">ACTION</th>
            </tr>
        </thead>
        <tbody>
            <?php $__empty_1 = true; $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <tr>
                <th scope="row">
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-<?php echo e($product->id); ?>">
                        <input type="checkbox" id="checkbox-<?php echo e($product->id); ?>" class="mdl-checkbox__input">
                    </label>
                </th>
                <td>
                    <img src="<?php echo e(asset('storage/products/' . $product->image)); ?>" width="50px">
                </td>
                <td><?php echo e($product->name); ?></td>
                <td class="<?php echo e($product->stock !== 0 ? 'text-success' : 'text-danger'); ?>"><?php echo e($product->stock !== 0 ? 'In Stock' : 'Out of Stock'); ?></td>
                <td>₱ <?php echo e($product->price); ?></td>
            
                <td class="text-capitalize"><?php echo e($product->category->name); ?></td>
                <!-- <td>-</td> -->
                <!-- <td>
                    <div class="checkbox">
                        <input id="check1" name="check" type="checkbox" class="mo_chck" />
                        <label for="check1" id="lbl_check"></label>
                    </div>
                </td> -->
                <td>Published at<br><?php echo e(date('F d, Y', strtotime($product->created_at))); ?></td>
                <td>
                    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon overflow-visible " data-inverted="" data-position="top center" data-tooltip="Edit" >
                        <i class="material-icons-new outline-edit icon-action"></i>
                    </button>
                    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon overflow-visible btnDelete" data-inverted="" data-position="top center" data-tooltip="Delete" data-id="<?php echo e($product->id); ?>" >
                        <i class="material-icons-new outline-delete icon-action"></i>
                    </button>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?> 
            <tr><td colspan="8">NO ITEM</td></tr>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="10">
                    <div class="pagination-navigation">
                        <div class="pagination-current"></div>
                        <div class="pagination-dots">
                        <?php for($i = 1; $i <= $products->lastPage(); $i++): ?>
                        <!-- ACTIVE NOT WORKING -->
                            <button class="pagination-dot <?php echo e($products->currentPage() === $i ? 'paginate_active' : ''); ?>">
                                <a href="<?php echo e(URL('admin/products/'. '?page='. $i )); ?>"><span class="pagination-number"><?php echo e($i); ?></span></a>
                            </button>
                        <?php endfor; ?>
                        </div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
    
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
            </filter>
        </defs>
    </svg>

</div>