<div class="table_container table-responsive mdl-shadow--8dp mb-0 mt-3">
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th scope="col">ORDER NUMBER</th>
                <th scope="col">CUSTOMER</th>
                <th scope="col">ADDRESS</th>
                <th scope="col">TOTAL</th>
                <th scope="col">DATE OF PURCHASE</th>
                <th scope="col">STATUS</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><a href="#" data-id="<?php echo e($order->id); ?>" class="order_number text-uppercase">#<?php echo e($order->order_number); ?></a></td>
                <td><?php echo e($order->user->full_name()); ?></td>
                <td><?php echo e($order->address === null ? $order->user->address : $order->address); ?></td>
                <td>₱ <?php echo e(Crypt::decrypt($order->amount)); ?></td>
            
                <td><?php echo e(date('F d, Y', strtotime($order->created_at))); ?></td>
                <td class="text-uppercase <?php if($order->status === 0 ): ?> text-danger <?php elseif($order->status === 1): ?> text-secondary <?php else: ?> text-success <?php endif; ?>"><?php if($order->status === 0 ): ?> processed <?php elseif($order->status === 1): ?> shipped <?php else: ?> delivered <?php endif; ?></td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="10">
                    <?php echo e($orders->links("pagination::bootstrap-4")); ?>

                </td>
            </tr>
        </tfoot>
    </table>
    
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
            </filter>
        </defs>
    </svg>

</div>