<div class="d-flex justify-content-center">
    <div class="selection-highlights1 position-absolute d-flex flex-wrap justify-content-center">
        <!-- TO BE LOOPED ALSO AS THE SELECTION ITEM LOOPED -->
        <?php $counter = 0; ?>
        <?php $__currentLoopData = Storage::disk('products')->files('products'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filename): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <input class="selection-checkbox" type="checkbox" id="selection-check-<?php echo e($counter++); ?>">
            <div class="selection-highlight"></div>
        
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
        <!-- END TO BE LOOPED -->
    </div>
</div>
<?php $counter = 0; ?>
<div class="selection-content d-flex flex-wrap justify-content-center">
<?php $__currentLoopData = Storage::disk('products')->files('products'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filename): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <label class="selection-item" for="selection-check-<?php echo e($counter++); ?>" data-checked="false">
        <span class="selection-item-container">
            <div class="d-flex justify-content-center h-100 position-relative">
                <div class="mediaLib_imgHolder d-flex justify-content-center position-relative align-self-center w-100">
                    <div class="align-self-center">
                        <img src="<?php echo e(asset('storage/' . $filename)); ?>" class="img-fluid">
                    </div>
                </div>
            </div>
        </span>
    </label>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>