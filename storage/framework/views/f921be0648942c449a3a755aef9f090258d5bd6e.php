<div class="table_container table-responsive mdl-shadow--8dp mb-0 mt-3">
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th scope="col">
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                        <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                    </label>
                </th>
                <th scope="col">ORDER NUMBER</th>
                <th scope="col">CUSTOMER</th>
                <th scope="col">TOTAL</th>
                <th scope="col">DATE OF PURCHASE</th>
                <th scope="col">SATATUS</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                        <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                    </label>
                </th>
                <td><a href="#" class="order_number">#20180001</a></td>
                <td>JOHNARCOJADA</td>
                <td>₱ 2M</td>
            
                <td>SEPTEMBER 22, 2018</td>
                <td>PROCESSED</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="10">
                    <div class="pagination-navigation">
                        <div class="pagination-current"></div>
                        <div class="pagination-dots">
                        <!-- ACTIVE NOT WORKING -->
                            <button class="pagination-dot paginate_active">
                                <a href="#"><span class="pagination-number">1</span></a>
                            </button>
                            <button class="pagination-dot">
                                <a href="#"><span class="pagination-number">2</span></a>
                            </button>
                        </div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
    
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
            </filter>
        </defs>
    </svg>

</div>