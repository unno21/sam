<?php $__env->startSection('title'); ?>
    Products | SAM
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


    <!-- CUSTOM SNACKBAR -->

    <div class="cust_snackbar snackBar-label p-3 mdl-shadow--4dp">
        <div class="text-white mb-3 label-text">
            
        </div>
    </div>

    <div class="cust_snackbar snackBar-okCancel p-3 mdl-shadow--4dp">
        <div class="text-white mb-3" id="snack-question">
            Remove this product?
        </div>
        <div class="d-flex w-100 justify-content-end">
            <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect text-primary" id="btnOk">ok</button>
            <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect text-primary" id="btnCancel">cancel</button>
        </div>
    </div>
    <!-- END CUSTOM SNACKBAR -->

    <!-- *** FAB *** -->

    <div class="fab_holder">
    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab mdl-button--raised mdl-button--colored add_item cust_gradient " id="openModal">
    <i class="material-icons">add</i>
    </button>
    </div>


    <!-- *** INITIAL MODAL *** -->

    <div class="ui first small longer modal" id="cust_modal">
        <div class="header d-flex justify-content-between">
            <div class="header_title">ADD PRODUCT</div>
            <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
                <a href="#" class="close-button" id="hideModal">
                    <div class="in">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                    <div class="out">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="content">
            <div class="container">
                <form class="modal_form" id="addProduct" >
                <?php echo e(csrf_field()); ?>

                    <!-- FEATURED IMAGE -->
                    <input type="file" name="addImage" id="upload_img" class="d-none">
                    <!-- PRODUCT SKU -->
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="text" id="name" name="name">
                        <label class="mdl-textfield__label" for="name">Name</label>
                    </div>
                    <!-- PRODUCT NAME -->
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="number" id="price" name="price">
                        <label class="mdl-textfield__label" for="price">Price</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="number" id="stock" name="mainStock">
                        <label class="mdl-textfield__label" for="stock">Stock</label>
                    </div>
                    
                    <div class="filterByCateg mdl_select">
                         <div class="d-inline">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height w-100">
                                <input type="text" class="mdl-textfield__input" id="filterByCateg" name="category" readonly>
                                <input type="hidden" value="prodSize" name="" id="filterByCateg">
                                <i class="drpdwn-icon material-icons mdl-icon-toggle__label">keyboard_arrow_down</i>
                                <label for="filterByCateg" class="mdl-textfield__label">Categories</label>
                                <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mb-0" for="filterByCateg">
                                    <?php $__currentLoopData = $categ_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categ_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li class="mdl-menu__item" data-val="TS" id="category_<?php echo e($categ_product->id); ?>"><?php echo e($categ_product->name); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col">
                                <center>
                                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option_<?php echo e($category->id); ?>">
                                        <input type="radio" id="option_<?php echo e($category->id); ?>" class="mdl-radio__button" name="options" value="<?php echo e($category->id); ?>">
                                        <span class="mdl-radio__label"><?php echo e($category->name); ?></span>
                                    </label>
                                </center>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </div>
                    </div>

                    <!-- CHOOSE COLOR -->
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">

                        <dl class="dropdown"> 
  
                            <dt class="m-0 p-0 position-relative">
                                <a class="d-block">   
                                    <p class="multiSel m-0" id="color-values"></p>
                                    <span class="hidas">Color</span>  
                                    <span class="hida"></span>  
                                </a>
                            </dt>
                          
                            <dd class="m-0 p-0 position-relative">
                                <div class="mutliSelect">
                                    <ul class="mdl-shadow--4dp  p-3">
                                        <?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="<?php echo e($color->name); ?>">
                                                <input type="checkbox" id="<?php echo e($color->name); ?>" class="mdl-checkbox__input check-color" value="<?php echo e($color->name); ?>" data-id="<?php echo e($color->id); ?>">
                                                <span class="mdl-checkbox__label"><div class="d-flex align-items-center"><div class="color_icon <?php echo e($color->name); ?> mr-2" ></div><?php echo e($color->name); ?></div></span>
                                            </label>
                                        </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        <li class="position-relative text-center">
                                            <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect addColor" id="addColor" data-id="insert">Add Colors</button>
                                        </li>

                                    </ul>
                                </div>
                            </dd>

                        </dl>

                    </div>  

                    <!-- OUTPUT COLORS -->

                    <div class="table-responsive" >

                        <table class="table table-bordered mb-0" id="color_list_table">

                            <!-- TO BE LOOP -->

                                <!-- color_ids[] is the name of category id -->
                                <!-- color_images[] is the name of input type file for image per color -->

                            <!-- END TO BE LOOP -->
                        </table>
                    </div>

                     <!-- CHOOSE SIZE -->
                     
                    
                </form>
                <div class="actions text-center border-0 bg-white p-3">
                    <button class="mdl-button mdl-js-button mdl-js-ripple-effect myButton1 text-white px-5 py-2" id="btnSubmit">SUBMIT</button>
                </div>
            </div>
        </div>
    </div>

                       


    <!-- *** SECOND MODAL / FORM MEDIA LIBRARY *** -->


    <div class="ui second longer large modal draggable" id="media_modal">
        <div class="header d-flex justify-content-between handle">
            <div class="header_title">MEDIA LIBRARY</div>
            <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
                <a href="#" class="close-button" id="hide_media_modal">
                    <div class="in">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                    <div class="out">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="content">
            <nav class="cust_tabs mt-4 p-0 border">
                <div class="cust_selector" id="cust_selector"></div>
                <a href="#" class="active" id="mediaLibrary">MEDIA LIBRARY</a>
                <a href="#" class="" id="uploadFiles">UPLOAD IMAGES</a>
            </nav>
            <ul class="nav nav-tabs d-none">
                <li class="active"><a href="#tab1" data-toggle="tab">Shipping</a></li>
                <li><a href="#tab2" data-toggle="tab">Quantities</a></li>
            </ul>
            <div class="tab-content border mt-4">
                <div class="tab-pane active" id="tab1">
                    <p class="p-4 mb-0">MEDIA LIBRARY</p>

                    <div class="media_images_container">

                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
                            <defs>
                                <filter id="goo1">
                                    <feGaussianBlur in="SourceGraphic" stdDeviation="9" result="blur" />
                                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 20 -6" result="goo" />
                                    <feComposite in="SourceGraphic" in2="goo" operator="atop" result="goo"/>
                                </filter>
                            </defs>
                        </svg>

                        <div class="select-container position-relative pb-4">

                            <div class="d-flex justify-content-center">
                                <div class="selection-highlights position-absolute d-flex flex-wrap justify-content-center">
                                    <?php $counter = 0; ?>
                                    <?php $__currentLoopData = Storage::disk('products')->files('products'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filename): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <!-- TO BE LOOPED ALSO AS THE SELECTION ITEM LOOPED -->
                                        <input class="selection-checkbox" type="checkbox" id="selection-check-<?php echo e($counter++); ?>">
                                        <div class="selection-highlight"></div>
                                        <!-- END TO BE LOOPED -->
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                            <?php $counter = 0; ?>
                            <div class="selection-content d-flex flex-wrap justify-content-center">
                            <?php $__currentLoopData = Storage::disk('products')->files('products'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filename): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <label class="selection-item" for="selection-check-<?php echo e($counter++); ?>" data-checked="false">
                                    <span class="selection-item-container">
                                        <div class="d-flex justify-content-center h-100 position-relative">
                                            <div class="mediaLib_imgHolder d-flex justify-content-center position-relative align-self-center w-100">
                                                <div class="align-self-center">
                                                    <img src="<?php echo e(asset('storage/'. $filename )); ?>" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                    </span>
                                </label>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                            

                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <div class="tab_wrapper1 w-100 text-center">
                        <p class="h5">DROP FILES ANYWHERE TO UPLOAD</p>
                        <div class="upload_img_container mt-5">
                            <label for="upload_img"><span class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored mdl-button--raised text-white upload_img_btn">upload files</span></label>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="actions text-center border-0 bg-white p-3">
            <button class="btnSubmit">UPLOAD IMAGES</button>
        </div>
    </div>

    <!-- THIRD MODAL FOR UPDATE PRODUCTS -->

    <div class="ui longer small modal" id="updateModal">
        <div class="header d-flex justify-content-between">
            <div class="header_title">UPDATE PRODUCT</div>
            <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
                <a href="#" class="close-button" id="hideUpdateModal">
                    <div class="in">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                    <div class="out">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="content">
            <div class="container">
                <form class="modal_form" id="updateProduct">
                <?php echo e(csrf_field()); ?>

                    <!-- FEATURED IMAGE -->
                    <div class="ft_img_container w-100 mb-4">
                        
                        <center><label class="lead text-secondary">SET FEATURED IMAGE</label></center>
                        <div class="row justify-content-center">

                            <div class="col-sm-6">
                                <!-- <input type="file" name="" id="chooseFtImg" class="d-none"> -->
                                <label for="upload_imgUpdate" id="haha">
                                    <img class="img-thumbnail w-100 file-in" src="../assets/images/add_img1.png" id="open_media_modalUpdate" >
                                    <input type="file" name="image" id="upload_imgUpdate" class="d-none">
                                </label>
                            </div>

                        </div><!-- END ROW -->

                    </div>
                    <input type="hidden" name="id" value="" id="idUpdate">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="text" id="nameUpdate" name="name">
                        <label class="mdl-textfield__label" for="name">Name</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="number" id="priceUpdate" name="price">
                        <label class="mdl-textfield__label" for="price">Price</label>
                    </div>
                    <!-- <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="number" id="stockUpdate" name="stock">
                        <label class="mdl-textfield__label" for="stock">Stock</label>
                    </div> -->
                   

                    <!-- CHOOSE COLOR -->
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">

                        <dl class="dropdown"> 
  
                            <dt class="m-0 p-0 position-relative">
                                <a href="#" class="d-block">   
                                    <p class="multiSel m-0" id="color-values"></p>
                                    <span class="hidas">Color</span>  
                                    <span class="hida"></span>  
                                </a>
                            </dt>
                          
                            <dd class="m-0 p-0 position-relative">
                                <div class="mutliSelect">
                                    <ul class="mdl-shadow--4dp  p-3">
                                        <?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="color_<?php echo e($color->name); ?>">
                                                <input type="checkbox" id="color_<?php echo e($color->name); ?>" class="mdl-checkbox__input check-color" value="<?php echo e($color->name); ?>" data-id="<?php echo e($color->id); ?>">
                                                <span class="mdl-checkbox__label"><div class="d-flex align-items-center"><div class="color_icon <?php echo e($color->name); ?> mr-2" ></div><?php echo e($color->name); ?></div></span>
                                            </label>
                                        </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        <li class="text-center">
                                            <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect addColor" id="addColorUpdate" data-id="update">Add Colors</button>
                                        </li>

                                    </ul>
                                </div>
                            </dd>

                        </dl>

                    </div>  
                    <div class="table-responsive">
                        <table class="table table-bordered mb-0" id="color_list_table_update">
                            
                        </table>
                    </div>
                </form>
            </div>
        </div>
        <div class="actions text-center border-0 bg-white p-3">
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect myButton1 text-white px-5 py-2" id="btnUpdateProduct">SUBMIT</button>
        </div>
    </div>
    

    <!-- *** MAIN CONTAINER *** -->

    <div class="main-container w-100 py-5">

        <!-- THIS AREA IS THE MAIN WRAPPER -->
        <div class="main-wrapper">
            <div class="container">

                <!-- FILTER/ACTION AREA -->

                <div class="filterArea container">
                    <div class="container">

                        <div class="d-flex align-items-center">
                            <!-- FILTER BY CATEGORY -->
                                <div class="filterByCateg mdl_select">
                                    <div class="d-inline">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height w-100">
                                            <input type="text" class="mdl-textfield__input" id="category_filter" name="filterByCateg" readonly>
                                            <input type="hidden" value="prodSize" name="" id="filterByCateg">
                                            <i class="drpdwn-icon material-icons mdl-icon-toggle__label">keyboard_arrow_down</i>
                                            <label for="filterByCateg" class="mdl-textfield__label">Category</label>
                                            <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mb-0" for="filterByCateg">
                                                <li class="mdl-menu__item" data-val="ALL" id="ALL">All</li>
                                                <li class="mdl-menu__item" data-val="TS" id="TS">T-Shirt</li>
                                                <li class="mdl-menu__item" data-val="PS" id="PS">Polo Shirt</li>
                                                <li class="mdl-menu__item" data-val="J" id="J">Jacket</li>
                                                <li class="mdl-menu__item" data-val="LS" id="LS">Long Sleeve</li>
                                                <li class="mdl-menu__item" data-val="TF" id="TF">Three Fourth's</li>
                                                <li class="mdl-menu__item" data-val="B" id="B">Blouse</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div><!-- END FILTER BY CATEGORY -->

                                <div class="mdl-layout-spacer"></div>

                                
                                <div class="search-wrap d-flex position-relative align-items-center">
                                    <form class="w-100 d-flex justify-content-end"><input type="" name="" class="search_input"></form>
                                    <button id="custBtn-search" class="mdl-button mdl-js-button mdl-js-ripple-effect cust_gradient text-white">
                                        <i class="material-icons">search</i>
                                    </button>
                                </div>  
                                

                        </div><!-- END ROW FILTER/ACTIONS -->

                    </div>
                </div><!-- END FILTER/ACTIONS AREA -->

                <div id="content">
                    <?php echo $__env->make('back-end.products.includes.index-inner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript" src="<?php echo e(asset('assets/custom/js/admin.js')); ?>"></script>
    <script>
    $(".prodSNL").addClass("SNLactive")
    $(".prodSNL a").css("color","white")  
    
    $(document).ready(function(){
        $('#custBtn-search').on('click',function(){
            $('.search_input').toggleClass('expand')
            $('.search_input').focus()
        })
        $('.snackBar-okCancel').hide()
        $('.search_input').focusout(function(){
            $('.search_input').toggleClass('expand')
        })
        $('.page-item:first-child .page-link').empty()
        $('.page-item:first-child .page-link').append('<i class="material-icons">keyboard_arrow_left</i>')
        $('.page-item:last-child .page-link').empty()
        $('.page-item:last-child .page-link').append('<i class="material-icons">keyboard_arrow_right</i>')

        $('#cust_modal').modal('attach events', '#openModal', 'show')
        $('#cust_modal').modal('attach events', '#hideModal', 'hide')

        $('#updateModal').modal('attach events', '.btnUpdate', 'show')
        $('#updateModal').modal('attach events', '#hideUpdateModal', 'hide')


        btnRemove()

        $('#btnSubmit').on('click', function(){
            $('#addProduct').submit() 
        });
        $('#btnUpdateProduct').on('click', function(){
            $('#updateProduct').submit() 
        })
        $("#upload_img").change(function() {
            readURL(this)
        });
        $('#upload_imgUpdate').change(function(){
            readURL(this)
        })  
        $('#search_engine').on('submit',function(e){
            e.preventDefault();
            var keyword = $('#search_word').val()
            $.ajax({
                type        : "get",
                url         : "<?php echo e(URL('icp/products/search')); ?>/" + keyword,
                success     : function(data) {
                            $('#content').empty();
                            $('#content').append(data.content);
                            $('#btn-search-close').trigger('click');
                            rerun_modal()
                            btnRemove()
                },
                error       : function(data) {
                            console.log(data)
                },
            });
        })
        $('#category_filter').on('change', function(){
            $.ajax({
                type        : "get",
                url         : "<?php echo e(URL('icp/products/filter')); ?>/" + $(this).val(),
                success     : function(data) {
                            $('#content').empty();
                            $('#content').append(data.content);
                            rerun_modal()
                            btnRemove()
                },
                error       : function(data) {
                            console.log(data)
                },
            });
        })
        

        $('.btnUpdate').on('click', function(){
            var id = $(this).data('id')
            $.ajax({
                type        : "get",
                url         : "<?php echo URL('icp/products/edit'); ?>/" + id,
                success     : function(data) {
                            $('#nameUpdate').val(data.product.name)
                            $('#priceUpdate').val(data.product.price)
                            $('#idUpdate').val(data.product.id)
                            $('#open_media_modalUpdate').attr('src', "<?php echo e(asset('storage/products')); ?>/" + data.product.image)
                            $("#nameUpdate").parent().addClass("is-dirty");
                            $("#priceUpdate").parent().addClass("is-dirty");
                            $("#stockUpdate").parent().addClass("is-dirty");
                            $('#color_list_table_update').empty()
                            $('#color_list_table_update').append(data.content)
                },
                error       : function(data) {
                            console.log(data)
                },
            });
        })
        $('#updateProduct').submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                'url'			: "<?php echo URL('icp/products/update'); ?>",
                'method'		: 'post',
                'dataType'      : 'json',
                'data'			: formData,
                success 		: function(data){
                                console.log(data)
                                $('#content').empty();
                                $('#content').append(data.content)
                                $('#hideUpdateModal').trigger('click')
                                rerun_modal()
                                btnRemove()
                },
                error           : function(data){
                                console.log(data);
                },
                contentType		: false,
                cache			: false,
                processData		: false
            });
        })
        
        $('#addProduct').submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                'url'			: "<?php echo URL('icp/products/insert'); ?>",
                'method'		: 'post',
                'dataType'      : 'json',
                'data'			: formData,
                success 		: function(data){
                                $('#content').empty();
                                $('#content').append(data.content);
                                $('#hideModal').trigger('click')
                                rerun_modal()
                                btnRemove()
                                showSnackBar(data.word)
                },
                error           : function(data){
                                console.log(data);
                },
                contentType		: false,
                cache			: false,
                processData		: false
            });
        })

        //color events

        $('.addColor').on('click',function(){
            var label = $(this).data('id')
            $(".dropdown dd ul").hide()
            let str_color_images = ''

            $('.check-color').each(function(){
               if($(this).is(':checked')) {
                str_color_images += `<tr>
                                        <td><div class="color_icon ${$(this).val()}"></div></td>
                                        <td class="text-center">
                                            ${$(this).val()}
                                            <input type="hidden" name="color_ids[]" value="${$(this).data('id')}">
                                        </td>
                                        <td>
                                            <input type="number" name="stock[]" class="form-control">
                                        </td>
                                        <td>
                                            <label for="upload_img_color-${$(this).data('id')}" class="text-center w-100 mb-0" id="img_color">
                                                <img class="file-in border p-1" height="50px" src="../assets/images/add_img1.png" id="img_${$(this).data('id')}">
                                                <input type="file" name="color_images[]" id="upload_img_color-${$(this).data('id')}" class="d-none img-input" data-target="img_${$(this).data('id')}">
                                            </label>
                                        </td>
                                        <td>
                                            <input type="file" name="upload_files[]" class="" >
                                        </td>
                                    </tr>`
               }
            })
            
            if (label === 'insert') {
                $('#color_list_table').empty()
                $('#color_list_table').append(str_color_images)
            } else {
                $('#color_list_table_update').empty()
                $('#color_list_table_update').append(str_color_images)
            }
            

            $(".img-input").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    let target_img = $(this).data('target')
                    reader.onload = function (e) {

                        $('#' + target_img).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        })
    });
    let id 
    function btnRemove(){
        $('.btnDelete').on('click', function(){
			id = $(this).data('id')
			$('#snack-question').empty()
			$('#snack-question').append("Remove this product from database?")

			$(".snackBar-okCancel").show()
			$(".snackBar-okCancel").animate({
				bottom  : 15,
				opacity : 1
			})
        });
        $("#btnOk").on("click",function(){
            deleteProduct(id)
        })
        $("#btnCancel").on('click',function(){
            $(".snackBar-okCancel").animate({
                bottom  : 0,
                opacity : 0
            }).hide()
        })
    }
    
    function deleteProduct(id){
        $.ajax({
            type        : "get",
            url         : "<?php echo URL('icp/products/delete'); ?>/" + id,
            success     : function(data) {
                        $('#content').empty();
                        $('#content').append(data.content);
                        $('#snackbar').show()
                        $('#snackbar-text').html("Deleted Successfully")
                        btnRemove()            
                        rerun_modal()
                        showSnackBar(data.word)
                        $(".snackBar-okCancel").animate({
                            bottom  : 0,
                            opacity : 0
                        })
            },
            error       : function(data) {
                        console.log(data)
            },
        })
    }
    function rerun_modal(){
        $('#cust_modal').modal('attach events', '#openModal', 'show')
        $('#media_modal').modal('attach events', '.open_media_modal', 'show')
        $('#cust_modal').modal('attach events', '#hideModal', 'hide')
        $('#media_modal').modal('attach events', '#hide_media_modal', 'hide')

        $('#updateModal').modal('attach events', '.btnUpdate', 'show')
        $('#updateModal').modal('attach events', '#hideUpdateModal', 'hide')
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#open_media_modal').attr('src', e.target.result);
                $('#open_media_modalUpdate').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function clearInputs() {
        $('#name').val()
        $('#price').val()
        $('#stock').val()
        $('#nameUpdate').val()
        $('#priceUpdate').val()
        $('#stockUpdate').val()
    }
    
    function showSnackBar(word) {
        $('.label-text').html(word)
        $(".snackBar-label").show()
        $(".snackBar-label").animate({
            bottom  : 15,
            opacity : 1,
        })
        setTimeout(function(){
            $(".snackBar-label").animate({
                bottom  : 0,
                opacity : 0
            })
            setTimeout(function(){
                $(".snackBar-label").hide()
            },2000)
        }, 2000)
    }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('back-end.includes.icp', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>