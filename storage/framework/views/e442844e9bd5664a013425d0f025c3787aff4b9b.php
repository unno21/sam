<?php $__env->startSection('title'); ?>
    Media Library | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="fab_holder">
        <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab mdl-button--raised mdl-button--colored add_item cust_gradient " id="openModal">
            <i class="material-icons">add</i>
        </button>
    </div>


            <!-- *** INITIAL MODAL *** -->

    <div class="ui first longer modal" id="cust_modal">
        <div class="header d-flex justify-content-between">
            <div class="header_title">ADD AN IMAGE</div>
            <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
                <a href="#" class="close-button" id="hideModal">
                    <div class="in">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                    <div class="out">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="content">
            <form class="modal_form--mediaLib border" id="form-upload" enctype="multipart/form-data">

                <div class="h-100">

                    <div class="d-flex justify-content-center meidaLib_uploadWrapper" id="upload-wrapper">
                        <label class="mb-0 align-self-center" for="images" >
                            <span class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored cust_gradient align-self-center">upload image</span>
                        </label>
                            <?php echo e(csrf_field()); ?>

                            <input type="file" name="images[]" multiple id="images" class="d-none">

                    </div>

                    <div class="h-100 w-100">

                        <div class="mediaLib_imagesContainer position-relative">

                            <div class="p-3">
                                <div class="row" id="images_tobeupload">

                                    <!-- <div class="col-sm-3 col-lg-2 mb-3" id="mediaLib_wrapper">
                                        <div class="d-flex justify-content-center h-100 mediaLib_inner">
                                            <div class="mediaLib_imgHolder d-flex justify-content-center position-relative align-self-center w-100">
                                                <i class="material-icons removeAddedImage">close</i>
                                                <div class="align-self-center">
                                                    <img src="../assets/images/logo.png" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                </div>
                            </div>

                        </div>
                        
                    </div>

                    
                            
                </div>

            </form>

            <!-- <form action="/file-upload" class="dropzone" id="my-awesome-dropzone"></form> -->

        </div>
        <div class="actions text-center border-0 bg-white p-3">
            <button class="btnSubmit" id="submit-upload">SUBMIT</button>
        </div>
    </div>



            <!-- *** MAIN CONTAINER *** -->

    <div class="main-container w-100 py-5">

        <!-- THIS AREA IS FOR SEARCH -->
        <svg class="cust_hidden">
            <defs>
                <symbol id="icon-arrow" viewBox="0 0 24 24">
                    <title>arrow</title>
                    <polygon points="6.3,12.8 20.9,12.8 20.9,11.2 6.3,11.2 10.2,7.2 9,6 3.1,12 9,18 10.2,16.8 "/>
                </symbol>
                <symbol id="icon-drop" viewBox="0 0 24 24">
                    <title>drop</title>
                    <path d="M12,21c-3.6,0-6.6-3-6.6-6.6C5.4,11,10.8,4,11.4,3.2C11.6,3.1,11.8,3,12,3s0.4,0.1,0.6,0.3c0.6,0.8,6.1,7.8,6.1,11.2C18.6,18.1,15.6,21,12,21zM12,4.8c-1.8,2.4-5.2,7.4-5.2,9.6c0,2.9,2.3,5.2,5.2,5.2s5.2-2.3,5.2-5.2C17.2,12.2,13.8,7.3,12,4.8z"/><path d="M12,18.2c-0.4,0-0.7-0.3-0.7-0.7s0.3-0.7,0.7-0.7c1.3,0,2.4-1.1,2.4-2.4c0-0.4,0.3-0.7,0.7-0.7c0.4,0,0.7,0.3,0.7,0.7C15.8,16.5,14.1,18.2,12,18.2z"/>
                </symbol>
                <symbol id="icon-search" viewBox="0 0 24 24">
                    <title>search</title>
                    <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
                </symbol>
                <symbol id="icon-cross" viewBox="0 0 24 24">
                    <title>cross</title>
                    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
                </symbol>
            </defs>
        </svg>

        <div class="mysearch d-flex justify-content-center flex-column text-center align-items-center">
            <button id="btn-search-close" class="mybtn btn--search-close" aria-label="Close search form"><svg class="myicon icon--cross"><use xlink:href="#icon-cross"></use></svg></button>

            <form class="search__form my-5" action="" method="">
                <input class="search__input" name="search" type="search" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
                <span class="search__info">Hit enter to search or ESC to close</span>
            </form>

            <!-- <div class="search__related d-flex w-75 text-left text-white">
                <div class="search__suggestion w-50 pt-2">
                    <h3 class="mb-0">May We Suggest?</h3>
                    <p>#drone #funny #catgif #broken #lost #hilarious #good #red #blue #nono #why #yes #yesyes #aliens #green</p>
                </div>
                <div class="search__suggestion w-50 pb-2">
                    <h3 class="mb-0">Needle, Where Art Thou?</h3>
                    <p>#broken #lost #good #red #funny #lala #hilarious #catgif #blue #nono #why #yes #yesyes #aliens #green #drone</p>
                </div>
            </div> -->

        </div>

        <!-- THIS AREA IS THE MAIN WRAPPER -->
        <div class="main-wrapper">
            <div class="container">

                <!-- FILTER/ACTION AREA -->

                <div class="filterArea container">
                    <div class="container">

                        <div class="row">
                            <!-- FILTER BY CATEGORY -->
                            <div class="col-sm-6 col-md-6 col-lg-3">
                                <div class="filterByCateg mdl_select">
                                    <div class="d-inline">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height w-100">
                                            <input type="text" class="mdl-textfield__input" id="filterByCateg" name="filterByCateg" readonly>
                                            <input type="hidden" value="prodSize" name="" id="filterByCateg">
                                            <i class="drpdwn-icon material-icons mdl-icon-toggle__label">keyboard_arrow_down</i>
                                            <label for="filterByCateg" class="mdl-textfield__label">Filter By Date</label>
                                            <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mb-0" for="filterByCateg">
                                                <li class="mdl-menu__item" data-val="CL" id="CL">Small</li>
                                                <li class="mdl-menu__item" data-val="SH" id="SH">Medium</li>
                                                <li class="mdl-menu__item" data-val="AC" id="AC">Large</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- END FILTER BY CATEGORY -->

                            <!-- FILTER/ACTION -->
                            <div class="col-md-12 col-sm-12 col-lg-9">

                                <div class="d-flex justify-content-between h-100">
                                    <div class="filterAction h-100">
                                        <div class="d-flex align-content-center h-100">
                                            <div class="align-self-center">
                                                <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored cust_gradient">Trash</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="search4prod_container h-100">
                                        <div class="d-flex align-content-center h-100">
                                            <div class="search-wrap align-self-center ml-auto">
                                                <button id="custBtn-search" class="mybtn btn--search">
                                                    <svg class="myicon icon--search"><use xlink:href="#icon-search"></use></svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- END FILTER/ACTION -->

                        </div><!-- END ROW FILTER/ACTIONS -->

                    </div>
                </div><!-- END FILTER/ACTIONS AREA -->


                <!-- MAIN AREA -->

                <div class="container bg-white mdl-shadow--8dp mt-4 py-4">
                    <div class="media_images_container">

                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
                            <defs>
                                <filter id="goo1">
                                    <feGaussianBlur in="SourceGraphic" stdDeviation="9" result="blur" />
                                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 20 -6" result="goo" />
                                    <feComposite in="SourceGraphic" in2="goo" operator="atop" result="goo"/>
                                </filter>
                            </defs>
                        </svg>

                        <div class="select-container position-relative" id="images-container">
                            <?php echo $__env->make('back-end.medialibrary.includes.index-inner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>

                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
                        <defs>
                            <filter id="goo">
                                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                                <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
                            </filter>
                        </defs>
                    </svg>
                    <div class="pagination-navigation1">
                        <div class="pagination-current"></div>
                        <div class="pagination-dots">
                            <button class="pagination-dot paginate_active">
                                <span class="pagination-number">1</span>
                            </button>
                            <button class="pagination-dot">
                                <span class="pagination-number">2</span>
                            </button>
                            <button class="pagination-dot">
                                <span class="pagination-number">3</span>
                            </button>
                            <button class="pagination-dot">
                                <span class="pagination-number">4</span>
                            </button>
                            <button class="pagination-dot">
                                <span class="pagination-number">5</span>
                            </button>
                        </div>
                    </div>
                </div><!-- MAIN AREA -->

            </div>
        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <!-- SearchUIEffects --><script type="text/javascript" src="../assets/SearchUIEffects/js/demo5.js"></script>
    <script type="text/javascript" src="../assets/custom/js/admin.js"></script>

        <!-- checkbox-with-mo-js --><script src='../assets/checkbox-with-mo-js/js/mo.min.js'></script>
        <!-- checkbox-with-mo-js --><script src="../assets/checkbox-with-mo-js/js/index.js"></script>
    <script>
        $(".libSNL").addClass("SNLactive")
        $(".libSNL a").css("color","white")
    </script>

    <!-- dropzone js --><script type="text/javascript" src="../dropzone.js"></script>

    <!-- custom js -->
    <script>
        // -- on upload, hide "upload-wrapper" -- //
        $('#images').change(function(){
            $('#upload-wrapper').removeClass('d-flex')
            $('#upload-wrapper').addClass('d-none')
        })
        
        // -- submit form -- //
        $('#submit-upload').on('click', function(){
            $('#form-upload').submit()
        })
        $('#form-upload').on('submit', function(e){
            var formData = new FormData($(this)[0]);
            formData.append('removed_indexes', tempRemovedIndexes);
            e.preventDefault()
            $.ajax({
                url             : "<?php echo e(URL('manager/medialibrary/insert')); ?>",
                method          : "POST",
                data            : formData,
                success 		: function(data) {
                                $('#images-container').empty()
                                $('#images-container').append(data.content)
                                console.log(data)
                },
                error           : function(data){
                                console.log(data);
                },
                contentType		: false,
                cache			: false,
                processData		: false

            })
        })

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('back-end.includes.manager', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>