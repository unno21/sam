<div class="table_container table-responsive mdl-shadow--8dp mb-0 mt-3">
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th scope="col">
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                        <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                    </label>
                </th>
                <th scope="col">
                    <i class="material-icons">image</i>
                </th>
                <th scope="col">NAME</th>
                <th scope="col">STOCK</th>
                <th scope="col">PRICE</th>
                <th scope="col">CATEGORIES</th>
                <!-- <th scope="col">TAGS</th> -->
                <!-- <th scope="col">FEATURED</th> -->
                <th scope="col">DATE</th>
                <th scope="col">ACTION</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                        <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                    </label>
                </th>
                <td>
                    <img src="<?php echo e(asset('assets/images/yellow.png')); ?>" width="50px">
                </td>
                <td>yellow blouse</td>
                <td class="text-danger">out of stock</td>
                <td>₱ 2323</td>
            
                <td class="text-capitalize">t-shirt</td>
                <!-- <td>-</td> -->
                <!-- <td>
                    <div class="checkbox">
                        <input id="check1" name="check" type="checkbox" class="mo_chck" />
                        <label for="check1" id="lbl_check"></label>
                    </div>
                </td> -->
                <td>Published at<br>2018</td>
                <td>
                    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon overflow-visible " data-inverted="" data-position="top center" data-tooltip="Edit" id="openUpdateModal">
                        <i class="material-icons-new outline-edit icon-action"></i>
                    </button>
                    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon overflow-visible btnDelete" data-inverted="" data-position="top center" data-tooltip="Delete" data-id="1" >
                        <i class="material-icons-new outline-delete icon-action"></i>
                    </button>
                </td>
            </tr>
            <tr><td colspan="8">NO ITEM</td></tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="10">
                    <div class="pagination-navigation">
                        <div class="pagination-current"></div>
                        <div class="pagination-dots">
                        <!-- ACTIVE NOT WORKING -->
                            <button class="pagination-dot paginate_active">
                                <a href="#"><span class="pagination-number">1</span></a>
                            </button>
                        </div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
    
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
            </filter>
        </defs>
    </svg>

</div>