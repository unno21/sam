<?php $__env->startSection('title'); ?>
    Orders | SAM
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


    <!-- *** UI MODAL *** -->

    <div class="ui modal" id="order_detail_modal">
        <div class="header d-flex justify-content-between">
            <div class="header_title">ORDER DETAIL</div>
            <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
                <a href="#" class="close-button" id="hide_order_detail_modal">
                    <div class="in">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                    <div class="out">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm">
                        <h4 id="name" class="mb-0">JOHNARCOJADA</h4>
                        <p id="date" class="text_grayish"><sup>SEPTEMBER 22, 2018</sup></p>
                    </div>
                    <div class="position-relative">
                        <button id="change_order_status" class="mdl-button mdl-js-button mdl-js-ripple-effect change_order_status position-relative">
                            <div class="d-flex align-items-center">
                                <span class="text_grayish mr-3">Status:</span><span class="status_text text-uppercase status">PROCESSED</span>
                            </div>
                        </button>

                        <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect change_order_status_items" for="change_order_status">
                            <li class="mdl-menu__item" data-value="processed">PROCESSED</li>
                            <li class="mdl-menu__item" data-value="shipped">SHIPPED</li>
                            <li class="mdl-menu__item" data-value="delivered">DELIVERED</li>
                            <li class="mdl-menu__item" data-value="closed">CLOSED</li>
                        </ul>
                    </div>
                </div>

                <div id="second-content">

                </div>
                
                
                <h4 class="mb-0 px-3 text-right">TOTAL: ₱2M</h4>

            </div>
        </div>
        <div class="actions text-center border-0 bg-white p-3">
            <button class="btnSubmit" id="btnSubmit">SUBMIT</button>
        </div>
    </div>

    <!-- *** MAIN CONTAINER *** -->

    <div class="main-container w-100 py-5">

        <!-- THIS AREA IS FOR SEARCH -->
        <svg class="cust_hidden" >
            <defs>
                <symbol id="icon-arrow" viewBox="0 0 24 24">
                    <title>arrow</title>
                    <polygon points="6.3,12.8 20.9,12.8 20.9,11.2 6.3,11.2 10.2,7.2 9,6 3.1,12 9,18 10.2,16.8 "/>
                </symbol>
                <symbol id="icon-drop" viewBox="0 0 24 24">
                    <title>drop</title>
                    <path d="M12,21c-3.6,0-6.6-3-6.6-6.6C5.4,11,10.8,4,11.4,3.2C11.6,3.1,11.8,3,12,3s0.4,0.1,0.6,0.3c0.6,0.8,6.1,7.8,6.1,11.2C18.6,18.1,15.6,21,12,21zM12,4.8c-1.8,2.4-5.2,7.4-5.2,9.6c0,2.9,2.3,5.2,5.2,5.2s5.2-2.3,5.2-5.2C17.2,12.2,13.8,7.3,12,4.8z"/><path d="M12,18.2c-0.4,0-0.7-0.3-0.7-0.7s0.3-0.7,0.7-0.7c1.3,0,2.4-1.1,2.4-2.4c0-0.4,0.3-0.7,0.7-0.7c0.4,0,0.7,0.3,0.7,0.7C15.8,16.5,14.1,18.2,12,18.2z"/>
                </symbol>
                <symbol id="icon-search" viewBox="0 0 24 24">
                    <title>search</title>
                    <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
                </symbol>
                <symbol id="icon-cross" viewBox="0 0 24 24">
                    <title>cross</title>
                    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
                </symbol>
            </defs>
        </svg>

        <div class="mysearch d-flex justify-content-center flex-column text-center align-items-center">
        <button id="btn-search-close" class="mybtn btn--search-close" aria-label="Close search form"><svg class="myicon icon--cross"><use xlink:href="#icon-cross"></use></svg></button>

        <form class="search__form my-5" action="" method="" id="search_engine">
            <input class="search__input" name="search" type="search" id="search_word" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
            <span class="search__info">Hit enter to search or ESC to close</span>
        </form>

        </div>

        <!-- THIS AREA IS THE MAIN WRAPPER -->
        <div class="main-wrapper">
            <div class="container">

                <!-- FILTER/ACTION AREA -->

                <div class="filterArea container">
                    <div class="container">

                        <div class="row">
                            <!-- FILTER BY CATEGORY -->
                            <div class="col-sm-6 col-md-6 col-lg-3">
                                <div class="filterByCateg mdl_select">
                                    <div class="d-inline">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height w-100">
                                            <input type="text" class="mdl-textfield__input" id="category_filter" name="filterByCateg" readonly>
                                            <input type="hidden" value="prodSize" name="" id="filterByCateg">
                                            <i class="drpdwn-icon material-icons mdl-icon-toggle__label">keyboard_arrow_down</i>
                                            <label for="filterByCateg" class="mdl-textfield__label">Category</label>
                                            <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mb-0" for="filterByCateg">
                                                <li class="mdl-menu__item" data-val="ALL" id="ALL">All</li>
                                                <li class="mdl-menu__item" data-val="TS" id="TS">T-Shirt</li>
                                                <li class="mdl-menu__item" data-val="PS" id="PS">Polo Shirt</li>
                                                <li class="mdl-menu__item" data-val="J" id="J">Jacket</li>
                                                <li class="mdl-menu__item" data-val="LS" id="LS">Long Sleeve</li>
                                                <li class="mdl-menu__item" data-val="TF" id="TF">Three Fourth's</li>
                                                <li class="mdl-menu__item" data-val="B" id="B">Blouse</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- END FILTER BY CATEGORY -->

                            <!-- FILTER/ACTION -->
                            <div class="col-md-12 col-sm-12 col-lg-9">

                                <div class="d-flex justify-content-between h-100">
                                    <div class="filterAction h-100">
                                        <div class="d-flex align-content-center h-100">
                                            <div class="align-self-center">
                                                <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored cust_gradient mr-3">Filter</button>
                                                <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored cust_gradient">Trash</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="search4prod_container h-100">
                                        <div class="d-flex align-content-center h-100">
                                            <div class="search-wrap align-self-center ml-auto">
                                                <button id="custBtn-search" class="mybtn btn--search">
                                                    <svg class="myicon icon--search"><use xlink:href="#icon-search"></use></svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- END FILTER/ACTION -->

                        </div><!-- END ROW FILTER/ACTIONS -->

                    </div>
                </div><!-- END FILTER/ACTIONS AREA -->

                <div id="content">
                    <?php echo $__env->make('back-end.orders.includes.index-inner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <!-- SearchUIEffects --><script type="text/javascript" src="<?php echo e(asset('assets/SearchUIEffects/js/demo5.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/custom/js/admin.js')); ?>"></script>

    <script>
    $(".ordersSNL").addClass("SNLactive")
    $(".ordersSNL a").css("color","white")  

    $(document).ready(function(){
        $('.order_number').on('click', function(){
            var id = $(this).data('id')
            $.ajax({
                url     : "<?php echo e(url('/admin/orders/detail')); ?>/" + id ,
                type    : "get",
                success : function(data){
                    if (data.order.status == 0 ){
                        $('.status').html('processed')
                    } else if (data.order.status == 1){
                        $('.status').html('shipped')
                    } else {
                        $('.status').html('delivered')
                    }
                    $('#second-content').empty()
                    $('#second-content').append(data.content)
                    $('.ui.modal').modal("refresh")
                    console.log(data)
                },
                error   : function(data){
                    console.log(data)
                }
            })
        })
    });
    </script>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('back-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>