

<?php $__env->startSection('title'); ?>
    Order | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

   

<div class="main-container">

    <div class="banner">

        <div class="d-flex justify-content-center align-content-center h-100">
            
            <div class="logo_container h-100 d-flex flex-column">
                <img src="<?php echo e(asset('assets/images/logo.png')); ?>" class="img-fluid h-50 d-flex align-self-center mt-5 pt-5"><br>
                <p class="h5 text-uppercase Lspacing2 text-white text-center m-0 align-self-center">shopping assistant mirror</p>
            </div>

        </div>

    </div>

    <div class="container main-wrapper">
        
        <div class="main_area radius5 overflow-hidden mdl-shadow--16dp mb-5">

            <div class="container">
                <p class="h5 text-uppercase Lspacing2 py-5 ml-5">recent orders</p>

                <div class="container">
                    <div class="table-responsive">
                        <table class="table cart_table text-center">
                                <tr>
                                    <th>ORDER NO.</th>
                                    <th>TOTAL</th>
                                    <th>DATE OF PURCHASE</th>
                                </tr>
                            <tbody>
                                <?php $__empty_1 = true; $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <!-- TO BE LOOPED -->
                                <tr>
                                    <td class="text-uppercase">
                                        <a href="<?php echo e(url('order/detail/' . Crypt::encrypt($order->id))); ?>">#<?php echo e($order->order_number); ?></a>
                                    </td>
                                    <td class="text-uppercase">₱<?php echo e(Crypt::decrypt($order->amount)); ?>.00</td>
                                    <td class="text-uppercase"><?php echo e(date('F d, Y',strtotime($order->created_at))); ?></td>
                                </tr><!-- END BE LOOPED -->
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>

                                <?php endif; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- END TABLE CONTAINER -->
                    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 px-5 py-2 mb-5">continue shopping</button>

                </div>
            </div>

        </div>

    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript">
		$('.mdl-navigation2').find('a:nth-child(3)').addClass('activeLink')
	</script>
	<!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/script.js')); ?>"></script>
	<?php echo $__env->make('includes.links-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>