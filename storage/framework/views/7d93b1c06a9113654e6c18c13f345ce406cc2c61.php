<?php $__env->startSection('title'); ?>
    Dashboard | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="main-container w-100 py-5">

    <div class="main-wrapper">
    <div class="container">
        
        <!-- FOUR PANELS -->
        
        <div class="row">

            <div class="col-sm-6 col-md-6 col-lg-3 panel_wrapper mb-4">
                <div class="panel">
                    <div class="row">
                        <div class="col-6">
                            
                        </div>
                        <div class="col-6 py-3">
                            <p class="h2 text-white mb-0">12,508</p>
                            <p class="text-uppercase text-white"><small>total products</small></p>
                        </div> 
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-6 col-lg-3 panel_wrapper mb-4">
                <div class="panel">
                    <div class="row">
                        <div class="col-6">
                            
                        </div>
                        <div class="col-6 py-3">
                            <p class="h2 text-white mb-0">12,508</p>
                            <p class="text-uppercase text-white"><small>total products</small></p>
                        </div> 
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-6 col-lg-3 panel_wrapper mb-4">
                <div class="panel">
                    <div class="row">
                        <div class="col-6">
                            
                        </div>
                        <div class="col-6 py-3">
                            <p class="h2 text-white mb-0">12,508</p>
                            <p class="text-uppercase text-white"><small>total products</small></p>
                        </div> 
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-6 col-lg-3 panel_wrapper mb-4">
                <div class="panel">
                    <div class="row">
                        <div class="col-6">
                            
                        </div>
                        <div class="col-6 py-3">
                            <p class="h2 text-white mb-0">12,508</p>
                            <p class="text-uppercase text-white"><small>total products</small></p>
                        </div> 
                    </div>
                </div>
            </div>

        </div><!-- END FOUR PANELS -->

        <!-- GRAPH -->
        <div class="row">

            <div class="col-sm-12">
                <div class="chart-container bg-white mdl-shadow--8dp mb-4 radius-5">
                    <p class="h1 text-uppercase letter-spacing-2 text-center py-3">monthly sales</p>
                    <div class="ct-chart ct-perfect-fourth"></div>
                </div>
            </div>

        </div><!-- END GRAPH -->

        <!-- TWO PANELS -->
        <div class="row">
        
            <!-- BEST SELLER  -->
            <div class="col-md-6 panel2_wrapper mb-4 d-flex">
                <div class="panel2 d-flex">
                    <div class="row">
                        <div class="col-sm-6 p-4">
                            <div class="d-flex flex-column h-100">
                                <div class="mb-auto">
                                    <p class="h2 text-uppercase letter-spacing text-opacity-6 text-white panel2ItemName"><?php echo e($best_seller->product->name); ?></p>
                                </div>
                                <div class="align-self-end mt-auto mr-auto">
                                    <p class="h4 text-uppercase letter-spacing text-opacity-6 text-white panel2Caption1">best seller</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="image_holder position-relative w-100 h-100 d-flex align-content-center">
                                <img src="<?php echo e(asset('storage/products/'. $best_seller->product->image)); ?>" class="w-100 align-self-center">
                                <p class="h4 text-uppercase letter-spacing text-opacity-6 text-white panel2Caption2 d-none text-center">best seller</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- BEST SELLER  -->
            <div class="col-md-6 panel2_wrapper mb-4 d-flex">
                <div class="panel2 d-flex">
                    <div class="row">
                        <div class="col-sm-6 p-4">
                            <div class="d-flex flex-column h-100">
                                <div class="mb-auto">
                                    <p class="h2 text-uppercase letter-spacing text-opacity-6 text-white panel2ItemName"><?php echo e($critical_product->name); ?></p>
                                </div>
                                <div class="align-self-end mt-auto mr-auto">
                                    <p class="h4 text-uppercase letter-spacing text-opacity-6 text-white panel2Caption1">critical level</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="image_holder position-relative w-100 h-100 d-flex align-content-center">
                                <img src="<?php echo e(asset('storage/products/'. $critical_product->image)); ?>" class="w-100 align-self-center">
                                <p class="h4 text-uppercase letter-spacing text-opacity-6 text-white panel2Caption2 d-none text-center">critical level</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- END TWO PANELS -->

        <!-- UNDER TWO PANELS -->
        <div class="row">
            
            <div class="col-lg-8">
                <div class="bg-white mdl-shadow--8dp panelLatestOrder mb-4 radius-5">

                    <div class="px-4 pt-4">
                        <p class="lead text-uppercase letter-spacing-2">latest orders</p>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover text-center">
                                <thead class="text-uppercase mb-0">
                                    <tr>
                                        <th>order id</th>
                                        <th>customer</th>
                                        <th>no. of items</th>
                                        <th>total</th>
                                        <th>date of purchase</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $latest_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $latest_order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($latest_order->order_number); ?></td>
                                        <td class="text-capitalize">jear</td>
                                        <td><?php echo e(count($latest_order->order_details)); ?></td>
                                        <td>₱<?php echo e(Crypt::decrypt($latest_order->amount)); ?>.00</td>
                                        <td class="text-capitalize"><?php echo e(date('F m, Y', strtotime($latest_order->created_at))); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex">
                        <a href="<?php echo e(URL('/products')); ?>" class="text-center text-uppercase letter-spacing-2 py-2 w-100 mybg_primary text-white manageBTN mdl-js-button mdl-js-ripple-effect bottom_radius-5 position-relative">manage products</a>
                    </div>

                </div>
            </div>

            <div class="col-lg-4">
                <div class="bg-white mdl-shadow--8dp panelRecetlyAddedProducts radius-5">

                    <div class="px-4 pt-4 pb-3">
                        <p class="lead text-uppercase">recently added products</p>
                        <div class="container-fluid">
                            <div class="row">
                            <?php $__currentLoopData = $recently_addeds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recently_added): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <!-- ITEM TO BE LOOPED -->
                                <div class="col-sm-12 mdl-shadow--4dp mb-3 py-2 radius-3">
                                    <div class="row">
                                        <div class="col-sm-2 px-2">
                                            <div class="img_holderRAP">
                                                <img src="<?php echo e(asset('storage/products/'. $recently_added->image)); ?>" class="img-fluid">
                                            </div>
                                        </div>
                                        <div class="col-sm-10 px-2">
                                            <div class="position-relative">
                                                <div class="itemNameRAP d-flex justify-content-between">
                                                    <a href="#" class="text-uppercase align-self-center"><?php echo e($recently_added->name); ?></a>
                                                    <div>
                                                        <div class="mybg_primary p-1 radius-3 text-white"><small>₱<?php echo e($recently_added->price); ?></small></div>
                                                    </div>
                                                </div>
                                                <div class="itemParaRAP">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- ITEM TO BE LOOPED -->
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div> 

                    <div class="d-flex">
                        <a href="<?php echo e(url('admin/products')); ?>" class="text-center text-uppercase letter-spacing-2 py-2 w-100 mybg_primary text-white manageBTN mdl-js-button mdl-js-ripple-effect bottom_radius-5 position-relative">manage products</a>
                    </div>

                </div>
            </div>

        </div><!-- END UNDER TWO PANELS -->

    </div>
    </div>

    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript" src="../assets/custom/js/admin.js"></script>
    <script type="text/javascript" src="<?php echo e(asset("assets/custom/js/dashboard_chart.js")); ?>"></script>
    <script>
        $(".dashSNL").addClass("SNLactive")
        $(".dashSNL a").css("color","white")   
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('back-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>