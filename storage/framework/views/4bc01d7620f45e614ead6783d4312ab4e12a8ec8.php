<?php $__currentLoopData = $wishlists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $wishlist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
        <td>
            <div class="td_wrapper">
                <div class="item_img_container">
                    <a href="<?php echo e(url('product/'. Crypt::encrypt($wishlist->product->id))); ?>">
                        <img src="<?php echo e(asset('storage/products/'. $wishlist->product->image)); ?>" height="80px" class="border">
                    </a>
                </div>
            </div>
        </td>
        <td><div class="td_wrapper"><?php echo e($wishlist->product->name); ?></div></td>
        <td><div class="td_wrapper"><?php echo e($wishlist->product->price); ?></div></td>
        <td>
            <div class="td_wrapper">
                <a href="<?php echo e(url('wishlist/add/' . $wishlist->id)); ?>">
                    <button data-id="<?php echo e($wishlist->id); ?>" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon mr-2 overflow-visible btn_icon-action btnCart" data-inverted="" data-tooltip="Add to Cart" data-position="top center">
                        <i class="material-icons-new outline-add_shopping_cart icon-action"></i>
                    </button>
                </a>
                <button data-id="<?php echo e($wishlist->id); ?>" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon overflow-visible btn_icon-action remove_wishlist_item btnRemove" data-inverted="" data-tooltip="Remove to Wishlist" data-position="top center">
                    <i class="material-icons-new outline-delete icon-action"></i>
                </button>
            </div>
        </td>
    </tr>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>