

<?php $__env->startSection('title'); ?>
    Cart | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>


<!-- CUSTOM SNACKBAR -->

<div class="cust_snackbar snackBar-plain p-3 mdl-shadow--4dp">
    <div class="text-white mb-3">
        <?php echo e($snackbar); ?>

    </div>
</div>

<div class="cust_snackbar snackBar-okCancel p-3 mdl-shadow--4dp">
    <div class="text-white mb-3" id="snack-question">
        Remove this product?
    </div>
    <div class="d-flex w-100 justify-content-end">
        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect text-primary" id="ok">ok</button>
        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect text-primary" id="cancel">cancel</button>
    </div>
</div>


<!-- END CUSTOM SNACKBAR --> 


<div class="main-container">

	<div class="banner">

		<div class="d-flex justify-content-center align-content-center h-100">
			
			<div class="logo_container h-100 d-flex flex-column">
				<img src="<?php echo e(asset('assets/images/logo.png')); ?>" class="img-fluid h-50 d-flex align-self-center mt-5 pt-5"><br>
				<p class="h5 text-uppercase Lspacing2 text-white text-center m-0 align-self-center">shopping assistant mirror</p>
			</div>

		</div>

	</div>

	<div class="container main-wrapper">
		
		<div class="main_area radius5 overflow-hidden mdl-shadow--16dp mb-5">

			<div class="container">
				<p class="h5 text-uppercase Lspacing2 py-5 ml-5">cart</p>

				<div class="container">
					<div class="table-responsive">
						<table class="table cart_table text-center">
							<thead>
								<tr>
									<th></th>
									<th>PRODUCT</th>
									<th>SIZE</th>
									<th>PRICE</th>
									<th>QUANTITY</th>
									<th>TOTAL</th>
									<th></th>
								</tr>
							</thead>
							<tbody id="content">
								<?php echo $__env->make('front-end.cart.includes.inner-index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="6"></td>
								</tr>
							</tfoot>
						</table>
					</div><!-- END TABLE CONTAINER -->

					<div class="d-flex justify-content-end">
						<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 px-5 py-2 mb-5" id="btnUpdate">update cart</button>
					</div>

					<div class="mb-5">
						<p class="h5 text-uppercase">cart total: <span class="text-primary">₱<?php echo e($cart->total()); ?>.00</span></p>
					</div>
					</form>
					

					<form action="<?php echo e(url('cart/payment')); ?>" method="post">
						<?php echo e(csrf_field()); ?>

						<input type="hidden" name="cart_id" id="" value="<?php echo e($cart !== null ? $cart->id : ''); ?>">
						<input type="hidden" name="grandtotal" value="<?php echo e($cart->total()); ?>">
						<button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 px-5 py-2 mb-5">link to payment</button>
					</form>
					
				</div>
			</div>

		</div>

	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
	<script type="text/javascript">
		$('.mdl-navigation2').find('a:nth-child(2)').addClass('activeLink')
	</script>
	<!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/script.js')); ?>"></script>
	<?php echo $__env->make('includes.links-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<script type="text/javascript">
		var id
		var productName
		$('.btnRemove').on('click', function(){
			// var qwe = confirm('Delete this product
			id = $(this).data('id')
			productName = $(this).data('product')
			$('#snack-question').empty()
			$('#snack-question').append("Remove " + productName + " from cart?")

			$(".snackBar-okCancel").animate({
		        bottom  : 15,
		        opacity : 1
		    })
		});
	    $("#ok").on("click",function(){
			$.ajax({
				url		: "cart/delete/" + id,
				type	: "get",
				success : function(data) {
						$('#snack-question').empty()
						$('#snack-question').append(productName + " removed.")

						$('#content').empty();
						$('#content').append(data.content);
						$(".snackBar-okCancel").animate({
					        bottom  : 0,
					        opacity : 0
					    })
					    setTimeout(function(){
					    	$(".snackBar-plain").animate({
						        bottom  : 15,
						        opacity : 1
						    })
					    },300)
					    setTimeout(function(){
					    	$(".snackBar-plain").animate({
						        bottom  : 0,
						        opacity : 0
						    })
					    },600)
				},
				error	: function(data) {
					console.log(data);
				}
			});
	    })
	    $("#cancel").on('click',function(){
	    	$(".snackBar-okCancel").animate({
		        bottom  : 0,
		        opacity : 0
		    })
		})
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>