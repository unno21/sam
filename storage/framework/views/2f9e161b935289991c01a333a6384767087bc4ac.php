<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

	<title><?php echo $__env->yieldContent('title'); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- *** JQUERY *** -->

	<!-- Bootstrap jquery --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/code-jquery-3.2.1.slim.min.js')); ?>"></script>
	<!-- jquery --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/jquery-3.3.1.min.js')); ?>"></script>
	<!-- dragabilly --><script type="text/javascript" src="<?php echo e(asset('assets/jquery/dragabilly.pkgd.js')); ?>"></script>
	<!-- moment.js --><script type="text/javascript" src="<?php echo e(asset('assets/Bootstrap/moment.js')); ?>"></script>

	<!-- amcharts --><script src="<?php echo e(asset('assets/amcharts/amcharts/amcharts.js')); ?>"></script>
	<!-- amcharts --><script src="<?php echo e(asset('assets/amcharts/amcharts/serial.js')); ?>"></script>
	<!-- amcharts --><script src="<?php echo e(asset('assets/amcharts/amcharts/plugins/export/export.min.js')); ?>"></script>
	<!-- amcharts --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/amcharts/amcharts/plugins/export/export.css')); ?>">
	<!-- amcharts --><script src="<?php echo e(asset('assets/amcharts/amcharts/themes/light.js')); ?>"></script>

	
	<!-- custom css --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/custom/css/admin.css')); ?>">
	<?php echo $__env->make('includes.links-styles', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<script>document.documentElement.className = 'js';</script>

</head>
<body>
	<div class="container">
		<ul id="gn-menu" class="gn-menu-main">
			<li class="gn-trigger">
				<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
				<nav class="gn-menu-wrapper">
					<div class="gn-scroller">
						<ul class="gn-menu">
							<li class="gn-search-item">
								<div class="d-flex py-4">
									<i class="material-icons ml-4">search</i>
									<input placeholder="SEARCH ..." type="search" class="gn-search text-uppercase">
								</div>
							</li>
							<li class="dashSNL">
								<a href="<?php echo e(URL('admin/dashboard/')); ?>" class="d-flex align-content-center position-relative mdl-js-button mdl-js-ripple-effect"><i class="material-icons mx-4 align-self-center">dashboard</i>DASHBOARD</a>
								<!-- <ul class="gn-submenu">
									<li><a class="gn-icon gn-icon-illustrator">Vector Illustrations</a></li>
									<li><a class="gn-icon gn-icon-photoshop">Photoshop files</a></li>
								</ul> -->
							</li>
							<li class="prodSNL"><a class="d-flex align-content-center position-relative mdl-js-button mdl-js-ripple-effect" href="<?php echo e(URL('admin/products/')); ?>"><i class="material-icons mx-4 align-self-center">layers</i>PRODUCTS</a></li>
							<li class="ordersSNL"><a class="d-flex align-content-center position-relative mdl-js-button mdl-js-ripple-effect" href="<?php echo e(URL('admin/orders/')); ?>"><i class="material-icons mx-4 align-self-center">view_list</i>ORDERS</a></li>
							<li class="repSNL"><a class="d-flex align-content-center position-relative mdl-js-button mdl-js-ripple-effect" href="<?php echo e(URL('admin/reports/')); ?>"><i class="material-icons mx-4 align-self-center">timeline</i>REPORTS</a></li>
							<li class="custSNL"><a class="d-flex align-content-center position-relative mdl-js-button mdl-js-ripple-effect" href="<?php echo e(URL('admin/customers/')); ?>"><i class="material-icons mx-4 align-self-center">person_outline</i>CUSTOMERS</a></li>
							<li class="empSNL"><a class="d-flex align-content-center position-relative mdl-js-button mdl-js-ripple-effect" href="<?php echo e(URL('admin/employees/')); ?>"><i class="material-icons mx-4 align-self-center">account_circle</i>EMPLOYEES</a></li>
							<li class="libSNL"><a class="d-flex align-content-center position-relative mdl-js-button mdl-js-ripple-effect" href="<?php echo e(URL('admin/medialibrary/')); ?>"><i class="material-icons mx-4 align-self-center">photo_library</i>MEDIA LIBRARY</a></li>
							<li class="mesSNL"><a class="d-flex align-content-center position-relative mdl-js-button mdl-js-ripple-effect" href="<?php echo e(URL('admin/messages/')); ?>"><i class="material-icons mx-4 align-self-center">message</i>MESSAGES</a></li>
						</ul>
					</div><!-- /gn-scroller -->
				</nav>
			</li>

			<li><a href="#">SAM</a></li>
			<li class="float-right cust_li">
				<a class="codrops-icon d-flex align-content-center px-3" href="<?php echo e(url('/logout')); ?>">
					<i class="material-icons align-self-center">exit_to_app</i>
				</a>
			</li>
			<li class="float-right cust_li">
				<a class="codrops-icon d-flex align-content-center px-3" id="openSidenav">
					<i class="material-icons align-self-center">account_circle</i>
				</a>
				<input type="checkbox" name="" id="menuCkhBx" class="d-none">
			</li><!-- 
			<li class="float-right cust_li" id="notif_drp">
				<a class="codrops-icon d-flex align-content-center px-3" href="#">
					<i class="material-icons align-self-center mdl-badge mdl-badge--overlap m-0" data-badge="2">notifications_none</i>
				</a>
			</li>
			<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="notif_drp">
			  <li class="mdl-menu__item">Some Action</li>
			  <li class="mdl-menu__item">Another Action</li>
			  <li class="mdl-menu__item">Yet Another Action</li>
			</ul> -->
			<li class="float-right cust_li" id="msg_drp">
				<a class="codrops-icon d-flex align-content-center px-3" href="#">
					<i class="material-icons align-self-center mdl-badge mdl-badge--overlap m-0" data-badge="10">mail_outline</i>
				</a>
			</li>
			<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="msg_drp">
			  <li class="mdl-menu__item">Some Action</li>
			  <li class="mdl-menu__item">Another Action</li>
			  <li class="mdl-menu__item">Yet Another Action</li>
			</ul>
		</ul>
		
	</div><!-- /container -->
	<nav id="sidebar" class="position-fixed">
        <div class="sidebar-header position-relative">
            <a href="#" class="d-block">
            	<img src="<?php echo e(asset('assets/images/logo.png')); ?>" class="w-100">
            </a>
        </div>
        <div class="container account_form">
        	<form>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                    <input class="mdl-textfield__input" type="text" id="name" name="name">
                    <label class="mdl-textfield__label" for="name">Name</label>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                    <input class="mdl-textfield__input" type="text" id="username" name="username">
                    <label class="mdl-textfield__label" for="username">Username</label>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                    <input class="mdl-textfield__input" type="text" id="email" name="email">
                    <label class="mdl-textfield__label" for="email">Email</label>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                    <input class="mdl-textfield__input" type="password" id="password" name="password">
                    <label class="mdl-textfield__label" for="password">Password</label>
                </div>
                <center>
	                <button class="myButton1 mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored w-50 my-3">
	                	update
	                </button>
                </center>
        	</form>
        </div>
        <div class="menuTxt">
        	<p class="mb-0 text-uppercase select_none lspacing2" id="menuTxt">account</p>
        </div>
    </nav>
    <div class="overlay position-fixed w-100"></div>

    <?php echo $__env->yieldContent('content'); ?>
	
</body>

	<?php echo $__env->make('back-end.includes.links-scripts-admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/admin.js')); ?>"></script>
    <?php echo $__env->yieldContent('js'); ?>

</html>