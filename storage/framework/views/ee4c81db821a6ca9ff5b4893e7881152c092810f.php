<?php $__env->startSection('title'); ?>
    Account | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="main-container">

    <div class="banner">

        <div class="d-flex justify-content-center align-content-center h-100">
            
            <div class="logo_container h-100 d-flex flex-column">
                <img src="<?php echo e(asset('assets/images/logo.png')); ?>" class="img-fluid h-50 d-flex align-self-center mt-5 pt-5"><br>
                <p class="h5 text-uppercase Lspacing2 text-white text-center m-0 align-self-center">shopping assistant mirror</p>
            </div>

        </div>
        
    </div>

    <div class="container main-wrapper">
        
        <div class="main_area radius5 overflow-hidden mdl-shadow--16dp mb-5">

            <div class="container">
                <div class="d-flex justify-content-between px-5">
                    <p class="h5 text-uppercase Lspacing2 py-5">order details</p>
                    <p class="h5 text-uppercase Lspacing2 py-5"><?php echo e(Auth::user()->username); ?></p>
                </div>

                <div class="container">
                    
                    <ul class="nav nav_account">
                        <li class="nav-item">
                            <a class="nav-link active text-uppercase" href="#accountDetails" data-toggle="tab"><b>Account Details</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" href="#addresses" data-toggle="tab"><b>addresses</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" href="<?php echo e(url('/order')); ?>"><b>recent orders</b></a>
                        </li>
                        <li class="nav-item ml-auto">
                            <a class="nav-link text-uppercase text-danger" href="<?php echo e(url('/logout')); ?>"><b>logout</b></a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active py-5" id="accountDetails" role="tabpanel" aria-labelledby="home-tab">
                            <form method="post" action="<?php echo e(URL('account/update')); ?>" id="updateAccount">
                            <?php echo e(csrf_field()); ?>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6">

                                            <input type="text" name="first_name" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="FIRST NAME" value="<?php echo e($account->first_name); ?>">
                                            <div class="text-danger mb-1"><?php echo e($errors->first('first_name')); ?></div>
                                            <input type="text" name="last_name" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="LAST NAME" value="<?php echo e($account->last_name); ?>">
                                            <div class="text-danger mb-1"><?php echo e($errors->first('last_name')); ?></div>
                                            <input type="text" name="email" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="E-MAIL" value="<?php echo e($account->email); ?>">
                                            <div class="text-danger mb-1"><?php echo e($errors->first('email')); ?></div>

                                            <p class="passwordChange py-4"><b>PASSWORD CHANGE</b></p>

                                            <input type="password" name="current_password" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="CURRENT PASSWORD">
                                            <div class="text-danger mb-1"><?php echo e($errors->first('current_password')); ?></div>
                                            <input type="password" name="password" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="NEW PASSWORD">
                                            <div class="text-danger mb-1"><?php echo e($errors->first('new_password')); ?></div>
                                            <input type="password" name="confirm_password" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="CONFIRM PASSWORD">
                                            <div class="text-danger mb-1"><?php echo e($errors->first('confirm_password')); ?></div>

                                            <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 px-5 py-2 mb-3 mt-5" id="btnSave">save changes</button>

                                        </div>
                                        <div class="col-md-6">

                                            <input type="text" name="middle_name" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="MIDDLE NAME" value="<?php echo e($account->middle_name); ?>" >
                                            <div class="text-danger mb-1"><?php echo e($errors->first('middle_name')); ?></div>
                                            <input type="text" name="username" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="USERNAME" value="<?php echo e($account->username); ?>">
                                            <div class="text-danger mb-1"><?php echo e($errors->first('username')); ?></div>
                                            <input type="number" name="phone_number" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="PHONE NUMBER" value="<?php echo e($account->phone_number); ?>" >
                                            <div class="text-danger mb-1"><?php echo e($errors->first('phone_number')); ?></div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="addresses" role="tabpanel" aria-labelledby="profile-tab">Addresses</div>
                        <div class="tab-pane fade" id="recentOrderss" role="tabpanel" aria-labelledby="contact-tab">Recent Orders</div>
                    </div>

                </div>
            </div>

        </div>

    </div>

</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript">
		$('.mdl-navigation1').find('a:nth-child(1)').addClass('activeLink')
	</script>
	<!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/script.js')); ?>"></script>
	<?php echo $__env->make('includes.links-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>