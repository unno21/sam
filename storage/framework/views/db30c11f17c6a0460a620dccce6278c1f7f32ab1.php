<?php $__env->startSection('title'); ?>
    Reports | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="main-container w-100">

        <div class="main-wrapper">
            
            <div class="container">
                             
                <div class="py-5">
                    <div class="table-responsive">
                            <table class="table table-hover table-bordered bg-white mdl-shadow--4dp" id="iReportsTbl">
                                <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Category</th>
                                        <th>Price</th>
                                        <th>Available Stock</th>
                                        <th>Stock (%)</th>
                                    </tr>
                                </thead>
                                <tbody>
                               
                                    <tr>
                                        <td>yellow blouse</td>
                                        <td class="text-capitalize">tshirt</td>
                                        <td>₱ 2018.00</td>
                                        <td>201</td>
                                        <td>6%</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="8">
                                            <a href="<?php echo e(url('/icp/reports/download_invent')); ?>">
                                            <button class="mdl-button mdl-js-ripple-effect mdl-js-button mdl-button--raised mdl-button--colored myButton1 px-4 py-2 d-flex align-items-center">
                                                <i class="material-icons-new outline-print icon-white mr-2"></i>Print Report
                                            </button></a>
                                        </th>
                                    </tr>
                                </tfoot>
                                <tfoot>
                                    <tr>
                                        <th colspan="8">
                                            
                                        </th>
                                    </tr>
                                </tfoot>
                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
                                    <defs>
                                        <filter id="goo">
                                            <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                                            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                                            <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
                                        </filter>
                                    </defs>
                                </svg>
                            </table>
                        </div>
                </div>

            </div>
        </div>

    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript" src="<?php echo e(asset('assets/custom/js/admin.js')); ?>"></script>
    <script>
        $(".repSNL").addClass("SNLactive")
        $(".repSNL a").css("color","white") 

        $('#sReport1').click(function(){
            $('.nav-tabs > .active').next('li').find('a').trigger('click')
        })
        $('#iReport1').click(function(){
            $('.nav-tabs > .active > a').trigger('click')
        })

        $(".cust_tabs1").on("click","a",function(){
            $('.cust_tabs1 a').removeClass("reportsTab_active")
            $(this).addClass('reportsTab_active')
            var itemPos = $(this).position()
            $(".cust_selector1").css({
                "left":itemPos.left+"px"
            })
        })  
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('back-end.includes.manager', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>