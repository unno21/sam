<?php $__env->startSection('title'); ?>
    Account | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>


<!-- CUSTOM SNACKBAR -->

<?php if( Auth::check() ): ?>
<div class="cust_snackbar snackBar-plain p-3 mdl-shadow--4dp">
    <div class="text-white">
        <p class="d-flex">
            Welcome!
            <img src="<?php echo e(asset('assets/images/asd.png')); ?>" width="16px" height="16px" class="ml-2">
        </p>
        <?php echo e($account->full_name()); ?>

    </div>
</div>
<?php endif; ?>

<!-- END CUSTOM SNACKBAR -->


<div class="main-container">

    <div class="banner">

        <div class="d-flex justify-content-center align-content-center h-100">
            
            <div class="logo_container h-100 d-flex flex-column">
                <img src="<?php echo e(asset('assets/images/logo.png')); ?>" class="img-fluid h-50 d-flex align-self-center mt-5 pt-5"><br>
                <p class="h5 text-uppercase Lspacing2 text-white text-center m-0 align-self-center">shopping assistant mirror</p>
            </div>

        </div>
        
    </div>

    <div class="container main-wrapper">
        
        <div class="main_area radius5 overflow-hidden mdl-shadow--16dp mb-5">

            <div class="container">
                <div class="row justify-content-between px-4 pt-5 pb-3">
                    <div class="col-md-6">
                        <p class="h5 text-uppercase Lspacing2 mb-3 order_txt">order details</p>
                    </div>
                    <div class="col-md-6">
                        <p class="h5 text-uppercase Lspacing2 username"><?php echo e(Auth::user()->username); ?></p>
                    </div>
                </div>

                <div class="container">
                    
                    <ul class="row nav nav_account">
                        <li class="col-sm nav-item">
                            <a class="nav-link active text-uppercase mdl-js-button mdl-js-ripple-effect position-relative" href="#accountDetails" data-toggle="tab"><b>Account&nbsp;Details</b></a>
                        </li>
                        <li class="col-sm nav-item">
                            <a class="nav-link text-uppercase mdl-js-button mdl-js-ripple-effect position-relative" href="#addresses" data-toggle="tab"><b>addresses</b></a>
                        </li>
                        <li class="col-sm nav-item">
                            <a class="nav-link text-uppercase mdl-js-button mdl-js-ripple-effect position-relative" href="#gallery" data-toggle="tab"><b>my&nbsp;gallery</b></a>
                        </li>
                        <li class="col-sm nav-item">
                            <a class="nav-link text-uppercase mdl-js-button mdl-js-ripple-effect position-relative" href="<?php echo e(url('/order')); ?>"><b>recent&nbsp;orders</b></a>
                        </li>
                        <li class="col-sm nav-item">
                            <a class="nav-link text-uppercase text-danger mdl-js-button mdl-js-ripple-effect position-relative" href="<?php echo e(url('/logout')); ?>"><b>logout</b></a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade py-5 show active" id="accountDetails" role="tabpanel">
                            <form id="updateAccount">
                            <?php echo e(csrf_field()); ?>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="first_name" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="FIRST NAME" value="<?php echo e($account->first_name); ?>">
                                                    <div class="text-danger mb-1"><?php echo e($errors->first('first_name')); ?></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="middle_name" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="MIDDLE NAME" value="<?php echo e($account->middle_name); ?>" >
                                                    <div class="text-danger mb-1"><?php echo e($errors->first('middle_name')); ?></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="last_name" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="LAST NAME" value="<?php echo e($account->last_name); ?>">
                                                    <div class="text-danger mb-1"><?php echo e($errors->first('last_name')); ?></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="username" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="USERNAME" value="<?php echo e($account->username); ?>">
                                                    <div class="text-danger mb-1"><?php echo e($errors->first('username')); ?></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="email" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="E-MAIL" value="<?php echo e($account->email); ?>">
                                                    <div class="text-danger mb-1"><?php echo e($errors->first('email')); ?></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="number" name="phone_number" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="PHONE NUMBER" value="<?php echo e($account->phone_number); ?>" >
                                                    <div class="text-danger mb-1"><?php echo e($errors->first('phone_number')); ?></div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">

                                            <p class="passwordChange py-4"><b>PASSWORD CHANGE</b></p>

                                            <input type="password" name="current_password" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="CURRENT PASSWORD">
                                            <div class="text-danger mb-1"><?php echo e($errors->first('current_password')); ?></div>
                                            <input type="password" name="password" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="NEW PASSWORD">
                                            <div class="text-danger mb-1"><?php echo e($errors->first('new_password')); ?></div>
                                            <input type="password" name="confirm_password" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="CONFIRM PASSWORD">
                                            <div class="text-danger mb-1"><?php echo e($errors->first('confirm_password')); ?></div>

                                            <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 px-5 py-2 mb-3 mt-5" id="btnSave">save changes</button>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade py-5" id="addresses" role="tabpanel">
                            
                            <div class="container">
                                
                                <p class="passwordChange py-4"><b><?php echo e($account->address); ?> ( DEFAULT )</b></p>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive tbl_container">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>FULLNAME</th>
                                                        <th>ADDRESS</th>
                                                        <th>POSTCODE</th>
                                                        <th>PHONE NUMBER</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="content">
                                                    <?php echo $__env->make('front-end.account.includes.index-inner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                </tbody>
                                            </table>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored" id="add_new_address">add new address</button>
                                                    </td>
                                                </tr> 
                                            </tfoot>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <form id="address_form">
                                        <?php echo e(csrf_field()); ?>

                                        <div class="tblField_container">
                                            <label for="fn" class="text-secondary">FULLNAME</label>
                                            <input type="text" name="full_name" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="FULL NAME" value="" id="fn">
                                            <label for="adrs" class="text-secondary">ADDRESS</label>
                                            <input type="text" name="address" class="radius5 py-3 px-4 w-100 login_field mb-4" placeholder="ADDRESS" id="addrs">
                                            <label for="pc" class="text-secondary">POSTCODE</label>
                                            <input type="text" name="post_code" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="POSTCODE" value="" id="pc">
                                            <label for="pn" class="text-secondary">PHONE NUMBER</label>
                                            <input type="text" name="phone_number" class="radius5 py-3 px-4 w-100 login_field mb-4" autocomplete="off" placeholder="PHONE NUMBER" value="" id="pn">

                                            <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored mr-2" id="add_address">
                                                save
                                            </button>
                                            <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised cancel_edit_address" id="cancel_edit_address">
                                                cancel
                                            </button>
                                        </div>
                                        </form>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="tab-pane fade py-5" id="gallery" role="tabpanel">

                            <div class="gallery_container">
                                <div class="container">
                                    <div class="row my-gallery" itemscope>
                                        
                                        <!-- TO BE LOOPED -->
                                        <figure class="col-lg-2 col-md-3 col-sm-4 col-6 p-1 m-0" itemprop="associatedMedia" itemscope>
                                            <a class="gallery_img_holder overflow-hidden position-relative w-100 p-2 border d-block h-100" href="<?php echo e(asset('assets/images/1.jpg')); ?>" data-size="1024x1024">
                                                <img src="<?php echo e(asset('assets/images/1.jpg')); ?>" class="w-100 gallery_img" itemprop="thumbnail">
                                            </a>
                                        </figure>
                                        <!-- END TO BE LOOPED -->
                                        <figure class="col-lg-2 col-md-3 col-sm-4 col-6 p-1 m-0" itemprop="associatedMedia" itemscope>
                                            <a class="gallery_img_holder overflow-hidden position-relative w-100 p-2 border d-block h-100" href="<?php echo e(asset('assets/images/2.jpg')); ?>" data-size="1024x1024">
                                                <img src="<?php echo e(asset('assets/images/2.jpg')); ?>" class="w-100 gallery_img" itemprop="thumbnail">
                                            </a>
                                        </figure>
                                        <figure class="col-lg-2 col-md-3 col-sm-4 col-6 p-1 m-0" itemprop="associatedMedia" itemscope>
                                            <a class="gallery_img_holder overflow-hidden position-relative w-100 p-2 border d-block h-100" href="<?php echo e(asset('assets/images/3.jpg')); ?>" data-size="1024x1024">
                                                <img src="<?php echo e(asset('assets/images/3.jpg')); ?>" class="w-100 gallery_img" itemprop="thumbnail">
                                            </a>
                                        </figure>

                                    </div><!-- END ROW -->
                                </div>
                            </div><!-- END GALLERY container -->

                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

</div>

<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
        <div class="pswp__container">
            <!-- don't modify these 3 pswp__item elements, data is added later on -->
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript">
		$('.mdl-navigation2').find('a:nth-child(3)').addClass('activeLink')
	</script>
	<?php echo $__env->make('includes.links-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- photoswipe --><script type="text/javascript" src="<?php echo e(asset('assets/PhotoSwipe-master/dist/photoswipe.min.js')); ?>"></script>
    <!-- photoswipe --><script type="text/javascript" src="<?php echo e(asset('assets/PhotoSwipe-master/dist/photoswipe-ui-default.min.js')); ?>"></script>
    <!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/photoswipe.js')); ?>"></script>
    <!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/script.js')); ?>"></script>

    <script>

    $(".tblField_container").hide()
    $(".edit_address").on("click",function(){
        $(".tblField_container").show(100)
        $(".tbl_container").hide(100)
        var id = $(this).data('id')
        $.ajax({
            url     : "<?php echo e(URL('account/edit_address')); ?>/" + id ,
            type    : "get",
            success : function(data) {
                    $('#fn').val(data.address.full_name)
                    $('#addrs').val(data.address.address)
                    $('#pc').val(data.address.post_code)
                    $('#pn').val(data.address.phone_number)
            },error : function(data){
                    console.log(data)
            }
        })
    })
    
    $('#add_address').on('click', function(){
        $.ajax({
            url     : "<?php echo URL('account/insert'); ?>",
            type    : 'post',
            data    : $('#address_form').serialize(),
                       
            success : function(data) {
                    $('#content').empty()
                    $('#content').append(data.content)
                    cancelEdit()
                    addNew()
                    $("#cancel_edit_address").trigger('click')
            }, error : function(data){
                console.log(data)
            }
        })
    })

    function cancelEdit()
    {
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
        alert(';sdfd')
>>>>>>> 108ecc0fb1571edb962dfcf80761a3471e9f6ba2
>>>>>>> 13e6863de7c09bd046de1eb4af34ec578c49c065
        $(".cancel_edit_address").on("click",function(){
            $(".tblField_container").hide(100)
            $(".tbl_container").show(100)
        })
    }
    function addNew()
    {
        $('#add_new_address').on('click', function(){
            $(".tblField_container").show(100)
            $(".tbl_container").hide(100)
        })
    }

    cancelEdit()
    addNew()
    
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>