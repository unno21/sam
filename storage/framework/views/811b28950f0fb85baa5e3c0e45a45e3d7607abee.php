<?php $__env->startSection('title'); ?>
    Products | SAM
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


    <!-- CUSTOM SNACKBAR -->

    <div class="cust_snackbar snackBar-action p-3 mdl-shadow--4dp" id="snackbar">
        <div class="text-white mb-3" id="snackbar-text">
            Product name has been removed.
        </div>
        <div class="d-flex w-100 justify-content-end">
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect text-primary">Undo</button>
        </div>
    </div>

    <!-- END CUSTOM SNACKBAR -->


    <!-- *** SVG FOR SUBMIT BUTTON *** -->


    <svg class="svg--template loader">
        <circle class="circle1" stroke="none" stroke-width="4" fill="none" r="25" cx="25" cy="25"/>
    </svg>
    <svg class="svg--template checkmark" viewBox="0 0 50 50">
        <g class="checkmark1">
            <path class="line1" d="M20.8,36l-4,4c-0.7,0.7-1.7,0.7-2.4,0L0.8,26.4c-0.7-0.7-0.7-1.7,0-2.4l4-4c0.7-0.7,1.7-0.7,2.4,0l13.6,13.6
            C21.5,34.3,21.5,35.4,20.8,36z"/>
            <path class="line2" d="M14.5,39.9l-4-4c-0.7-0.7-0.7-1.7,0-2.4L43.4,0.6c0.7-0.7,1.7-0.7,2.4,0l4,4c0.7,0.7,0.7,1.7,0,2.4L16.9,39.9
            C16.3,40.6,15.2,40.6,14.5,39.9z"/>
        </g>
    </svg>

    <!-- *** FAB *** -->

    <div class="fab_holder">
    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab mdl-button--raised mdl-button--colored add_item cust_gradient " id="openModal">
    <i class="material-icons">add</i>
    </button>
    </div>


    <!-- *** INITIAL MODAL *** -->

    <div class="ui first longer modal" id="cust_modal">
        <div class="header d-flex justify-content-between">
            <div class="header_title">ADD PRODUCT</div>
            <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
                <a href="#" class="close-button" id="hideModal">
                    <div class="in">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                    <div class="out">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="content">
            <div class="container">
                <form class="modal_form" id="addProduct">
                <?php echo e(csrf_field()); ?>

                    <!-- FEATURED IMAGE -->
                    <div class="ft_img_container w-100 mb-4">
                        
                        <label class="lead text-secondary">SET FEATURED IMAGE</label>
                        <div class="row">

                            <div class="col-sm-6">
                                <!-- <input type="file" name="" id="chooseFtImg" class="d-none"> -->
                                <label for="chooseFtImg" id="haha">
                                    <img class="img-thumbnail w-100 file-in" src="../assets/images/add_img1.png" id="open_media_modal" >
                                    <input type="file" name="image" id="upload_img" class="d-none">
                                </label>
                            </div>

                            <div class="col-sm-6">

                                <div class="container">

                                    <label class="lead text-secondary"><small>SET PRODUCT GALLERY</small></label>
                                    <div class="row">
                                        
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2 d-flex justify-content-center align-content-center">
                                            <span class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab align-self-center view_more_btn" data-tooltip="View More Images" data-position="right center" data-inverted=""><i class="material-icons view_more">more_horiz</i></span>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div><!-- END ROW -->

                    </div>
                    <!-- PRODUCT SKU -->
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="text" id="name" name="name">
                        <label class="mdl-textfield__label" for="name">Name</label>
                    </div>
                    <!-- PRODUCT NAME -->
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="number" id="price" name="price">
                        <label class="mdl-textfield__label" for="price">Price</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="number" id="stock" name="stock">
                        <label class="mdl-textfield__label" for="stock">Stock</label>
                    </div>
                    
                    <div class="filterByCateg mdl_select">
                        <div class="d-inline">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height w-100">
                                <input type="text" class="mdl-textfield__input" id="filterByCateg" name="category" readonly>
                                <input type="hidden" value="prodSize" name="" id="filterByCateg">
                                <i class="drpdwn-icon material-icons mdl-icon-toggle__label">keyboard_arrow_down</i>
                                <label for="filterByCateg" class="mdl-textfield__label">Categories</label>
                                <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mb-0" for="filterByCateg">
                                    <li class="mdl-menu__item" data-val="TS" id="TS">T-Shirt</li>
                                    <li class="mdl-menu__item" data-val="PS" id="PS">Polo Shirt</li>
                                    <li class="mdl-menu__item" data-val="J" id="J">Jacket</li>
                                    <li class="mdl-menu__item" data-val="LS" id="LS">Long Sleeve</li>
                                    <li class="mdl-menu__item" data-val="TF" id="TF">Three Fourth's</li>
                                    <li class="mdl-menu__item" data-val="B" id="B">Blouse</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                            
                    
                </form>
            </div>
        </div>
        <div class="actions text-center border-0 bg-white p-3">
            <button class="btnSubmit" id="btnSubmit">SUBMIT</button>
        </div>
    </div>


    <!-- *** SECOND MODAL / FORM MEDIA LIBRARY *** -->


    <div class="ui second longer large modal draggable" id="media_modal">
        <div class="header d-flex justify-content-between handle">
            <div class="header_title">MEDIA LIBRARY</div>
            <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
                <a href="#" class="close-button" id="hide_media_modal">
                    <div class="in">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                    <div class="out">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="content">
            <nav class="cust_tabs mt-4 p-0 border">
                <div class="cust_selector" id="cust_selector"></div>
                <a href="#" class="active" id="mediaLibrary">MEDIA LIBRARY</a>
                <a href="#" class="" id="uploadFiles">UPLOAD IMAGES</a>
            </nav>
            <ul class="nav nav-tabs d-none">
                <li class="active"><a href="#tab1" data-toggle="tab">Shipping</a></li>
                <li><a href="#tab2" data-toggle="tab">Quantities</a></li>
            </ul>
            <div class="tab-content border mt-4">
                <div class="tab-pane active" id="tab1">
                    <p class="p-4 mb-0">MEDIA LIBRARY</p>

                    <div class="media_images_container">

                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
                            <defs>
                                <filter id="goo1">
                                    <feGaussianBlur in="SourceGraphic" stdDeviation="9" result="blur" />
                                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 20 -6" result="goo" />
                                    <feComposite in="SourceGraphic" in2="goo" operator="atop" result="goo"/>
                                </filter>
                            </defs>
                        </svg>

                        <div class="select-container position-relative pb-4">

                            <div class="d-flex justify-content-center">
                                <div class="selection-highlights position-absolute d-flex flex-wrap justify-content-center">
                                    <?php $counter = 0; ?>
                                    <?php $__currentLoopData = Storage::disk('products')->files('products'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filename): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <!-- TO BE LOOPED ALSO AS THE SELECTION ITEM LOOPED -->
                                        <input class="selection-checkbox" type="checkbox" id="selection-check-<?php echo e($counter++); ?>">
                                        <div class="selection-highlight"></div>
                                        <!-- END TO BE LOOPED -->
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                            <?php $counter = 0; ?>
                            <div class="selection-content d-flex flex-wrap justify-content-center">
                            <?php $__currentLoopData = Storage::disk('products')->files('products'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filename): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <label class="selection-item" for="selection-check-<?php echo e($counter++); ?>" data-checked="false">
                                    <span class="selection-item-container">
                                        <div class="d-flex justify-content-center h-100 position-relative">
                                            <div class="mediaLib_imgHolder d-flex justify-content-center position-relative align-self-center w-100">
                                                <div class="align-self-center">
                                                    <img src="<?php echo e(asset('storage/'. $filename )); ?>" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                    </span>
                                </label>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                            

                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <div class="tab_wrapper1 w-100 text-center">
                        <p class="h5">DROP FILES ANYWHERE TO UPLOAD</p>
                        <div class="upload_img_container mt-5">
                            <label for="upload_img"><span class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored mdl-button--raised text-white upload_img_btn">upload files</span></label>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="actions text-center border-0 bg-white p-3">
            <button class="btnSubmit">UPLOAD IMAGES</button>
        </div>
    </div>

    <!-- THIRD MODAL FOR UPDATE PRODUCTS -->

    <div class="ui longer modal" id="updateModal">
        <div class="header d-flex justify-content-between">
            <div class="header_title">UPDATE PRODUCT</div>
            <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
                <a href="#" class="close-button" id="hideUpdateModal">
                    <div class="in">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                    <div class="out">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="content">
            <div class="container">
                <form class="modal_form" id="addProduct">
                <?php echo e(csrf_field()); ?>

                    <!-- FEATURED IMAGE -->
                    <div class="ft_img_container w-100 mb-4">
                        
                        <label class="lead text-secondary">SET FEATURED IMAGE</label>
                        <div class="row">

                            <div class="col-sm-6">
                                <!-- <input type="file" name="" id="chooseFtImg" class="d-none"> -->
                                <label for="chooseFtImg" id="haha">
                                    <img class="img-thumbnail w-100 file-in" src="../assets/images/add_img1.png" id="open_media_modal" >
                                    <input type="file" name="image" id="upload_img" class="d-none">
                                </label>
                            </div>

                            <div class="col-sm-6">

                                <div class="container">

                                    <label class="lead text-secondary"><small>SET PRODUCT GALLERY</small></label>
                                    <div class="row">
                                        
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2">
                                            <img class="img-thumbnail w-100" src="../assets/images/add_img1.png">
                                        </div>
                                        <div class="col-4 mb-2 d-flex justify-content-center align-content-center">
                                            <span class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab align-self-center view_more_btn" data-tooltip="View More Images" data-position="right center" data-inverted=""><i class="material-icons view_more">more_horiz</i></span>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div><!-- END ROW -->

                    </div>
                    <!-- PRODUCT SKU -->
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="text" id="name" name="name">
                        <label class="mdl-textfield__label" for="name">Name</label>
                    </div>
                    <!-- PRODUCT NAME -->
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="number" id="price" name="price">
                        <label class="mdl-textfield__label" for="price">Price</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="number" id="stock" name="stock">
                        <label class="mdl-textfield__label" for="stock">Stock</label>
                    </div>
                    
                    <div class="filterByCateg mdl_select">
                        <div class="d-inline">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height w-100">
                                <input type="text" class="mdl-textfield__input" id="filterByCateg" name="category" readonly>
                                <input type="hidden" value="prodSize" name="" id="filterByCateg">
                                <i class="drpdwn-icon material-icons mdl-icon-toggle__label">keyboard_arrow_down</i>
                                <label for="filterByCateg" class="mdl-textfield__label">Categories</label>
                                <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mb-0" for="filterByCateg">
                                    <li class="mdl-menu__item" data-val="TS" id="TS">T-Shirt</li>
                                    <li class="mdl-menu__item" data-val="PS" id="PS">Polo Shirt</li>
                                    <li class="mdl-menu__item" data-val="J" id="J">Jacket</li>
                                    <li class="mdl-menu__item" data-val="LS" id="LS">Long Sleeve</li>
                                    <li class="mdl-menu__item" data-val="TF" id="TF">Three Fourth's</li>
                                    <li class="mdl-menu__item" data-val="B" id="B">Blouse</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                            
                    
                </form>
            </div>
        </div>
        <div class="actions text-center border-0 bg-white p-3">
            <button class="btnSubmit" id="btnSubmit">SUBMIT</button>
        </div>
    </div>
    

    <!-- *** MAIN CONTAINER *** -->

    <div class="main-container w-100 py-5">

        <!-- THIS AREA IS FOR SEARCH -->
        <svg class="cust_hidden" >
            <defs>
                <symbol id="icon-arrow" viewBox="0 0 24 24">
                    <title>arrow</title>
                    <polygon points="6.3,12.8 20.9,12.8 20.9,11.2 6.3,11.2 10.2,7.2 9,6 3.1,12 9,18 10.2,16.8 "/>
                </symbol>
                <symbol id="icon-drop" viewBox="0 0 24 24">
                    <title>drop</title>
                    <path d="M12,21c-3.6,0-6.6-3-6.6-6.6C5.4,11,10.8,4,11.4,3.2C11.6,3.1,11.8,3,12,3s0.4,0.1,0.6,0.3c0.6,0.8,6.1,7.8,6.1,11.2C18.6,18.1,15.6,21,12,21zM12,4.8c-1.8,2.4-5.2,7.4-5.2,9.6c0,2.9,2.3,5.2,5.2,5.2s5.2-2.3,5.2-5.2C17.2,12.2,13.8,7.3,12,4.8z"/><path d="M12,18.2c-0.4,0-0.7-0.3-0.7-0.7s0.3-0.7,0.7-0.7c1.3,0,2.4-1.1,2.4-2.4c0-0.4,0.3-0.7,0.7-0.7c0.4,0,0.7,0.3,0.7,0.7C15.8,16.5,14.1,18.2,12,18.2z"/>
                </symbol>
                <symbol id="icon-search" viewBox="0 0 24 24">
                    <title>search</title>
                    <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
                </symbol>
                <symbol id="icon-cross" viewBox="0 0 24 24">
                    <title>cross</title>
                    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
                </symbol>
            </defs>
        </svg>

        <div class="mysearch d-flex justify-content-center flex-column text-center align-items-center">
        <button id="btn-search-close" class="mybtn btn--search-close" aria-label="Close search form"><svg class="myicon icon--cross"><use xlink:href="#icon-cross"></use></svg></button>

        <form class="search__form my-5" action="" method="" id="search_engine">
            <input class="search__input" name="search" type="search" id="search_word" placeholder="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
            <span class="search__info">Hit enter to search or ESC to close</span>
        </form>

        </div>

        <!-- THIS AREA IS THE MAIN WRAPPER -->
        <div class="main-wrapper">
            <div class="container">

                <!-- FILTER/ACTION AREA -->

                <div class="filterArea container">
                    <div class="container">

                        <div class="row">
                            <!-- FILTER BY CATEGORY -->
                            <div class="col-sm-6 col-md-6 col-lg-3">
                                <div class="filterByCateg mdl_select">
                                    <div class="d-inline">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height w-100">
                                            <input type="text" class="mdl-textfield__input" id="category_filter" name="filterByCateg" readonly>
                                            <input type="hidden" value="prodSize" name="" id="filterByCateg">
                                            <i class="drpdwn-icon material-icons mdl-icon-toggle__label">keyboard_arrow_down</i>
                                            <label for="filterByCateg" class="mdl-textfield__label">Category</label>
                                            <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mb-0" for="filterByCateg">
                                                <li class="mdl-menu__item" data-val="ALL" id="ALL">All</li>
                                                <li class="mdl-menu__item" data-val="TS" id="TS">T-Shirt</li>
                                                <li class="mdl-menu__item" data-val="PS" id="PS">Polo Shirt</li>
                                                <li class="mdl-menu__item" data-val="J" id="J">Jacket</li>
                                                <li class="mdl-menu__item" data-val="LS" id="LS">Long Sleeve</li>
                                                <li class="mdl-menu__item" data-val="TF" id="TF">Three Fourth's</li>
                                                <li class="mdl-menu__item" data-val="B" id="B">Blouse</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- END FILTER BY CATEGORY -->

                            <!-- FILTER/ACTION -->
                            <div class="col-md-12 col-sm-12 col-lg-9">

                                <div class="d-flex justify-content-between h-100">
                                    <div class="filterAction h-100">
                                        <div class="d-flex align-content-center h-100">
                                            <div class="align-self-center">
                                                <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored cust_gradient mr-3">Filter</button>
                                                <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored cust_gradient">Trash</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="search4prod_container h-100">
                                        <div class="d-flex align-content-center h-100">
                                            <div class="search-wrap align-self-center ml-auto">
                                                <button id="custBtn-search" class="mybtn btn--search">
                                                    <svg class="myicon icon--search"><use xlink:href="#icon-search"></use></svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- END FILTER/ACTION -->

                        </div><!-- END ROW FILTER/ACTIONS -->

                    </div>
                </div><!-- END FILTER/ACTIONS AREA -->

                <div id="content">
                    <?php echo $__env->make('back-end.products.includes.index-inner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <!-- SearchUIEffects --><script type="text/javascript" src="<?php echo e(asset('assets/SearchUIEffects/js/demo5.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/custom/js/admin.js')); ?>"></script>
    <!-- checkbox-with-mo-js --><script src="<?php echo e(asset('assets/checkbox-with-mo-js/js/mo.min.js')); ?>"></script>
    <!-- checkbox-with-mo-js --><script src="<?php echo e(asset('assets/checkbox-with-mo-js/js/index.js')); ?>"></script>

    <script>
    $(".prodSNL").addClass("SNLactive")
    $(".prodSNL a").css("color","white")  
    
    $(document).ready(function(){
        $('#snackbar').hide();
        $('#btnSubmit').on('click', function(){
            $('#addProduct').submit() //trigger event
        });
        $("#upload_img").change(function() {
            readURL(this)
        });
        $('#search_engine').on('submit',function(e){
            e.preventDefault();
            var keyword = $('#search_word').val()
            $.ajax({
                type        : "get",
                url         : "<?php echo e(URL('admin/products/search')); ?>/" + keyword,
                success     : function(data) {
                            $('#content').empty();
                            $('#content').append(data.content);
                            $('#btn-search-close').trigger('click');
                },
                error       : function(data) {
                            console.log(data)
                },
            });
        });
        $('#category_filter').on('change', function(){
            $.ajax({
                type        : "get",
                url         : "<?php echo e(URL('admin/products/filter')); ?>/" + $(this).val(),
                success     : function(data) {
                            $('#content').empty();
                            $('#content').append(data.content);
                            console.log(data);
                },
                error       : function(data) {
                            console.log(data)
                },
            });
        });
        $('.btnDelete').on('click', function(){
            var qwe = confirm('Delete this product?')
            if (qwe) {
                var id = $(this).data('id')
                $.ajax({
                    type        : "get",
                    url         : "<?php echo URL('admin/products/delete'); ?>/" + id,
                    success     : function(data) {
                                $('#content').empty();
                                $('#content').append(data.content);
                                $('#snackbar').show()
                                $('#snackbar-text').html("Deleted Successfully")
                    },
                    error       : function(data) {
                                console.log(data)
                    },
                });
            }
        });
        
        // -- event handler -- //
        $('#addProduct').submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                'url'			: "<?php echo URL('admin/products/insert'); ?>",
                'method'		: 'post',
                'dataType'      : 'json',
                'data'			: formData,
                success 		: function(data){
                                $('#content').empty();
                                $('#content').append(data.content);
                },
                error           : function(data){
                                console.log(data);
                },
                contentType		: false,
                cache			: false,
                processData		: false
            });
        });

    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#open_media_modal').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    </script>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('back-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>