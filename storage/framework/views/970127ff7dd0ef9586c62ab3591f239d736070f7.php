<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Export Data</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <center><h2><?php echo e($title); ?></h2></center>
    <div class="table-responsive bg-white mdl-shadow--2dp" id="table-container">
        <table class="table table-hover mb-0">
            <thead>
                <tr>
                    <th>Order Name</th>
                    <th>Payment</th>
                    <th>Status</th>
                    <th>Date of Ordered</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $sales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td class="text-capitalize"><?php echo e($sale->order_number); ?></td>
                        <td>PHP <?php echo e(Crypt::decrypt($sale->amount)); ?>.00</td>
                        <td class="text-<?php echo e($sale->status == '2' ? 'success' : 'info'); ?>"><?php if($sale->status == '2'): ?> Delivered <?php elseif($sale->status == '1'): ?> Shipped <?php else: ?> In Process <?php endif; ?></td>
                        <td><?php echo e(date('F d, Y', strtotime($sale->created_at))); ?></td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</body>
</html>