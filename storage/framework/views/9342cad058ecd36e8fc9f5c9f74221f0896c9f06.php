<!-- *** SCRIPTS *** -->
	
	<!-- Bootstrap popper --><script type="text/javascript" src="<?php echo e(asset('assets/bootstrap/popper.min.js')); ?>"></script>
	<!-- Bootstrap --><script type="text/javascript" src="<?php echo e(asset('assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
	<!-- Bootstrap --><script type="text/javascript" src="<?php echo e(asset('assets/bootstrap-datetimepicker/js/bootstrap-material-datetimepicker.js')); ?>"></script>
	<!-- Material Design Lite --><script type="text/javascript" src="<?php echo e(asset('assets/MDL/material.min.js')); ?>"></script>
	<!-- Material Design Lite --><script type="text/javascript" src="<?php echo e(asset('assets/getmdl-select-master/getmdl-select.min.js')); ?>"></script>
	<!-- Semantic UI --><script type="text/javascript" src="<?php echo e(asset('assets/semantic/semantic.min.js')); ?>"></script>
	<!-- cust-button --><script type="text/javascript" src="<?php echo e(asset('assets/cust-button/js/index.js')); ?>"></script>
	<!-- chartist-js-master --><script type="text/javascript" src="<?php echo e(asset('assets/chartist-js-master/dist/chartist.min.js')); ?>"></script>
	<!-- CreativeGooeyEffects --><script type="text/javascript" src="<?php echo e(asset('assets/CreativeGooeyEffects/js/TweenMax.min.js')); ?>"></script>
	<!-- CreativeGooeyEffects --><script type="text/javascript" src="<?php echo e(asset('assets/CreativeGooeyEffects/js/pagination.js')); ?>"></script>
	<!-- CreativeGooeyEffects --><script type="text/javascript" src="<?php echo e(asset('assets/CreativeGooeyEffects/js/select.js')); ?>"></script>
	<!-- GoogleNexusMenu --><script type="text/javascript" src="<?php echo e(asset('assets/GoogleNexusWebsiteMenu/js/gnmenu.js')); ?>"></script>
	<!-- GoogleNexusMenu --><script type="text/javascript" src="<?php echo e(asset('assets/GoogleNexusWebsiteMenu/js/classie.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/tinymce/js/tinymce/tinymce.min.js')); ?>"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            height: 500,
            theme: 'modern',
            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools  contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ]
        });
    </script>
	<script>
		$(document).ready( function() {
		  	var $draggable = $('.draggable').draggabilly()
	  		// Draggabilly instance
		  	var draggie = $draggable.data('draggabilly')
		  	$draggable.on( 'dragMove', function() {
		    	console.log( 'dragMove', draggie.position.x, draggie.position.y )
		  	})
		})
		new gnMenu( document.getElementById( 'gn-menu' ) )
	</script>

	<script src="<?php echo e(asset('assets/custom/js/charts/monthlySales.js')); ?>"></script>