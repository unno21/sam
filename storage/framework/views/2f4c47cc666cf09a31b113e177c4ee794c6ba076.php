<!-- *** ICONS *** -->
	
	<!-- Material-icon --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/MDL/material-icon-master/material-icons.css')); ?>">
	<!-- Material-icon --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/MDL/material-icon-master/outline.css')); ?>">
	<!-- Ionicons --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/ionicons/css/ionicons.css')); ?>">
	<!-- font awesome --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets//font-awesome-4.5.0/css/font-awesome.min.css')); ?>" />

<!-- *** CSS *** -->

	<!-- CreativeGooeyEffects --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/CreativeGooeyEffects/css/normalize.css')); ?>">
	<!-- Bootstrap --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/bootstrap/css/bootstrap.min.css')); ?>">
	<!-- Bootstrap date-time-picker --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/bootstrap-datetimepicker/css/bootstrap-material-datetimepicker.css')); ?>">
	<!-- Bootstrap star rating --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/rateYo-master/src/jquery.rateyo.css')); ?>">
	<!-- Material Design Lite --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/MDL/material.min.css')); ?>">
	<!-- Material Design Lite --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/getmdl-select-master/getmdl-select.min.css')); ?>">
	<!-- Semantic UI --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/semantic/semantic.min.css')); ?>">
	<!-- GoogleNexusMenu --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/GoogleNexusWebsiteMenu/css/component.css')); ?>">
	<!-- GoogleNexusMenu --><script type="text/javascript" src="<?php echo e(asset('assets/GoogleNexusWebsiteMenu/js/modernizr.custom.js')); ?>"></script>
	<!-- UI dribble button --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/cust-button/css/style.css')); ?>">
	<!-- checkbox-with-mo-js --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/checkbox-with-mo-js/css/style.css')); ?>">
	<!-- chartist-js-master --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/chartist-js-master/dist/chartist.min.css')); ?>">
	<!-- elastic_slideshow --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/custom/css/elastic_slideshow.css')); ?>">
	<!-- jquery-ui --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/custom/css/jquery-ui.css')); ?>">
	<!-- photoswipe --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/PhotoSwipe-master/dist/photoswipe.css')); ?>">
	<!-- photoswipe --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/PhotoSwipe-master/dist/default-skin/default-skin.css')); ?>">
	<!-- rateYo-master --><link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/rateYo-master/min/jquery.rateyo.min.css')); ?>">
	