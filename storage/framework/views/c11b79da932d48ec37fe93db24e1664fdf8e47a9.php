<?php $__currentLoopData = $product_filter; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr>
    <td><div class="color_icon <?php echo e($product->category->name); ?>"></div></td>
    <td class="text-center">
        <?php echo e($product->category->name); ?>

        <input type="hidden" name="color_ids[]" value="<?php echo e($product->category_id); ?>">
    </td>
    <td>
        <input type="number" name="stock[]" class="form-control" value="<?php echo e($product->stock); ?>" min="0" step="1">
    </td>
    <td>
        <label for="upload_img_color-<?php echo e($product->category_id); ?>" class="text-center w-100 mb-0" id="img_color">
            <img class="file-in border p-1" height="50px" src="<?php echo e(asset('storage/products/'. $product->image)); ?>" id="img_<?php echo e($product->category_id); ?>">
            <input type="file" name="color_images[]" id="upload_img_color-<?php echo e($product->category_id); ?>" class="d-none img-input" data-target="img_${$(this).data('id')}">
        </label>
    </td>
</tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>