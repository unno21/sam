<?php $__env->startSection('title'); ?>
    Employees | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <!-- *** FAB *** -->

    <div class="fab_holder">
    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab mdl-button--raised mdl-button--colored add_item cust_gradient " id="openModal">
    <i class="material-icons">add</i>
    </button>
    </div>


    <!-- *** INITIAL MODAL *** -->

    <div class="ui first longer modal" id="cust_modal">
        <div class="header d-flex justify-content-between">
            <div class="header_title">ADD PRODUCT</div>
            <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
                <a href="#" class="close-button" id="hideModal">
                    <div class="in">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                    <div class="out">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="content">
            <div class="container">
                <form class="modal_form" id="addProduct">
                <?php echo e(csrf_field()); ?>

                    <!-- FEATURED IMAGE -->
                    <div class="ft_img_container w-100 mb-4">
                        
                        <p class="lead text-secondary text-center">SET FEATURED IMAGE</p>
                        <div class="row justify-content-center">

                            <div class="col-sm-4">
                                <!-- <input type="file" name="" id="chooseFtImg" class="d-none"> -->
                                <label for="chooseFtImg" id="haha">
                                    <img class="img-thumbnail w-100 file-in" src="../assets/images/add_img1.png" id="open_media_modal" >
                                    <input type="file" name="image" id="upload_img" class="d-none">
                                </label>
                            </div>

                        </div><!-- END ROW -->

                    </div>
                    <div class="row">
                    <!-- EMPLOYEE L NAME -->
                        <div class="col-sm-4">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                                <input class="mdl-textfield__input" type="text" id="lname" name="lname">
                                <label class="mdl-textfield__label" for="lname">Last Name</label>
                            </div>
                        </div>
                        <!-- EMPLOYEE F NAME -->
                        <div class="col-sm-4">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                                <input class="mdl-textfield__input" type="text" id="fname" name="fname">
                                <label class="mdl-textfield__label" for="fname">First Name</label>
                            </div>
                        </div>
                        <!-- EMPLOYEE M NAME -->
                        <div class="col-sm-4">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                                <input class="mdl-textfield__input" type="text" id="mname" name="mname">
                                <label class="mdl-textfield__label" for="mname">Middle Name</label>
                            </div>
                        </div>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="text" id="uname" name="uname">
                        <label class="mdl-textfield__label" for="uname">Userame</label>
                    </div>
                    
                    <div class="filterByCateg mdl_select">
                        <div class="d-inline">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height w-100">
                                <input type="text" class="mdl-textfield__input" id="filterByCateg" name="category" readonly>
                                <input type="hidden" value="prodSize" name="" id="filterByCateg">
                                <i class="drpdwn-icon material-icons mdl-icon-toggle__label">keyboard_arrow_down</i>
                                <label for="filterByCateg" class="mdl-textfield__label">Categories</label>
                                <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mb-0" for="filterByCateg">
                                    <li class="mdl-menu__item" data-val="M" id="TS">Manager</li>
                                    <li class="mdl-menu__item" data-val="C" id="PS">Cashier</li>
                                    <li class="mdl-menu__item" data-val="ICP" id="J">Inventory Control Personnel</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                            
                    
                </form>
            </div>
        </div>
        <div class="actions text-center border-0 bg-white p-3">
            <button class="btnSubmit" id="btnSubmit">SUBMIT</button>
        </div>
    </div>

    <div class="main-container w-100">

        <div class="main-wrapper">
            <div class="container">
                             
                <div class="py-5">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered bg-white mdl-shadow--4dp" id="iReportsTbl">
                            <thead>
                                <tr>
                                    <th>Employee Name</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Customer Name</td>
                                    <td>username123</td>
                                    <td>Cashier</td>
                                    <td>
                                        <span class="onlineStatus online"></span>Online
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <td>Customer Name</td>
                                    <td>username123</td>
                                    <td>Cashier</td>
                                    <td>
                                        <span class="onlineStatus offline"></span>Offline
                                    </td>
                                </tr>
                            </tbody>
                            
                            <tfoot>
                                <tr>
                                    <th colspan="4">
                                        <div class="pagination-navigation">
                                            <div class="pagination-current cust_gradient"></div>
                                            <div class="pagination-dots" id="iReportsPag">
                                                <button class="pagination-dot paginate_active">
                                                    <span class="pagination-number">1</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">2</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">3</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">4</span>
                                                </button>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </tfoot>
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
                                <defs>
                                    <filter id="goo">
                                        <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                                        <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                                        <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
                                    </filter>
                                </defs>
                            </svg>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript" src="<?php echo e(asset('assets/custom/js/admin.js')); ?>"></script>
    <script>
        $(".empSNL").addClass("SNLactive")
        $(".empSNL a").css("color","white")  
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('back-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>