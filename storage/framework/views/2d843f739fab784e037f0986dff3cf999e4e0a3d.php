<?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
        <td><?php echo e($color->name); ?></td>
        <td><?php echo e($color->type); ?></td>
        <td>
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon overflow-visible updateModal" data-id="<?php echo e($color->id); ?>" data-inverted="" data-position="top center" data-tooltip="Edit" id="updateModal_color" >
                <i class="material-icons-new outline-edit icon-action"></i>
            </button>
        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>