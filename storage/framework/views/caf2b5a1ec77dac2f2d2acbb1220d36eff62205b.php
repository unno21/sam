
<div class="row prod_list">
    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <!-- TO BE LOOPED -->
    <div class="col-sm-4 col-md-6 col-lg-4 item_sale_wrapper">
        <div class="container h-100 py-3">
            <div class="item_img_container h-100 d-flex flex-column">
                <div class="item_img_holder d-flex align-content-center">
                    <img src="<?php echo e(asset('storage/products/'. $product->image)); ?>" class="img-fluid d-flex align-self-center">
                </div>
                <div class="mt-auto">
                    <p class="lead text-uppercase m-0"><a href="<?php echo e(URL('/product/'. Crypt::encrypt($product->id))); ?>" class="item_name"><?php echo e($product->name); ?></a></p>
                    <div class="d-flex">
                        <p class="strike-through mr-3"><small>₱233.00</small></p>
                        <p class="sale_price">₱<?php echo e($product->price); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END TO BE LOOPED -->
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</div><!-- END ROW -->
<!-- END ROW -->
<div class="d-flex shop_pagination w-100">
    <div>
        <button class="mdl-button mdl-js-button mdl-js-ripple-effect shop_paginate_nav  h-100"><i class="material-icons">keyboard_arrow_left</i></button>
    </div>
    <div class="container h-100 py-3 borderX">
        <div class="pagination-navigation">
            <div class="pagination-current"></div>
            <div class="pagination-dots">
                <button class="pagination-dot paginate_active">
                    <span class="pagination-number">1</span>
                </button>
            </div>
        </div>
    </div>
    <div>
        <button class="mdl-button mdl-js-ripple-effect mdl-js-button shop_paginate_nav h-100"><i class="material-icons">keyboard_arrow_right</i></button>
    </div>
</div>