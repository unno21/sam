<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Export Data</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <center><h2><?php echo e($title); ?></h2></center>
    <div class="table-responsive bg-white mdl-shadow--2dp" id="table-container">
        <table class="table table-hover mb-0">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Available Stock</th>
                    <th>Stock (%)</th>
                </tr>
            </thead>
            <tbody>
                <?php $__empty_1 = true; $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <?php
                    $total_stock = $product->used_stock + $product->stock;
                    $percent_stock = ($product->used_stock / $total_stock) * 100;
                ?>
                    <tr>
                        <td><?php echo e($product->name); ?></td>
                        <td><?php echo e($product->category->name); ?></td>
                        <td><?php echo e($product->price); ?></td>
                        <td><?php echo e($product->stock); ?></td>
                        <td><?php echo e($percent_stock); ?> (%)</td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr><td colspan="7">No data</td></tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</body>
</html>