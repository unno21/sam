<?php echo $__env->make('includes/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="main-container">

	<div class="slideshow">
		<div class="slides">
			<div class="slide slide--current">
				<div class="slide__img" style="background-image: url(<?php echo e(asset('assets/images/1.jpg')); ?>)"></div>
				<h1 class="slide__title text-white Lspacing2 text-uppercase display-4">SAM</h1>
				<p class="slide__desc text-white">A matter of delicate proportions and aesthetics.</p>
				<a class="slide__link mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 Lspacing2 py-2 px-5" href="#">Read More</a>
			</div>
			<div class="slide">
				<div class="slide__img" style="background-image: url(<?php echo e(asset('assets/images/2.jpg')); ?>)"></div>
				<h2 class="slide__title text-white Lspacing2 text-uppercase display-4">Massive</h2>
				<p class="slide__desc text-white">The thoughtful making of space is an art.</p>
				<a class="slide__link mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 Lspacing2 py-2 px-5" href="#">Go to Shop</a>
			</div>
			<div class="slide">
				<div class="slide__img" style="background-image: url(<?php echo e(asset('assets/images/3.jpg')); ?>)"></div>
				<h2 class="slide__title text-white Lspacing2 text-uppercase display-4">Towering</h2>
				<p class="slide__desc text-white">If a building becomes architecture, then it is art.</p>
				<a class="slide__link mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 Lspacing2 py-2 px-5" href="#">Find out more</a>
			</div>
			<div class="slide">
				<div class="slide__img" style="background-image: url(<?php echo e(asset('assets/images/4.jpg')); ?>)"></div>
				<h2 class="slide__title text-white Lspacing2 text-uppercase display-4">Immense</h2>
				<p class="slide__desc text-white">Architecture is a visual art, and the buildings speak for themselves.</p>
				<a class="slide__link mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 Lspacing2 py-2 px-5" href="#">Uncover beauty</a>
			</div>
		</div>
		<nav class="slidenav d-flex justify-content-between">
			<button class="slidenav__item slidenav__item--prev mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab"><i class="material-icons text-white">keyboard_arrow_left</i></button>
			<button class="slidenav__item slidenav__item--next mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab"><i class="material-icons text-white">keyboard_arrow_right</i></button>
		</nav>
	</div>

	<div class="container main-wrapper">
		
		<div class="main_area radius5 overflow-hidden mdl-shadow--16dp mb-5">

			<div class="layer1">
				<div class="container">

					<div class="row">
						<div class="col-md-6">

							<div class="row">
								<div class="col-sm-6 py-5">
									<div class="container">
										<label class="text-uppercase text-white py-1 px-2 radius3 bg-black"><small>new</small></label>
										<p class="h1 text-uppercase Lspacing2 text-white my-5">yellow blouse</p>
										<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton2 px-5 py-2">more&nbsp;info</button>
									</div>
								</div>
								<div class="col-sm-6 py-5">
									<div class="container h-100">
										<div class="item_img_container h-100 d-flex">
											<div class="item_img_holder d-flex align-content-center">
												<img src="<?php echo e(asset('assets/images/yellow.png')); ?>" class="img-fluid d-flex align-self-center">
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="col-md-6">

							<div class="row">
								<div class="col-sm-6 py-5">
									<div class="container">
										<label class="text-uppercase text-white py-1 px-2 radius3 bg-black"><small>new</small></label>
										<p class="h1 text-uppercase Lspacing2 text-white my-5">yellow blouse</p>
										<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton2 px-5 py-2">more&nbsp;info</button>
									</div>
								</div>
								<div class="col-sm-6 py-5">
									<div class="container h-100">
										<div class="item_img_container h-100 d-flex">
											<div class="item_img_holder d-flex align-content-center">
												<img src="<?php echo e(asset('assets/images/stripe.png')); ?>" class="img-fluid d-flex align-self-center">
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div><!-- END ROW -->

				</div>
			</div><!-- END LAYER1 -->

			<div class="layer2 position-relative py-5">
				
				<div class="deco deco--title"></div>
				<div id="e-slideshow" class="e-slideshow">
					<div class="e-slide">
						<h2 class="e-slide__title e-slide__title--preview">black astronaut <span class="slide__price">₱150.00</span></h2>
						<div class="e-slide__item">
							<div class="e-slide__inner">
								<img class="e-slide__img e-slide__img--small" src="<?php echo e(asset('assets/images/1.png')); ?>" alt="Some image" />
								<button class="action action--open" aria-label="View details"><i class="material-icons mt-3">add_shopping_cart</i></button>
							</div>
						</div>
					</div>
					<div class="e-slide">
						<h2 class="e-slide__title e-slide__title--preview">black ship <span class="slide__price">₱150.00</span></h2>
						<div class="e-slide__item">
							<div class="e-slide__inner">
								<img class="e-slide__img e-slide__img--small" src="<?php echo e(asset('assets/images/2.png')); ?>" alt="Some image" />
								<button class="action action--open" aria-label="View details"><i class="material-icons mt-3">add_shopping_cart</i></button>
							</div>
						</div>
					</div>
					<div class="e-slide">
						<h2 class="e-slide__title e-slide__title--preview">linkin park <span class="slide__price">₱150.00</span></h2>
						<div class="e-slide__item">
							<div class="e-slide__inner">
								<img class="e-slide__img e-slide__img--small" src="<?php echo e(asset('assets/images/3.png')); ?>" alt="Some image" />
								<button class="action action--open" aria-label="View details"><i class="material-icons mt-3">add_shopping_cart</i></button>
							</div>
						</div>
					</div>
					<div class="e-slide">
						<h2 class="e-slide__title e-slide__title--preview">yellow blouse <span class="slide__price">₱150.00</span></h2>
						<div class="e-slide__item">
							<div class="e-slide__inner">
								<img class="e-slide__img e-slide__img--small" src="<?php echo e(asset('assets/images/yellow.png')); ?>" alt="Some image" />
								<button class="action action--open" aria-label="View details"><i class="material-icons mt-3">add_shopping_cart</i></button>
							</div>
						</div>
					</div>
					<div class="e-slide">
						<h2 class="e-slide__title e-slide__title--preview">gray stripe <span class="slide__price">₱150.00</span></h2>
						<div class="e-slide__item">
							<div class="e-slide__inner">
								<img class="e-slide__img e-slide__img--small" src="<?php echo e(asset('assets/images/stripe.png')); ?>" alt="Some image" />
								<button class="action action--open" aria-label="View details"><i class="material-icons mt-3">add_shopping_cart</i></button>
							</div>
						</div>
					</div>
					<button class="action action--close" aria-label="Close"><i class="fa fa-close"></i></button>
				</div>
			</div><!-- END LAYER2 -->

			<div class="layer3">
				
				<h1 class="text-center text-uppercase Lspacing2 my-4">sale</h1>

				<div class="container item_sale_container">
					<div class="row">

						<!-- TO BE LOOPED -->
						<div class="col-sm-6 col-md-4 col-lg-3 item_sale_wrapper">
							<div class="container h-100 py-3">
								<div class="item_img_container h-100 d-flex flex-column">
									<div class="item_img_holder d-flex align-content-center">
										<img src="<?php echo e(asset('assets/images/yellow.png')); ?>" class="img-fluid d-flex align-self-center">
									</div>
									<div class="mt-auto">
										<p class="lead text-uppercase m-0"><a href="product.php" class="item_name">yellow blouse</a></p>
										<div class="d-flex">
											<p class="strike-through mr-3"><small>₱150.00</small></p>
											<p class="sale_price">₱100.00</p>
										</div>
									</div>
								</div>
							</div>
						</div><!-- END TO BE LOOPED -->

						<div class="col-sm-6 col-md-4 col-lg-3 item_sale_wrapper">
							<div class="container h-100 py-3">
								<div class="item_img_container h-100 d-flex flex-column">
									<div class="item_img_holder d-flex align-content-center">
										<img src="<?php echo e(asset('assets/images/stripe.png')); ?>" class="img-fluid d-flex align-self-center">
									</div>
									<div class="mt-auto">
										<p class="lead text-uppercase m-0"><a href="#" class="item_name">gray stripe</a></p>
										<div class="d-flex">
											<p class="strike-through mr-3"><small>₱150.00</small></p>
											<p class="sale_price">₱100.00</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 item_sale_wrapper">
							<div class="container h-100 py-3">
								<div class="item_img_container h-100 d-flex flex-column">
									<div class="item_img_holder d-flex align-content-center">
										<img src="<?php echo e(asset('assets/images/1.png')); ?>" class="img-fluid d-flex align-self-center">
									</div>
									<div class="mt-auto">
										<p class="lead text-uppercase m-0"><a href="#" class="item_name">black astronaut</a></p>
										<div class="d-flex">
											<p class="strike-through mr-3"><small>₱150.00</small></p>
											<p class="sale_price">₱100.00</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 item_sale_wrapper">
							<div class="container h-100 py-3">
								<div class="item_img_container h-100 d-flex flex-column">
									<div class="item_img_holder d-flex align-content-center">
										<img src="<?php echo e(asset('assets/images/2.png')); ?>" class="img-fluid d-flex align-self-center">
									</div>
									<div class="mt-auto">
										<p class="lead text-uppercase m-0"><a href="#" class="item_name">black ship</a></p>
										<div class="d-flex">
											<p class="strike-through mr-3"><small>₱150.00</small></p>
											<p class="sale_price">₱100.00</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 item_sale_wrapper">
							<div class="container h-100 py-3">
								<div class="item_img_container h-100 d-flex flex-column">
									<div class="item_img_holder d-flex align-content-center">
										<img src="<?php echo e(asset('assets/images/yellow.png')); ?>" class="img-fluid d-flex align-self-center">
									</div>
									<div class="mt-auto">
										<p class="lead text-uppercase m-0"><a href="#" class="item_name">yellow blouse</a></p>
										<div class="d-flex">
											<p class="strike-through mr-3"><small>₱150.00</small></p>
											<p class="sale_price">₱100.00</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 item_sale_wrapper">
							<div class="container h-100 py-3">
								<div class="item_img_container h-100 d-flex flex-column">
									<div class="item_img_holder d-flex align-content-center">
										<img src="<?php echo e(asset('assets/images/stripe.png')); ?>" class="img-fluid d-flex align-self-center">
									</div>
									<div class="mt-auto">
										<p class="lead text-uppercase m-0"><a href="#" class="item_name">gray stripe</a></p>
										<div class="d-flex">
											<p class="strike-through mr-3"><small>₱150.00</small></p>
											<p class="sale_price">₱100.00</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 item_sale_wrapper">
							<div class="container h-100 py-3">
								<div class="item_img_container h-100 d-flex flex-column">
									<div class="item_img_holder d-flex align-content-center">
										<img src="<?php echo e(asset('assets/images/1.png')); ?>" class="img-fluid d-flex align-self-center">
									</div>
									<div class="mt-auto">
										<p class="lead text-uppercase m-0"><a href="#" class="item_name">black astronaut</a></p>
										<div class="d-flex">
											<p class="strike-through mr-3"><small>₱150.00</small></p>
											<p class="sale_price">₱100.00</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 item_sale_wrapper">
							<div class="container h-100 py-3">
								<div class="item_img_container h-100 d-flex flex-column">
									<div class="item_img_holder d-flex align-content-center">
										<img src="<?php echo e(asset('assets/images/2.png')); ?>" class="img-fluid d-flex align-self-center">
									</div>
									<div class="mt-auto">
										<p class="lead text-uppercase m-0"><a href="#" class="item_name">black ship</a></p>
										<div class="d-flex">
											<p class="strike-through mr-3"><small>₱150.00</small></p>
											<p class="sale_price">₱100.00</p>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div><!-- END ROW -->
				</div>

			</div>

			<div class="layer4">
				
				<div class="container">
					<div class="text-center">
						<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored myButton1 py-2 px-5 my-5">go to shop</button>
					</div>
				</div>

			</div>

		</div>

	</div>

	<?php echo $__env->make('includes/footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>


</body>
	<script type="text/javascript">
		$('.mdl-navigation1').find('a:nth-child(1)').addClass('activeLink')
	</script>
	<!-- custom js --><script type="text/javascript" src="<?php echo e(asset('assets/custom/js/script.js')); ?>"></script>
    <?php echo $__env->make('includes/links-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</html>