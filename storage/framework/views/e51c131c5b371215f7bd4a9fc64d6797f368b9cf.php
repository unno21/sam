
<?php if(Session::has('success')): ?>
    <div class="alert alert-success">
        <?php echo e(session('success')); ?>

    </div>
<?php endif; ?>
<?php if(Session::has('failed')): ?>
    <div class="alert alert-danger">
        <?php echo e(session('failed')); ?>

    </div>
<?php endif; ?>

<form action="<?php echo e(URL('/forgot-password')); ?>" method="POST">
    <?php echo e(csrf_field()); ?>

    <input type="email" name="email" placeholder="Email">
    <input type="submit" name="submit" value="Submit">
</form>