<?php $__env->startSection('title'); ?>
    Employees | SAM
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="main-container w-100">

        <div class="main-wrapper">
            <div class="container">
                             
                <div class="py-5">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered bg-white mdl-shadow--4dp" id="iReportsTbl">
                            <thead>
                                <tr>
                                    <th>Employee Name</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Customer Name</td>
                                    <td>username123</td>
                                    <td>Cashier</td>
                                    <td>
                                        <span class="onlineStatus online"></span>Online
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <td>Customer Name</td>
                                    <td>username123</td>
                                    <td>Cashier</td>
                                    <td>
                                        <span class="onlineStatus offline"></span>Offline
                                    </td>
                                </tr>
                            </tbody>
                            
                            <tfoot>
                                <tr>
                                    <th colspan="4">
                                        <div class="pagination-navigation">
                                            <div class="pagination-current cust_gradient"></div>
                                            <div class="pagination-dots" id="iReportsPag">
                                                <button class="pagination-dot paginate_active">
                                                    <span class="pagination-number">1</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">2</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">3</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">4</span>
                                                </button>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </tfoot>
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
                                <defs>
                                    <filter id="goo">
                                        <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                                        <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                                        <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
                                    </filter>
                                </defs>
                            </svg>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script type="text/javascript" src="<?php echo e(asset('assets/custom/js/admin.js')); ?>"></script>
    <script>
        $(".empSNL").addClass("SNLactive")
        $(".empSNL a").css("color","white")  
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('back-end.includes.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>