<div class="table-responsive" >
    <table class="table cart_table text-center" >
        <thead>
            <tr>
                <th></th>
                <th>IMAGE</th>
                <th>NAME</th>
                <th>COLOR</th>
                <th>SIZE</th>
                <th>PRICE</th>
                <th>QUANTITY</th>
                <th>TOTAL</th>
                <th></th>
            </tr>
        </thead>
        <tbody >
        <?php if( $cart !== null ): ?>
            <?php $__empty_1 = true; $__currentLoopData = $cart->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <!-- TO BE LOOPED -->
            <tr>
                <td></td>
                <td>
                    <div class="td_wrapper">
                        <img src="<?php echo e($item->sub_category_id === null ? asset('storage/products/'.$item->product->image) : asset('storage/products/'.$item->product_filters->image)); ?>" width="70px" class="border" id="image_<?php echo e($item->id); ?>">
                    </div>
                </td>
                <td class="text-uppercase">
                    <div class="td_wrapper">
                        <?php echo e($item->product->name); ?> 
                    </div>
                </td>
                <td class="text-uppercase">
                    <div class="td_wrapper">
                        <select class="text-uppercase color-option" name="color" id="" data-id="<?php echo e($item->id); ?>">
                            <option class="text-uppercase" value="default">Default</option>
                            <?php $__currentLoopData = $item->product->product_filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option class="text-uppercase" value="<?php echo e($color->id); ?>" <?php echo e($item->sub_category_id === $color->id ? 'selected' : ''); ?>><?php echo e($color->category->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </td>
                <td class="text-uppercase">
                    <div class="td_wrapper">
                        <select name="size" id="" class="text-uppercase size-option" data-id="<?php echo e($item->id); ?>">
                            <?php $__currentLoopData = $sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($size->id); ?>" class="text-uppercase" <?php echo e($item->size_id === $size->id ? 'selected' : ''); ?>><?php echo e($size->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </td>
                <td class="text-uppercase">
                    <div class="td_wrapper">
                        ₱<?php echo e($item->product->price); ?></td>
                    </div>
                <td class="text-uppercase">
                    <div class="td_wrapper">
                        <input type="hidden" name="id" value="<?php echo e($item->id); ?>">
                        <input type="number" name="quantity" min="1" class="quantity p-3 text-center quantityChange" value="<?php echo e($item->quantity); ?>" data-id="<?php echo e($item->id); ?>">
                    </div>
                </td>

                <td class="text-uppercase text-primary lead">
                    <div class="td_wrapper">
                        ₱<?php echo e($item->sub_total()); ?>.00</td>
                    </div>
                <td>
                    <div class="td_wrapper">
                        <button data-id="<?php echo e(Crypt::encrypt($item->id)); ?>" data-product="<?php echo e($item->product->name); ?>" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab mdl-button--mini-fab removeToCart btnRemove" type="button">
                            <i class="material-icons">clear</i>
                        </button>
                    </div>
                </td>
            </tr><!-- END BE LOOPED -->
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                    <td colspan="9">NO ITEM</td>
                </tr>
            <?php endif; ?>
        <?php else: ?> 
            <tr>
                <td>NO ITEM</td>
            </tr>
        <?php endif; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="9"></td>
            </tr>
        </tfoot>
    </table>
</div>
<div class="mb-5">
    <p class="h5 text-uppercase">cart total: <span class="text-primary">₱<?php echo e($cart === null ? '0' : $cart->total(). '.00'); ?></span></p>
</div>