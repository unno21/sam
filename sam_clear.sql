-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2018 at 10:51 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sam`
--

-- --------------------------------------------------------

--
-- Table structure for table `audit_trails`
--

CREATE TABLE `audit_trails` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cart_details`
--

CREATE TABLE `cart_details` (
  `id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `size_id` int(7) NOT NULL DEFAULT '1',
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'small', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(2, 'medium\r\n', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(3, 'large', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(4, 'extra large', 'size', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(5, 'jacket', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(6, 'polo shirt', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(7, 'three fourth\'s', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(8, 't-shirt', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(9, 'blouse', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(10, 'long sleeve', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(11, 'others', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(12, 'yellow', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(13, 'blue', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(14, 'pink', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(15, 'red', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(16, 'orange', 'color', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(17, 'male', 'gender', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(18, 'female', 'gender', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(19, 'unisex', 'gender', '2018-10-14 02:37:48', '2018-10-14 02:37:48'),
(20, 'purple', 'color', '2018-10-13 18:37:58', '2018-10-13 18:37:58'),
(21, 'brown', 'color', '2018-11-23 07:38:22', '2018-11-23 07:38:22'),
(22, 'green', 'color', '2018-11-23 07:51:02', '2018-11-23 07:51:02'),
(23, 'skirt', 'product', '2018-11-26 21:29:27', '2018-11-26 21:29:27'),
(24, 'pants', 'product', '2018-11-26 21:30:14', '2018-11-26 21:30:14'),
(25, 'short', 'product', '2018-11-26 21:29:27', '2018-11-26 21:29:27'),
(26, 'dress', 'product', '2018-10-14 02:37:48', '2018-10-14 02:37:48');

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `logo` varchar(191) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(191) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`id`, `logo`, `name`, `email`, `contact`, `created_at`, `updated_at`) VALUES
(1, 'asd', 'Shopping Assistant Mirror', 'internsprog@gmail.com', '123', '2018-07-26 01:51:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `log_trails`
--

CREATE TABLE `log_trails` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `log_date` date NOT NULL,
  `log_in` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_out` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mirror_logs`
--

CREATE TABLE `mirror_logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mirror_logs`
--

INSERT INTO `mirror_logs` (`id`, `user_id`, `status`, `updated_at`, `created_at`) VALUES
(3, 28, 1, '2018-12-25 06:37:11', '2018-12-25 06:35:44'),
(4, 30, 1, '2018-12-26 19:24:12', '2018-12-26 19:24:12'),
(5, 31, 1, '2018-12-26 21:20:52', '2018-12-26 21:20:52'),
(6, 32, 1, '2018-12-26 21:24:55', '2018-12-26 21:24:55'),
(7, 33, 1, '2018-12-26 21:26:15', '2018-12-26 21:26:15'),
(8, 34, 1, '2018-12-26 22:45:40', '2018-12-26 22:45:40'),
(9, 35, 1, '2018-12-26 23:12:47', '2018-12-26 23:12:47'),
(10, 36, 1, '2018-12-26 23:34:00', '2018-12-26 23:34:00'),
(11, 37, 1, '2018-12-26 23:49:45', '2018-12-26 23:49:45'),
(12, 38, 1, '2018-12-26 23:50:53', '2018-12-26 23:50:53'),
(13, 39, 1, '2018-12-26 23:51:45', '2018-12-26 23:51:45'),
(14, 40, 1, '2018-12-26 23:52:10', '2018-12-26 23:52:10'),
(15, 41, 1, '2018-12-26 23:53:24', '2018-12-26 23:53:24'),
(16, 42, 1, '2018-12-26 23:54:14', '2018-12-26 23:54:14'),
(17, 43, 1, '2018-12-26 23:56:16', '2018-12-26 23:56:16'),
(18, 44, 1, '2018-12-27 00:04:16', '2018-12-27 00:04:16'),
(19, 45, 1, '2018-12-27 00:04:50', '2018-12-27 00:04:50'),
(20, 46, 1, '2018-12-27 00:38:16', '2018-12-27 00:38:16'),
(21, 47, 1, '2018-12-27 00:38:59', '2018-12-27 00:38:59'),
(22, 48, 1, '2018-12-27 22:41:47', '2018-12-27 22:41:47'),
(23, 49, 1, '2018-12-27 22:42:27', '2018-12-27 22:42:27'),
(24, 50, 1, '2018-12-27 22:43:03', '2018-12-27 22:43:03'),
(25, 51, 1, '2018-12-27 23:10:11', '2018-12-27 23:10:11'),
(26, 52, 1, '2018-12-27 23:11:35', '2018-12-27 23:11:35'),
(27, 53, 1, '2018-12-27 23:13:56', '2018-12-27 23:13:56'),
(28, 54, 1, '2018-12-27 23:15:59', '2018-12-27 23:15:59'),
(29, 55, 1, '2018-12-27 23:16:35', '2018-12-27 23:16:35'),
(30, 56, 1, '2018-12-27 23:18:10', '2018-12-27 23:18:10'),
(31, 57, 1, '2018-12-27 23:20:00', '2018-12-27 23:20:00'),
(32, 58, 1, '2018-12-27 23:20:58', '2018-12-27 23:20:58'),
(33, 59, 1, '2018-12-27 23:22:25', '2018-12-27 23:22:25'),
(34, 60, 1, '2018-12-27 23:26:47', '2018-12-27 23:26:47'),
(35, 61, 1, '2018-12-27 23:27:33', '2018-12-27 23:27:33'),
(36, 62, 1, '2018-12-27 23:28:56', '2018-12-27 23:28:56'),
(37, 63, 1, '2018-12-27 23:29:47', '2018-12-27 23:29:47'),
(38, 64, 1, '2018-12-28 00:42:10', '2018-12-28 00:42:10'),
(39, 65, 1, '2018-12-28 00:42:11', '2018-12-28 00:42:11'),
(40, 66, 1, '2018-12-28 00:42:12', '2018-12-28 00:42:12'),
(41, 67, 1, '2018-12-28 00:43:00', '2018-12-28 00:43:00'),
(42, 68, 1, '2018-12-28 00:43:01', '2018-12-28 00:43:01'),
(43, 69, 1, '2018-12-28 00:43:02', '2018-12-28 00:43:02'),
(44, 70, 1, '2018-12-28 00:44:22', '2018-12-28 00:44:22'),
(45, 71, 1, '2018-12-28 00:44:22', '2018-12-28 00:44:22'),
(46, 72, 1, '2018-12-28 00:44:23', '2018-12-28 00:44:23'),
(47, 73, 1, '2018-12-29 22:02:08', '2018-12-29 22:02:08'),
(48, 74, 1, '2018-12-29 22:13:33', '2018-12-29 22:13:33'),
(49, 75, 1, '2018-12-29 22:17:03', '2018-12-29 22:17:03'),
(50, 76, 1, '2018-12-29 22:22:07', '2018-12-29 22:22:07'),
(51, 77, 1, '2018-12-29 22:25:45', '2018-12-29 22:25:45'),
(52, 78, 1, '2018-12-29 22:27:20', '2018-12-29 22:27:20'),
(53, 79, 1, '2018-12-29 22:29:49', '2018-12-29 22:29:49'),
(54, 80, 1, '2018-12-29 22:35:34', '2018-12-29 22:35:34'),
(55, 81, 1, '2018-12-29 22:38:15', '2018-12-29 22:38:15'),
(56, 82, 1, '2018-12-29 23:13:43', '2018-12-29 23:13:43'),
(57, 83, 1, '2018-12-29 23:16:28', '2018-12-29 23:16:28'),
(58, 84, 1, '2018-12-29 23:17:31', '2018-12-29 23:17:31'),
(59, 85, 1, '2018-12-29 23:17:54', '2018-12-29 23:17:54'),
(60, 86, 1, '2018-12-29 23:19:50', '2018-12-29 23:19:50'),
(61, 87, 1, '2018-12-29 23:20:31', '2018-12-29 23:20:31'),
(62, 88, 1, '2018-12-29 23:23:14', '2018-12-29 23:23:14'),
(63, 89, 1, '2018-12-29 23:25:37', '2018-12-29 23:25:37'),
(64, 90, 1, '2018-12-29 23:27:23', '2018-12-29 23:27:23'),
(65, 91, 1, '2018-12-30 00:36:28', '2018-12-30 00:36:28'),
(66, 92, 1, '2018-12-30 01:02:48', '2018-12-30 01:02:48');

-- --------------------------------------------------------

--
-- Table structure for table `notify_order`
--

CREATE TABLE `notify_order` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notify_order`
--

INSERT INTO `notify_order` (`id`, `order_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 2, 'Your order has been delivered by our courier.', '2018-09-24 22:05:10', '2018-09-24 22:05:10'),
(2, 1, 'Your order has been shipped by our courier.', '2018-09-24 22:05:21', '2018-09-24 22:05:21'),
(3, 4, 'Your order has been shipped by our courier.', '2018-09-27 18:10:29', '2018-09-27 18:10:29'),
(4, 4, 'Your order has been delivered by our courier.', '2018-09-27 18:11:45', '2018-09-27 18:11:45'),
(5, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:16:49', '2018-10-05 03:16:49'),
(6, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:17:03', '2018-10-05 03:17:03'),
(7, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:17:07', '2018-10-05 03:17:07'),
(8, 6, 'Your order has been shipped by our courier.', '2018-10-05 03:17:24', '2018-10-05 03:17:24'),
(9, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:47', '2018-10-12 19:36:47'),
(10, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:50', '2018-10-12 19:36:50'),
(11, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:58', '2018-10-12 19:36:58'),
(12, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:59', '2018-10-12 19:36:59'),
(13, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:36:59', '2018-10-12 19:36:59'),
(14, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:37:00', '2018-10-12 19:37:00'),
(15, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:37:01', '2018-10-12 19:37:01'),
(16, 8, 'Your order has been delivered by our courier.', '2018-10-12 19:37:01', '2018-10-12 19:37:01'),
(17, 22, 'Your order is being processed at the moment.', '2018-10-13 04:09:04', '2018-10-13 04:09:04'),
(18, 22, 'Your order has been shipped by our courier.', '2018-10-13 05:38:45', '2018-10-13 05:38:45'),
(19, 22, 'Your order has been shipped by our courier.', '2018-10-13 05:38:46', '2018-10-13 05:38:46'),
(20, 22, 'Your order has been shipped by our courier.', '2018-10-13 05:38:47', '2018-10-13 05:38:47'),
(21, 22, 'Your order is being processed at the moment.', '2018-10-13 05:40:59', '2018-10-13 05:40:59'),
(22, 23, 'Your order is being processed at the moment.', '2018-10-13 05:55:28', '2018-10-13 05:55:28'),
(23, 24, 'Your order is being processed at the moment.', '2018-10-13 05:56:06', '2018-10-13 05:56:06'),
(24, 24, 'Your order has been shipped by our courier.', '2018-10-13 21:36:13', '2018-10-13 21:36:13'),
(25, 25, 'Your order is being processed at the moment.', '2018-10-17 16:46:29', '2018-10-17 16:46:29'),
(26, 26, 'Your order is being processed at the moment.', '2018-10-21 01:26:10', '2018-10-21 01:26:10'),
(27, 26, 'Your order has been shipped by our courier.', '2018-10-21 02:19:13', '2018-10-21 02:19:13'),
(28, 26, 'Your order has been delivered by our courier.', '2018-10-23 01:57:10', '2018-10-23 01:57:10'),
(29, 27, 'Your order is being processed at the moment.', '2018-11-26 21:12:34', '2018-11-26 21:12:34'),
(30, 28, 'Your order is being processed at the moment.', '2018-11-26 21:16:24', '2018-11-26 21:16:24'),
(31, 29, 'Your order is being processed at the moment.', '2018-11-26 21:17:36', '2018-11-26 21:17:36'),
(32, 59, 'Your order is being processed at the moment.', '2018-12-27 01:03:53', '2018-12-27 01:03:53'),
(33, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:38', '2018-12-27 01:05:38'),
(34, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:39', '2018-12-27 01:05:39'),
(35, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:40', '2018-12-27 01:05:40'),
(36, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:40', '2018-12-27 01:05:40'),
(37, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:41', '2018-12-27 01:05:41'),
(38, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:42', '2018-12-27 01:05:42'),
(39, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:43', '2018-12-27 01:05:43'),
(40, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:43', '2018-12-27 01:05:43'),
(41, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:44', '2018-12-27 01:05:44'),
(42, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:44', '2018-12-27 01:05:44'),
(43, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:45', '2018-12-27 01:05:45'),
(44, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:45', '2018-12-27 01:05:45'),
(45, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:45', '2018-12-27 01:05:45'),
(46, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:46', '2018-12-27 01:05:46'),
(47, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:46', '2018-12-27 01:05:46'),
(48, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:46', '2018-12-27 01:05:46'),
(49, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:46', '2018-12-27 01:05:46'),
(50, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:47', '2018-12-27 01:05:47'),
(51, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:47', '2018-12-27 01:05:47'),
(52, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:47', '2018-12-27 01:05:47'),
(53, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:48', '2018-12-27 01:05:48'),
(54, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:48', '2018-12-27 01:05:48'),
(55, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:48', '2018-12-27 01:05:48'),
(56, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:49', '2018-12-27 01:05:49'),
(57, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:49', '2018-12-27 01:05:49'),
(58, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:49', '2018-12-27 01:05:49'),
(59, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:50', '2018-12-27 01:05:50'),
(60, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:50', '2018-12-27 01:05:50'),
(61, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:50', '2018-12-27 01:05:50'),
(62, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:51', '2018-12-27 01:05:51'),
(63, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:51', '2018-12-27 01:05:51'),
(64, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:51', '2018-12-27 01:05:51'),
(65, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:05:52', '2018-12-27 01:05:52'),
(66, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:06:43', '2018-12-27 01:06:43'),
(67, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:06:46', '2018-12-27 01:06:46'),
(68, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:08:56', '2018-12-27 01:08:56'),
(69, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:08:56', '2018-12-27 01:08:56'),
(70, 59, 'Your order is being processed at the moment.', '2018-12-27 01:09:05', '2018-12-27 01:09:05'),
(71, 59, 'Your order has been shipped by our courier.', '2018-12-27 01:09:45', '2018-12-27 01:09:45'),
(72, 59, 'Your order has been delivered by our courier.', '2018-12-27 01:10:10', '2018-12-27 01:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_number` varchar(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` text NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `address` text,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0 = process, 1 = shipped, 2 = delivered',
  `type` int(11) NOT NULL COMMENT '0 = COD, 1 = Credit Card',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `size_id` int(10) NOT NULL DEFAULT '1',
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `image` text,
  `image_serialize` text,
  `barcode` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `gender_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `stock` int(10) NOT NULL,
  `used_stock` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_by_subcategory`
--

CREATE TABLE `product_by_subcategory` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `upload_file` varchar(255) NOT NULL,
  `barcode` varchar(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `stock` int(10) NOT NULL,
  `used_stock` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_review`
--

CREATE TABLE `product_review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `rate` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `returned_products`
--

CREATE TABLE `returned_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `buyer_note` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `extension_name` varchar(3) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(191) DEFAULT NULL,
  `phone_number` varchar(12) DEFAULT NULL,
  `random_code` varchar(6) DEFAULT NULL,
  `address` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `extension_name`, `username`, `email`, `password`, `remember_token`, `phone_number`, `random_code`, `address`, `status`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'S', 'A', 'M', 'SAM', 'Admin', 'web_admin@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', 'LSmyCkW0wZZZtDXSFx2g9HQR9iR4UsR9MUdaMHcBo5rj465rqulQLB2Gn6wu', NULL, '', NULL, 1, 'admin', '2018-07-25 17:10:33', '2018-07-25 17:10:33', NULL),
(4, 'U', 'C', 'C', NULL, 'ICP', 'icp@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', 'ymEJJEwHnySGvwyrTuBr1OMFFujVSi32cJ0cRgd2p7sQFZz11SYaibqLA8Zo', NULL, '', NULL, 1, 'icp', '2018-07-25 17:27:24', '2018-07-25 17:27:24', NULL),
(5, 'Store', NULL, 'Manager', NULL, 'Manager', 'store_manager@gmail.com', '$2y$10$BvyQam/KHznW3SRNCr9T9uvp0miAi5gd9d2h.f5pTImmVsrbpSsIS', NULL, NULL, '', NULL, 1, 'manager', '2018-07-25 17:43:53', '2018-07-25 17:43:53', NULL),
(6, 'John', NULL, 'Roncales', NULL, 'jrroncales', 'jrroncales25@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', '5qNl5LQJAHi1LnFjgaxZdwOHpBPgME9wz2Gid7S8wp3RGX0MJ4p15egZpaV9', '639452538775', 'JSVKTT', 'blk 11 lot 8 north triangle cielito camarin caloocan city', 1, 'user', '2018-07-25 17:59:44', '2018-10-13 17:41:45', NULL),
(8, 'e2e2', NULL, 'e2e', NULL, '2e2', 'e2e2@yahoo.com', '$2y$10$0GoeVsYSuJjlASB8qDoSmeVY3S/GNVX/y1OPvVvMX3RjObwCW4C3a', NULL, '2', 'JSVKTT', NULL, 0, 'user', '2018-09-01 21:33:46', '2018-09-01 21:33:46', NULL),
(12, 'Richard', NULL, 'Roncales', NULL, 'unno21', 'chadxx1993@gmail.com', '$2y$10$7q.tDakqaFuuIqkubMTLveFbWo9ohd3BCbpA2E1m8gZQP9J4Vxs.W', 'XoUNh0GSBNDAlIpmeeyLwOP5rzdENnSSvlXBPukZCAiZtzwXwJxgjOpAWWeK', '639507353920', 'JSSRG7', NULL, 0, 'user', '2018-09-01 22:40:37', '2018-09-01 22:40:37', NULL),
(18, 'asndansd', NULL, 'nasdna', NULL, 'iwqjweq', 'qwje@yahoo.com', '$2y$10$esfu/Ipxe8BikC8s8kLr0erLiYGjW3ncQJC.bLdgmNUbPxb3A5isS', NULL, '639507353920', 'HDFQLO', NULL, 0, 'user', '2018-09-01 22:51:42', '2018-09-01 22:51:42', NULL),
(19, 'qwehqweiq', NULL, 'qhweuqwe', NULL, 'hwuqeuwe', 'qweqwe@yahoo.com', '$2y$10$88DOAimDVKbkZazfwAvqUub4EFCHHB4k8TcEfQ2Pv4lOIctyohMja', NULL, '639507353920', 'VG8IO0', NULL, 0, 'user', '2018-09-01 22:52:09', '2018-09-01 22:52:09', NULL),
(20, 'dvasdvaos', NULL, 'viavoiaos', NULL, 'vaois', 'vsaio@gmail.com', '$2y$10$mcRv2w5kW0PVMWMXCO6eFuhyHLJmFeT5xfCRtBQGEn.vVz.sPy6y6', NULL, '639507353920', 'W1Y7KU', NULL, 0, 'user', '2018-09-20 07:35:37', '2018-09-20 07:35:37', NULL),
(21, 'Jade', NULL, 'Coral', NULL, 'jade_coral', 'jadecoral@gmail.com', '$2y$10$nZ55/YqUoTMTsCnBb3ftEeKp3IxVXoZJQR3qtNPFTJZnYfKuMAOGu', 'eLwmcny7jdENBQStJkicZ6JJL4ubNvCXlx6pkY204y4E4njryEsgSNdHtOij', '639507353920', '41J5YC', NULL, 0, 'user', '2018-10-02 09:09:03', '2018-10-02 09:09:03', NULL),
(22, 'Tin', NULL, 'Carpio', NULL, 'tincarpio', 'tincarpio@gmail.com', '$2y$10$usn8W/xkBBTsT56IA0LhbejwlbJaUuZnxXAwXvXCd.0/nxERRjudq', 'RUHgLDaBawnyNJQWZMDhHgTv2mBhmh6wCSDwLBR0DTHKaZTEb5W0AOuVcRvJ', '639507353920', '859Z2C', NULL, 0, 'user', '2018-10-02 09:10:26', '2018-10-02 09:10:26', NULL),
(23, 'Joseph', NULL, 'Malaga', NULL, 'joseph', 'malaga@gmail.com', '$2y$10$3YLBHu5gyLrV0g1IO7K.TuESaBYX5rhBqR.DfWK/GIj0.9m1vaKIu', 'JWjkRL7ZFFulwjfqZljmxIwR93iQRahV5MK2ohVS15FDKbA71MpooHfxR9N0', '639507353920', 'M8EIWC', NULL, 1, 'user', '2018-10-02 09:15:44', '2018-10-02 09:15:53', NULL),
(24, 'Mac', 'A', 'Acebes', NULL, 'allennegro', 'negronegro@gmail.com', '$2y$10$09IsmYTkmmJ5nwAVUC2.dOD8o5OP9i7iozCb3hjn8TOVMWHHr6b4C', NULL, NULL, NULL, NULL, 1, 'cashier', '2018-10-02 10:11:44', '2018-10-02 10:11:44', NULL),
(26, 'eohan', 'eohan', 'eohan', NULL, 'eohan', 'eohan@gmail.com', '$2y$10$2A2/bYH7TaB.3lYmKonnsuuVaWJSKQTNoggQ1Hj7/5Q79OHh.ZuIW', NULL, NULL, NULL, NULL, 1, 'cashier', '2018-10-05 04:01:58', '2018-10-05 04:01:58', NULL),
(27, 'regerge', NULL, 'rgergege', NULL, 'gergege', 'uih1ui2h31@yahoo.com', '$2y$10$UDJdv0hSErnR.iFtlzQhqu6siJiI2gy4mSuUPrRc1IDLxv1ICaX8q', NULL, '639507353920', 'RXRR4U', NULL, 1, 'user', '2018-10-21 00:44:32', '2018-10-21 00:48:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `post_code` varchar(255) NOT NULL,
  `phone_number` varchar(13) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`id`, `user_id`, `full_name`, `address`, `post_code`, `phone_number`, `created_at`, `updated_at`) VALUES
(1, 6, 'jomari germino', 'Blk 11 Lot 19921', '1391jdewq', '639423123121', '2018-09-05 06:59:10', '0000-00-00 00:00:00'),
(2, 6, 'Mac Allen Acebes', 'aosdmcmo2mfon n jw9ejejjvj  2123 fef', 'qwdiq29  8weq8e jqwjeq21', '639423123121', '2018-09-05 07:07:38', '0000-00-00 00:00:00'),
(723, 6, 'sf sfsdf s', '23 adeada', '2323', '13131313', '2018-09-16 00:01:05', '2018-09-16 00:01:05'),
(724, 6, 'fs s s f', '32 sdasdsfs', '223', '13131346', '2018-09-16 00:02:21', '2018-09-16 00:02:21'),
(725, 6, 'sfsf sfsf', '23213 asdasda', '2313', '13478765', '2018-09-16 00:03:13', '2018-09-16 00:03:13'),
(726, 6, 'dasdasd', 'sdasdasd', 'ada', '2131', '2018-09-16 00:20:49', '2018-09-16 00:20:49'),
(727, 6, 'huuuuuuuuuuuuuuuu', 'sdasdasd', 'ada', '2131', '2018-09-16 00:39:21', '2018-09-16 00:39:21'),
(728, 6, 'huuuuuuu', 'sdasdasd', 'ada', '2131', '2018-09-16 00:41:02', '2018-09-16 00:41:02'),
(729, 6, 'sf sfsdf s', '23 adeada', '2323', '42141241', '2018-09-16 00:44:05', '2018-09-16 00:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `user_id`, `product_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 12, 21, '2018-12-04 07:34:13', '2018-12-04 07:34:13', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audit_trails`
--
ALTER TABLE `audit_trails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_details`
--
ALTER TABLE `cart_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_trails`
--
ALTER TABLE `log_trails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mirror_logs`
--
ALTER TABLE `mirror_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notify_order`
--
ALTER TABLE `notify_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_by_subcategory`
--
ALTER TABLE `product_by_subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_review`
--
ALTER TABLE `product_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `returned_products`
--
ALTER TABLE `returned_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audit_trails`
--
ALTER TABLE `audit_trails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart_details`
--
ALTER TABLE `cart_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `log_trails`
--
ALTER TABLE `log_trails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `mirror_logs`
--
ALTER TABLE `mirror_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `notify_order`
--
ALTER TABLE `notify_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `product_by_subcategory`
--
ALTER TABLE `product_by_subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `product_review`
--
ALTER TABLE `product_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `returned_products`
--
ALTER TABLE `returned_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=730;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
