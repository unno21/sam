

<form action="{{ URL('/reset-password') }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{ $id }}" />
    <input type="password" name="password" />
    <input type="password" name="confirm_password" />
    <input type="submit" name="submit" value="Reset Password" />
</form>