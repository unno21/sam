<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Export Data</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <center><h2>{{ $title }}</h2></center>
    <div class="table-responsive bg-white mdl-shadow--2dp" id="table-container">
        <table class="table table-hover mb-0">
            <thead>
                <tr>
                    <th>Order Name</th>
                    <th>Payment</th>
                    <th>Status</th>
                    <th>Date of Ordered</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sales as $sale)
                    <tr>
                        <td class="text-capitalize">{{$sale->order_number}}</td>
                        <td>PHP {{Crypt::decrypt($sale->amount)}}.00</td>
                        <td class="text-{{$sale->status == '2' ? 'success' : 'info'}}">@if($sale->status == '2') Delivered @elseif($sale->status == '1') Shipped @else In Process @endif</td>
                        <td>{{date('F d, Y', strtotime($sale->created_at))}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>