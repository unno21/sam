<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Export Data</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <center><h2>{{ $title }}</h2></center>
    <div class="table-responsive bg-white mdl-shadow--2dp" id="table-container">
        <table class="table table-hover mb-0">
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Username</th>
                    <th>E-mail</th>
                    <th>Shipping Address</th>
                    <th>Orders</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->full_name()}}</td>
                        <td>{{$user->username}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->address == null ? 'Does not set his/her address yet' : $user->address}}</td>
                        <td>{{count($user->orders)}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>