<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Export Data</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <center><h2>{{ $title }}</h2></center>
    <div class="table-responsive bg-white mdl-shadow--2dp" id="table-container">
        <table class="table table-hover mb-0">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Available Stock</th>
                    <th>Stock (%)</th>
                </tr>
            </thead>
            <tbody>
                @forelse($products as $product)
                @php
                    $total_stock = $product->used_stock + $product->stock;
                    $percent_stock = ($product->used_stock / $total_stock) * 100;
                @endphp
                    <tr>
                        <td>{{$product->name}}</td>
                        <td>{{$product->category->name}}</td>
                        <td>{{$product->price}}</td>
                        <td>{{$product->stock}}</td>
                        <td>{{$percent_stock}} (%)</td>
                    </tr>
                @empty
                <tr><td colspan="7">No data</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>
</body>
</html>