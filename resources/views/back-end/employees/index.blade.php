@extends('back-end.includes.manager')

@section('title')
    Employees | SAM
@endsection
@section('content')

    <!-- *** FAB *** -->

    <div class="fab_holder">
    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab mdl-button--raised mdl-button--colored add_item cust_gradient " id="openModal">
    <i class="material-icons">add</i>
    </button>
    </div>


    <!-- *** INITIAL MODAL *** -->

    <div class="ui first longer modal" id="cust_modal">
        <div class="header d-flex justify-content-between">
            <div class="header_title">ADD PRODUCT</div>
            <div class="close_btn_wrapper d-flex align-item-center justify-content-center">
                <a href="#" class="close-button" id="hideModal">
                    <div class="in">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                    <div class="out">
                        <div class="close-button-block"></div>
                        <div class="close-button-block"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="content">
            <div class="container">
                <form class="modal_form" id="addEmployee">
                {{csrf_field()}}
                   
                    <div class="row">
                     <!-- EMPLOYEE F NAME -->
                        <div class="col-sm-4">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                                <input class="mdl-textfield__input" type="text" id="fname" name="first_name">
                                <label class="mdl-textfield__label" for="fname">First Name</label>
                            </div>
                        </div>
                         <!-- EMPLOYEE M NAME -->
                         <div class="col-sm-4">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                                <input class="mdl-textfield__input" type="text" id="mname" name="middle_name">
                                <label class="mdl-textfield__label" for="mname">Middle Name</label>
                            </div>
                        </div>
                    <!-- EMPLOYEE L NAME -->
                        <div class="col-sm-4">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                                <input class="mdl-textfield__input" type="text" id="lname" name="last_name">
                                <label class="mdl-textfield__label" for="lname">Last Name</label>
                            </div>
                        </div>
                       
                       
                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="text" id="username" name="username">
                        <label class="mdl-textfield__label" for="username">Username</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="email" id="email" name="email">
                        <label class="mdl-textfield__label" for="email">Email</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label w-100">
                        <input class="mdl-textfield__input" type="password" id="password" name="password">
                        <label class="mdl-textfield__label" for="password">Password</label>
                    </div>

                </form>
            </div>
        </div>
        <div class="actions text-center border-0 bg-white p-3">
            <button class="btnSubmit" id="btnSubmit">SUBMIT</button>
        </div>
    </div>

    <div class="main-container w-100">

        <div class="main-wrapper">
            <div class="container">
                             
                <div class="py-5">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered bg-white mdl-shadow--4dp" id="iReportsTbl">
                            <thead>
                                <tr>
                                    <th>Employee Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="content">
                                @include('back-end.employees.includes.index-inner')
                            </tbody>
                            
                            <tfoot>
                                <tr>
                                    <th colspan="4">
                                        <div class="pagination-navigation">
                                            <div class="pagination-current cust_gradient"></div>
                                            <div class="pagination-dots" id="iReportsPag">
                                                <button class="pagination-dot paginate_active">
                                                    <span class="pagination-number">1</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">2</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">3</span>
                                                </button>
                                                <button class="pagination-dot">
                                                    <span class="pagination-number">4</span>
                                                </button>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </tfoot>
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="800" class="d-none">
                                <defs>
                                    <filter id="goo">
                                        <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                                        <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                                        <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
                                    </filter>
                                </defs>
                            </svg>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>


@endsection

@section('js')
    <script type="text/javascript" src="{{asset('assets/custom/js/admin.js')}}"></script>
    <script>
        $(".empSNL").addClass("SNLactive")
        $(".empSNL a").css("color","white")  
    
    $(document).ready(function(){
        $('#btnSubmit').on('click', function(){
            $('#addEmployee').submit() 
        });
        $('#addEmployee').on('submit', function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url     : "{{url('/manager/employees/insert')}}",
                method  : 'post',
                data    : formData,
                success : function(data) {
                        $('#content').empty()
                        $('#content').append(data.content)
                },
                error   : function(data) {
                        console.log(data)
                },
                contentType		: false,
                cache			: false,
                processData		: false

            })
        })
    });

    </script>
@endsection