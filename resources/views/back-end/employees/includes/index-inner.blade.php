@foreach($employees as $employee)
    <tr>
        <td>{{$employee->full_name()}}</td>
        <td>{{$employee->email}}</td>
        <td>{{$employee->username}}</td>
        <td class="text-capitalize">{{$employee->type}}</td>
        <td>
            <span class="onlineStatus online"></span>Online
        </td>
    </tr>
@endforeach