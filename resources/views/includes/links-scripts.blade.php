<!-- *** SCRIPTS *** -->

	<!-- Bootstrap popper --><script type="text/javascript" src="{{asset('assets/bootstrap/popper.min.js')}}"></script>
	<!-- Bootstrap --><script type="text/javascript" src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
	<!-- Material Design Lite --><script type="text/javascript" src="{{asset('assets/MDL/material.min.js')}}"></script>
	<!-- Material Design Lite --><script type="text/javascript" src="{{asset('assets/getmdl-select-master/getmdl-select.min.js')}}"></script>
	<!-- Semantic UI --><script type="text/javascript" src="{{asset('assets/semantic/semantic.min.js')}}"></script>
	<!-- GoogleNexusMenu --><script type="text/javascript" src="{{asset('assets/GoogleNexusWebsiteMenu/js/gnmenu.js')}}"></script>
	<!-- GoogleNexusMenu --><script type="text/javascript" src="{{asset('assets/GoogleNexusWebsiteMenu/js/classie.js')}}"></script>
	<!-- CreativeGooeyEffects --><script type="text/javascript" src="{{asset('assets/CreativeGooeyEffects/js/TweenMax.min.js')}}"></script>
	<!-- CreativeGooeyEffects --><!-- <script type="text/javascript" src="{{asset('assets/CreativeGooeyEffects/js/pagination.js')}}"></script> -->
	<!-- CreativeGooeyEffects --><!-- <script type="text/javascript" src="{{asset('assets/CreativeGooeyEffects/js/select.js')}}"></script> -->
	<!-- slideshow --><script type="text/javascript" src="{{asset('assets/custom/js/demo.js')}}"></script>
	<!-- slideshow --><script type="text/javascript" src="{{asset('assets/custom/js/anime.min.js')}}"></script>
	<!-- shape_overlay --><!-- <script type="text/javascript" src="{{asset('assets/custom/js/easings.js')}}"></script> -->
	<!-- shape_overlay --><!-- <script type="text/javascript" src="{{asset('assets/custom/js/shape_overlay.js')}}"></script> -->
	<!-- shape_overlay --><!-- <script type="text/javascript" src="{{asset('assets/jquery/jquery-ui.js')}}"></script> -->
	<!-- rateYo-master --><script type="text/javascript" src="{{asset('assets/rateYo-master/min/jquery.rateyo.min.js')}}"></script>